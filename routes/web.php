<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('register/{ref?}', 'Auth\RegisterController@showRegistrationForm')->name('register');
 
Auth::routes(['verify' => true]); 
Route::get('/', 'HomeController@index')->name('home');

Route::get('/app/ask', 'HomeController@getAsk')->name('app.ask.get');
Route::post('/app/ask', 'HomeController@postAsk')->name('app.ask.post');
Route::get('/radio/list', 'HomeController@radio')->name('radio');


App\Http\Controllers\UserController::routes();
App\Http\Controllers\NoteController::routes();
App\Http\Controllers\NoteCommentController::routes();
App\Http\Controllers\UloginController::routes();
App\Http\Controllers\CheckUserDataController::routes();

//mission
App\Http\Controllers\Mission\MissionController::routes();

//group
App\Http\Controllers\GroupController::routes();

//complice
App\Http\Controllers\CompliceController::routes();

//accomplice
App\Http\Controllers\AccompliceController::routes();

//photo-report
App\Http\Controllers\PhotoReportController::routes();

//adavai
App\Http\Controllers\AdavaiController::routes();

//terminated
App\Http\Controllers\TerminatedController::routes();

//diary
App\Http\Controllers\DiaryController::routes();

//notification
App\Http\Controllers\NotificationController::routes();

//product
App\Http\Controllers\ProductController::routes();

//chat
App\Http\Controllers\ChatController::routes();
 
//private-chat
App\Http\Controllers\PrivateChatController::routes();

//admin
App\Http\Controllers\AdminController::routes();

