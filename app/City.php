<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'title',    
        'country_id'    
    ];
    
    /**
     * Relation to country
     *
     * @return relation
     */
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }

    /**
     * Get title attribute
     *
     * @return string
     */
    public function getTitleOriginalAttribute()
    {
        return $this->attributes["title"];
    }

    /**
     * Mutate title attribute. Translate
     *
     * @return string
     */
    public function getTitleAttribute($value)
    {
        $arr = explode('/', $value);
        if (count($arr) < 2) return $value;
        $locale = App::getLocale();
        if($locale == 'ru') return $arr[0];
        else return $arr[1];
    }

}
