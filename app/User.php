<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\URL;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Mailer;
use App\Models\Complice;
use App\Models\Mission\Mission;
use App\Models\Accomplice;
use App\Models\UserAdavaiProfile;
use Session; 
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Croppa; 
use Chat;
use App;
use App\Helpers\LocaleHelper;
use Date;
use Musonza\Chat\Models\MessageNotification;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname',
        'email', 
        'password',
        'name',
        'gender',
        'city',
        'quote',
        'info',
        'born',
        'img',
        'identity',
        'freezed',
        'rating',
        'balance',
        'money',
        'skype',
        'last_seen',
        'locale'
    ];

    public $defaultAvatar = 'default.png';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 
        'password', 'remember_token'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['avatar_thumb', 'user_name', 'short_info'];

    /**
     * Relation to notes
     *
     * @return relation
     */
    public function ownNotes()
    { 
        return $this->hasMany('App\Note', 'owner_id');
    }

    public function addedNotes()
    {
        return $this->hasMany('App\Note', 'author_id');
    }

    public function noteComments()
    {
        return $this->hasMany('App\NoteComment', 'user_id');
    }

    public function noteLikes()
    {
        return $this->hasMany('App\NoteLike', 'user_id');
    }

    public function adavaiki()
    {
        return $this->hasMany('App\Models\AdavaiSubscriber', 'user_id');
    }

    public function missions()
    {
        return $this->hasMany('App\Models\Mission\UserMission', 'user_id');
    }

    public function groups()
    {
        return $this->hasMany('App\Models\UserGroup', 'user_id');
    }

    public function createdGroups()
    {
        return $this->hasMany('App\Models\Group', 'author_id');
    }

    public function diaries()
    {
        return $this->hasMany('App\Models\Diary', 'user_id');
    }

    public function userCity()
    {
        return $this->belongsTo('App\City', 'city');
    }

    public function markedUsers()
    {
        return $this->hasMany('App\Models\MarkedUser', 'user_id', 'id');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification', 'user_id', 'id');
    }

    public function reposts()
    {
        return $this->hasMany('App\Models\Repost', 'user_id', 'id');
    }

    public function questions()
    {
        return $this->hasMany('App\Models\Question', 'user_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', 'user_id', 'id');
    }

    public function awards()
    {
        return $this->hasMany('App\Models\Award', 'user_id', 'id');
    }

    public static function admin()
    {
        return self::where('admin', 1)->first();
    }

    public function adavaiProfile()
    {
        return $this->hasOne(UserAdavaiProfile::class, 'user_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $user = self::where('email', $this->email)->first();        
        $mailer = new Mailer();
        $body = view('mail.password-reset', [
            'user' => $user,
            'token' => $token
        ])->render();
        if($this->locale) App::setLocale($this->locale);       
        if($mailer->send($user->email, __('mail.reset.password'), $body))
        {
            Session::flash('message', __('mail.you.received.mail'));
            return redirect()->back();
        }
        
    }

    public function sendEmailVerificationNotification()
    {
        $user = self::where('email', $this->email)->first();        
        $mailer = new Mailer();

        $link = URL::temporarySignedRoute(
            'verification.verify', Carbon::now()->addMinutes(60), ['id' => $this->getKey()]
        );

        $body = view('mail.verify-email', [
            'user' => $user,
            'link' => $link
        ])->render();
        if($this->locale) App::setLocale($this->locale);
        if($mailer->send($user->email, __('mail.account.activation'), $body))
        {
            Session::flash('resented', __('mail.you.received.mail.activation'));
            return redirect()->back();
        }
    }

    /**
     * Define is this user admin
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->admin === 1;
    }

    /**
     * Define is this user's profile
     *
     * @return boolean
     */
    public function isOwnProfile()
    {
        return $this->id === Auth::user()->id;
    }

    /**
     * User's avatar
     *
     * @return string
     */
    public function avatar()
    {
        if ($this->freezed == 1) return $this->defaultAvatar;
        if ($this->img != '' && !is_null($this->img)) {
            if (Storage::disk('site')->exists('img/users/' . $this->img)) {
                return $this->img;
            } else {
                return $this->defaultAvatar;
            }
        }
        return $this->defaultAvatar;
    } 

    /**
     * Check if User's avatar exists
     *
     * @return boolean
     */
    public function isAvatarExists()
    {
        return ($this->img != '' && !is_null($this->img) && Storage::disk('site')->exists('img/users/' . $this->img));
    }

    /**
     * Get the administrator flag for the user.
     *
     * @return bool
     */
    public function getAvatarThumbAttribute()
    {
        return Croppa::url('public/img/users/'.$this->avatar(), 60, 60);
    }

    /**
     * Get the administrator flag for the user.
     *
     * @return bool
     */
    public function getNicknameAttribute()
    {
        if ($this->freezed == 1) return "Deleted";
        return $this->attributes['nickname'];
    }

    /**
     * Get the title.
     *
     * @return bool
     */
    public function getUserNameAttribute()
    {
        if ($this->freezed == 1) return "Deleted";
        return $this->attributes['nickname'];
    }

    /**
     * Get the reg date.
     *
     * @return bool
     */
    public function getJoinedAtAttribute() 
    { 
        Date::setlocale(App::getLocale()); 
        return Date::parse($this->attributes['created_at'])
                        ->setTimezone(LocaleHelper::userTimeZone())
                        ->format('j F Y H:i');
    } 

    /**
     * Check if email exists
     *
     * @return bool
     */
    public static function emailExists($email)
    {
        $user = User::where('email', $email)->first();
        return $user == true;
    }

    /**
     * Calculate age
     *
     * @return string
     */
    public function getAgeAttribute()
    {
        if($this->born) return Carbon::parse($this->attributes['born'])->age;
    }

    /**
     * Calculate days from registration
     *
     * @return string
     */
    public function getShortInfoAttribute()
    {
        $days = Carbon::parse($this->created_at)->diffInDays(Carbon::now());
        $missionsCount = cache()->remember('user.short.info.'.$this->id, Carbon::now()->addDays(7), function(){
            return $this->missions()->where(['status' => 'finish'])->count();
        });

        return view('site.user.short-info', [
            'days' => $days,
            'missionsCount' => $missionsCount,
            'rating' => $this->rating
        ])->render();
    }

    /**
     * Is complice
     *
     * @return boolean
     */
    public function isComplice($missionId = false, $withoutUnaccepted = false)
    {
        $check = new Complice();
        return $check->isComplice($this->id, $missionId, $withoutUnaccepted); 
    }

    /**
     * Get complice status
     *
     * @return property
     */
    public function getCompliceStatus($missionId = false, $withoutUnaccepted = false)
    {
        $complice = new Complice();
        return $complice->getStatus($this->id, $missionId, $withoutUnaccepted); 
    }

    /**
     * Is accomplice
     *
     * @return boolean
     */
    public function isAccomplice($missionId = false)
    {
        $check = new Accomplice();
        return $check->isAccomplice($this->id, $missionId); 
    }

    /**
     * Is complice
     *
     * @return boolean
     */
    public static function wasReferrer($nickname)
    {
        if(!$nickname) exit;
        $user = self::where('nickname', $nickname)->first();
        if (!$user) exit;
        $user->setRating(0.1); 
    }


    /**
     * set rating 
     *
     * @return boolean
     */
    public function setRating($rating)
    {
        $newRating = ($this->rating + ($rating * 0.10));
        $newBalance = ($this->balance + ($rating * 0.10));
        
        return $this->update([
            'rating' => $newRating,
            'balance' => $newBalance
        ]);

    }

    public function setBalance($amount)
    {   
        $amount = intval($amount);
        if (is_integer($amount) && $amount > 0) {
            return $this->update([
                'balance' => ($this->balance + $amount)
            ]);
        }
        return false;
    }

    public function setMoney($amount)
    {   
        $amount = intval($amount);
        if (is_integer($amount) && $amount > 0) {
            return $this->update([
                'money' => ($this->money + $amount)
            ]);
        }
        return false;
    }

    public function checkBalanceFor($sum)
    {   
        return $sum <= $this->balance;
    }

    /**
     * Notify about bad word
     *
     * @return 
     */
    public function warnAboutBadWord()
    {
        $title = ($this->isAdmin()) ? 'Участник грубит' : 'Предупреждение';

        return $this->notifications()->create([
            'title' => $title,
            'user_id' => $this->id, 
            'text' => __('message.badword.warning'),
            'link' => '#'
        ]);
    }

    public function unreadMessagesCount()
    {
        return Chat::messages()->setUser($this)->unreadCount();
    }

    /**
     * Notify about account freezing
     *
     * @return
     */
    public function notifyAboutFreezing()
    {     
        if (!$this->email) exit;
        $mailer = new Mailer();

        $body = view('mail.profile-freezing', [
            'nickname' => $this->nickname
        ])->render();
        if($this->locale) App::setLocale($this->locale);
        if($mailer->send($this->email, __('mail.account.will.freezed'), $body)) return true;
    }

    /**
     * Get user public conversations
     *
     * @return 
     */
    public function publicConversations()
    {
        $cc = Chat::conversations()->setUser($this)->isPrivate(false)->get();
        $missions = Mission::all();

        $unreads = MessageNotification::where([['user_id', '=', $this->id], ['is_seen', '=', 0]])->whereIn('conversation_id', $cc->pluck('id'))->get()->groupBy('conversation_id');

        $uMissions = Auth::user()->missions;

        $cc = $cc->map(function($item) use ($cc, $missions, $unreads, $uMissions){
            if (array_key_exists('mission', $item->data)) {
                $mission = $missions->firstWhere('id', $item->data["mission"]);
                if ($mission && $uMissions->where('mission_id', $item->data["mission"])
                                          ->firstWhere('status', 'active')){
                    $item->mission = $mission;
                    $item->missionAvatar = $mission->avatar();
                }
            }
            if ($mission){
                $unreads->keys()->each(function($id) use ($item, $unreads){
                    if ($id == $item->id) $item->unreadCount = $unreads[$item->id]->count();
                });
            }
            return $item;
        });

        return $cc->sortBy(function ($item) {
                    return $item->unreadCount;
                })->values();
    }

    /**
     * Get user private conversations
     *
     * @return collection
     */
    public function privateConversations()
    {
        $conversations = Chat::conversations()
                        ->common([$this->id])   
                        ->where('private', 1)
                        ->load(['users']);
        
        foreach ($conversations as $key => $c) {
            foreach ($c->users as $user) {
                if ($c->users->count() == 2) {
                    $c->companion = $c->users->firstWhere('id', '!=', $this->id) ?? null;
                }
            }
            $c->unreadCount = Chat::conversation($c)->setUser($this)->unreadCount();
            if ($c->getMessages($this, 1000000000)->count() == 0) unset($conversations[$key]);
        }
        
        return $conversations->sortBy(function ($item) {
                    return $item->unreadCount;
                })->values();
    }

    /**
     * Delete relation models
     *
     * @return bool
     */
    public static function boot() {
        parent::boot();

        self::deleting(function($item) {
            $item->questions()->each(function($item) {
                $item->delete();
            });

            $item->addedNotes()->each(function($item) {
                $item->delete();
            });
        });

        self::created(function($user){
            $user->adavaiProfile()->create();
        });
    }

}
 