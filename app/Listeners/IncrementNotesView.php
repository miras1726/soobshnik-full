<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Note;
use App\Helpers\NoteHelper;
use Illuminate\Support\Facades\Auth;


class IncrementNotesView
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = Auth::user();
        if ($event->notes->count() > 0) {
            $event->notes->each(function($note) use($user){
                if ($note->adavai && !NoteHelper::userViewedNote($note->id, $user->id)) {
                    $note->increment('views');
                    NoteHelper::updateNoteViewers($note->id, $user->id);
                }
            });
        }
    }
}
