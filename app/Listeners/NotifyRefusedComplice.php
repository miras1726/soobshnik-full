<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use App\Models\Notification;
use App\User;
use App; 

class NotifyRefusedComplice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if($event->complice->user->locale) App::setLocale($event->complice->user->locale);
        Notification::create([
            'user_id' => $event->complice->user->id,
            'title' => __('message.complice.refused.title'),
            'text' => __('message.complice.refused', ['nickname' => Auth::user()->nickname,
                                                      'mission' => $event->mission->title,
                                                      'rating' => $event->rating,
                                                      'reason' => $event->reason]),
            'link' => route('complice.list')
        ]);
    }
}
