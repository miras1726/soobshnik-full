<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Note;
use App\Models\Repost;
use App\Models\MarkedUser;
use Illuminate\Support\Facades\DB;

class DoAboutLikedNote
{
    /** 
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $note = $event->note;
        if (!$note) return false;
        $this->repostToMarkedUsers($note);

        $check = DB::table('note_reached_likes')->where('note_id', $note->id)->first();
        if ($check) return false;
        if ($note->likes->count() >= 100) {
            $this->setRating($note);
        } 
    }

    private function setRating($note)
    {
        if ($note->adavai) {
                ($note->author && $note->author->adavaiProfile) ? $note->author->adavaiProfile->setRating(0.1) : null;    
        } else {
            $note->author ? $note->author->setRating(0.1) : null;
        } 
        DB::table('note_reached_likes')->insert(['note_id' => $note->id, 'likes_count' => $note->likes->count()]);
    }

    private function repostToMarkedUsers($note)
    {
        if ($note->type == 'mission' || $note->type == 'group') {
            $users = MarkedUser::where('marked_user_id', $note->author_id)->get(['user_id'])->pluck('user_id');
            $users->each(function($userId) use ($note){
                Repost::create([
                    'user_id' => $userId,
                    'note_id' => $note->id
                ]);
            });
        }
    }
}
