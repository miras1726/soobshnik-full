<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Models\Report;

class BadWordWarning
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    { 
        //
    }
 
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $admin = User::admin();
        $admin->warnAboutBadWord();
        if ($admin->id != $event->note->author_id) $event->user->warnAboutBadWord();
        Report::create([
                'item_id' => $event->note->id,
                'user_id' => $event->note->author_id,
                'type' => 'note',
                'report' => 0
            ]);
    }
}
