<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App;

class NotifyAdavaiManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    } 

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = User::find($event->note->author_id);
        if (!$user) exit();
        $status = $event->joined ? 'joined' : 'left'; 
        
        if($user->locale) App::setLocale($user->locale);
        $user->notifications()->create([
            'title' => __('message.adavai.notification.'.$status.'.title'),
            'text' => __('message.adavai.notification.'.$status.'.text', [
                'user' => $event->user->nickname,
                'content_short' => $event->note->content_short
            ]),
            'link' => route('adavai.list', ['note' => $event->note->id]),
            'type' => 'adavai',
            'target_id' => $event->note->id,
        ]);
    }
}
