<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Notification;
use App\User;
use App;

class NotifyCompliceRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $complice = User::find($event->complice->user_id);
        $user = User::find($event->complice->complice_id);
        if($user->locale) App::setLocale($user->locale);
        Notification::create([
            'user_id' => $event->complice->complice_id,
            'title' => __('message.complice.request.title'),
            'text' => __('message.complice.request', ['nickname' => $complice->nickname]),
            'link' => route('complice.list')
        ]);
    }
}
