<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App;

class NotifyAdavaiUsers
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $note = $event->note;
        $users = $this->getUsers($note->adavai->check_age, $note->adavai->city_id, $note->author_id);
        $users->each(function($user) use($note){
            if($user->locale) App::setLocale($user->locale);
            $user->notifications()->create([
                'title' => __('message.new.adavai.notification.title'),
                'text' => __('message.new.adavai.notification.text'),
                'link' => route('adavai.list', ['note' => $note->id]),
                'type' => 'adavai',
                'target_id' => $note->id,
            ]);
        });
    }

    private function getUsers($checkAge = false, $city, $author)
    {
        $now = now()->subYears(18)->format('Y-m-d');
        return User::whereHas('adavaiProfile', function($query){
                    $query->where('enable_notifications', 1);
                })->where(function($query) use($checkAge, $now){
                    $checkAge ? $query->whereNotNull('born')->whereDate('born', '<', $now) : null;
                })->where('city', $city)->where('id', '!=', $author)->get();
    }
}
