<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;
use App\Note;
use App\Models\Complice;

class CompliceRefused
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $complice;
    public $rating;
    public $mission;
    public $reason;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Complice $complice, $rating, $mission, $reason)
    { 
        $this->complice = $complice;
        $this->rating = $rating;
        $this->mission = $mission;
        $this->reason = $reason;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
