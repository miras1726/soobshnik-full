<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Routing\Router;
use File;

class GenerateRoutesJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'routes:json';

    protected $route;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Put all routes into json file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Router $route)
    {
        parent::__construct();
        $this->route = $route;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $routes = [];
        foreach($this->route->getRoutes() as $route){
            $routes[$route->getName()] = $route->uri();
        }

        File::put('resources/js/routes.json', json_encode($routes, JSON_PRETTY_PRINT));
    }
}
