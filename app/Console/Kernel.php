<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\CronController;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    { 
        $schedule->call(function(){
            CronController::prolongLicencies();
            CronController::birthdayCheck();
            CronController::profileCheck();
            CronController::diaryReminder(1);
        })->daily();

        $schedule->call(function(){
            CronController::diaryEmailing(30);
        })->monthly();

        $schedule->call(function(){
            CronController::diaryEmailing(7);
            CronController::diaryReminder(7);
        })->weekly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
