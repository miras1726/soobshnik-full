<form action="{{ route('complice.search') }}" method="get">

	  		<div class="col-12">
				<div class="form-group"> 
					<label>{{ __('search.nickname') }}</label>
					<input type="text" name="nickname" class="form-control" placeholder="{{ __('search.any') }}">
				</div>
	  		</div> 

	  		<div class="col-12">
				<div class="form-group"> 
					<label>{{ __('search.mission') }}</label>
					<select class="form-control" name="mission">
						@if($categories->count() > 0)
							@foreach($categories as $cat)
							<optgroup label="{{ $cat->title }}"></optgroup>
								@if($cat->missions->count() > 0)
									@foreach($cat->missions as $mission)
										<option value="{{ $mission->id }}">{{ $mission->title }}</option>
									@endforeach
								@endif
							@endforeach
						@endif 
					</select>
				</div>
	  		</div> 
			<div class="col-md-12">
				<div class="form-group">
					<label>{{ __('search.city') }}</label>
					<select class="form-control" name="city">
						<option value="">{{ __('search.any') }}</option>
						@if($countries->count() > 0)
							@foreach($countries as $country)
							<optgroup label="{{ $country->title }}"></optgroup>
								@if($country->cities->count() > 0)
									@foreach($country->cities as $city)
										<option value="{{ $city->id }}">{{ $city->title }}</option>
									@endforeach
								@endif
							@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label>{{ __('search.age') }}</label>
					<div class="row">
						<div class="col-6">
							<div class="form-group mb-0">
								<select class="form-control" name="from">
									<option value="">{{ __('search.from') }}</option>
										@for($from=16; $from < 50; $from++)
											<option value="{{ $from }}">{{ $from }}</option>
										@endfor
								</select>
							</div>
						</div>
						<div class="col-6">
							<div class="form-group mb-0">
								<select class="form-control" name="to">
									<option value="">{{ __('search.to') }}</option>
									@for($to=16; $to < 50; $to++)
										<option value="{{ $to }}">{{ $to }}</option>
									@endfor
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label>{{ __('search.gender') }}</label>
					<select class="form-control" name="gender">
						<option value="">{{ __('search.any') }}</option>
						<option value="male">{{ __('search.male') }}</option>
						<option value="female">{{ __('search.female') }}</option>
					</select>
				</div>
			</div>
	  		<div class="col-12 mt-3">
	  			<button type="submit" class="btn-default btn-default-sm text-center">{{ __('search.search') }}</button>
	  		</div>
	  		</form>


 