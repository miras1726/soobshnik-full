<?php

namespace App\Widgets;

use Klisl\Widgets\Contract\ContractWidget;
use App\Models\Mission\MissionCategory;
use App\Country;

/**
 * Class TestWidget
 * Класс для демонстрации работы расширения
 * @package App\Widgets
 */
class SearchWidget implements ContractWidget{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function execute(){
		$countries = Country::all();
        $categories = MissionCategory::all();
        
		return view('Widgets::search', [
			'countries' => $countries,
			'categories' => $categories
		]);
		
	}	
}
