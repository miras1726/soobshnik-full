<?php

namespace App\Widgets;

use Klisl\Widgets\Contract\ContractWidget;
use App\Models\Settings;

/**
 * Class TestWidget
 * Класс для демонстрации работы расширения
 * @package App\Widgets
 */
class AdminShowCountWidget implements ContractWidget{

	public $type;

	public function __construct($data)
	{
		$this->type = $data["property"];
	} 

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function execute(){
		$settings = Settings::first();

		$count =  ($settings->{$this->type} == 0) ? '' : $settings->{$this->type};
		return view('Widgets::admin-order-count', [
			'count' => $count
		]);
	}	 
}