<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Date;
use App;
use App\NoteLike;
use App\User;
use App\Helpers\LocaleHelper;

class Note extends Model 
{
    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [ 
        'owner_id',
        'author_id',
        'content',
        'type'    
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['posted_at'];

    /**
     * Relation to user
     *
     * @return relation
     */
    public function owner()
    {
        if ($this->type){
            return $this->belongsTo('App\Models\Group', 'owner_id', 'id');
        }
        return $this->belongsTo('App\User', 'owner_id', 'id');
    }

    /**
     * Relation to user
     *
     * @return relation
     */
    public function author()
    { 
        if($this->type == "group"){
            return $this->belongsTo('App\Models\Group', 'owner_id', 'id');
        } 
        else return $this->belongsTo('App\User', 'author_id', 'id');
    }

    /**
     * Relation to user
     *
     * @return relation
     */
    public function group()
    { 
        return $this->belongsTo('App\Models\Group', 'owner_id', 'id');
    }

    /**
     * Relation to Diary
     *
     * @return relation
     */
    public function diary()
    {
        return $this->belongsTo('App\Models\Diary', 'owner_id');
    }

    /**
     * Relation to note_image
     *
     * @return relation
     */
    public function images()
    {
        return $this->hasMany('App\NoteImage', 'note_id', 'id');
    }

   /**
     * Relation to note_files
     *
     * @return relation
     */
    public function files()
    {
        return $this->hasMany('App\Models\NoteFile', 'note_id', 'id');
    }

    public function adavai()
    {
        return $this->hasOne('App\Models\NoteAdavai', 'note_id', 'id');
    }

    /**
     * Relation to NoteParam
     *
     * @return relation
     */
    public function params()
    {
        return $this->hasOne('App\Models\NoteParam', 'note_id', 'id');
    }

    /**
     * Relation to note_comment
     *
     * @return relation
     */
    public function comments()
    {
        return $this->hasMany('App\NoteComment', 'note_id', 'id');
    }

    /**
     * Relation to NoteLike
     *
     * @return relation
     */
    public function likes()
    {
        return $this->hasMany('App\NoteLike', 'note_id', 'id');
    }

    /**
     * Relation to AdavaiSubsribers
     *
     * @return relation
     */
    public function adavaiki()
    {
        return $this->hasMany('App\Models\AdavaiSubscriber', 'note_id', 'id');
    }

    public function subscriptions()
    {
        return $this->hasMany('App\Models\AdavaiSubscriber', 'note_id', 'id');
    }
 
    /**
     * Relation to NoteLike
     *
     * @return relation
     */
    public function reposts()
    {
        //return $this->belongsTo('App\Models\Repost', 'id');
        return $this->hasMany('App\Models\Repost', 'note_id', 'id');
    }

	/**
     * Check if user can edit/delete
     *
     * @return boolean
     */
    public function canManage()
    {
        return ($this->author_id == Auth::user()->id || $this->owner_id == Auth::user()->id);
    }
 
    /** 
     * Get the administrator flag for the user.
     *
     * @return bool
     */
    public function getPostedAtAttribute() 
    { 
        Date::setlocale(App::getLocale());
        return Date::parse($this->attributes['created_at'])
                        ->setTimezone(LocaleHelper::userTimeZone())
                        ->format('j F Y H:i');
    }

    /**
     * Get comments count
     *
     * @return bool
     */
    public function getCommentsCountAttribute()
    {
        return $this->comments()->count();
    }

    /**
     * Get likes count
     *
     * @return bool
     */
    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }


    public function getContentShortAttribute()
    {
        return substr($this->content, 0, 100);
    }

    public function getSubscriptionsCountAttribute()
    {
        return $this->subscriptions->count();
    }

    public function scopeWithMainRelations($query)
    {
        return $query->with(['author', 'images', 'owner', 'group', 'files', 'adavai.city', 'diary'])
                     ->with(['adavai' => function($q){
                                $q->withCount('notifications');
                            }])
                     ->withCount(['comments', 'likes', 'subscriptions']);
    }

    /**
     * Delete relation models
     *
     * @return bool
     */
    public static function boot() {
        parent::boot();

        self::deleting(function($item) {
            $item->images()->each(function($img) {
                $img->delete();
            });

            $item->files()->each(function($item) {
                $item->delete();
            });

            $item->likes()->each(function($like) {
                $like->delete();
            });
        });
    }

}
