<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\LocaleHelper;
use Date;
use App;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array 
     */
    protected $fillable = [
        'user_id',
        'product_id'
    ]; 

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * created at.
     *
     * @return date
     */
    public function getOrderDateAttribute()
    { 
        Date::setlocale(App::getLocale());
        return Date::parse($this->attributes['created_at'])
                        ->setTimezone(LocaleHelper::userTimeZone())
                        ->format('j F Y H:i');
    }


}
 