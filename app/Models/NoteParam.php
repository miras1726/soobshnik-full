<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use App\Models\Repost;

class NoteParam extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note_id',
        'goal_reached',
        'is_easy',
        'is_motivation_enough',
    ]; 
 
    /**
     * Relation to Note
     *
     * @return relation
     */ 
    public function note()
    {
        return $this->belongsTo('App\Note', 'note_id');
    }

    /**
     * Define if user can edit | after 24 hours can't
     *
     * @return boolean
     */ 
    public function canEdit()
    {
        return Carbon::parse($this->note->created_at)->diffInHours() < 24;
    }

    /**
     * Reposted
     *
     * @return boolean
     */ 
    public function isReposted()
    {
        return Repost::userReposted($this->note->id);
    }

}
