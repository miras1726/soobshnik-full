<?php
 
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Note;
use App\User;

class MarkedUser extends Model
{	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'marked_user_id',
        'mission_id'
    ];
    
    protected $attributes = [
        'mission_id' => 0
    ];

	public function markedUser()
	{
		return $this->belongsTo('App\User', 'marked_user_id');
	}

    /**
     * Marked users notes
     *
     * @return bool
     */
    public static function notesOfMarkedUsers(User $user)
    {
        $ids = collect();
        $missionIds = $user->load('markedUsers')->markedUsers->pluck('mission_id');
        
        $user->load(['markedUsers.markedUser.addedNotes' => function($q) use ($missionIds){
            $q->withCount('likes')->where(['type' => 'mission'])->whereIn('owner_id', $missionIds);
        }]);

        $user->markedUsers->each(function($item) use ($ids){
            $item->markedUser->addedNotes->each(function($note) use ($ids){
                 $ids->push($note->id);
            });
        });
        return $ids;
    }

    /**
     * Marked users notes
     *
     * @return bool
     */
    public static function markedUserGroupNotes(User $user)
    {
        $ids = collect(); 
        $user->load([
            'markedUsers', 
            'markedUsers.groupNotes' => function($q){
                $q->withCount('likes');
            },
        ]);
        $user->markedUsers->each(function($item) use ($ids){
            $item->groupNotes->each(function($note) use ($ids){
                $ids->push($note->id);
            });
        });
        return $ids;

    }

    public function groupNotes()
    {
        return $this->hasMany('App\Note', 'author_id', 'marked_user_id')->where('type', 'group');
    }

    public static function isMarkedUserForAuthUser($userId, $missionId)
    {
        return self::where(['user_id' => Auth::user()->id, 'marked_user_id' => $userId, 'mission_id' => $missionId])->first() == true;
        
    }

} 