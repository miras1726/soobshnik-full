<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Note;

class Report extends Model
{
    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'item_id',
        'user_id',
        'type',
        'report'
    ];

    /**
     * reported notes
     *
     * @var 
     */
    public static function notes()
    {
        $ids = self::where(['type' => 'note', 'report' => 1, 'type' => 'adavai'])->pluck('item_id')->toArray();
        return Note::withMainRelations()->whereIn('id', $ids)->orderBy('id', 'desc')->get();;
    }  

    /**
     * Badword notes
     * 
     * @var 
     */
    public static function badWordNotes()
    {
        $ids = self::where(['type' => 'note', 'report' => 0])->pluck('item_id')->toArray();
        return Note::with(['author', 'images', 'owner', 'group', 'files'])->whereIn('id', $ids)->orderBy('id', 'desc')->get();;
    }


}
  