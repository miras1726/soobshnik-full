<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserAdavaiProfile;

class AdavaiLicensePurchase extends Model
{
    protected $fillable = [
    	'user_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public static function calculateForThisYear()
    {
    	return self::whereYear('created_at', date('Y'))->count();
    }

    public static function totalPrizeFotThisYear()
    {
    	//$purchases = self::calculateForThisYear();
        $profiles = UserAdavaiProfile::licensed()->count();

    	//$sum = $purchases * 100;

        $sum = (100 * $profiles) / 10;

    	return $profiles ? $sum : 0;
    }
}
 