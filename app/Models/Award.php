<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App;

class Award extends Model
{
    protected $fillable = [
    	'id',
    	'award',
    	'user_id',
    	'data',
    ];

    private static $awardNames = [
    	'best_manager' => 'Организатор года/Organizer of the year',
    	'lucky_manager' => 'Счастливчик года/Lucky organizer',
    ];

    protected $appends = [
    	'name',
    ];

    public static function setWinner(User $user, $award, $data = false)
    {
    	self::deleteWinner($award);
    	return self::create([
    		'award' => $award,
    		'user_id' => $user->id,
            'data' => $data
    	]);
    }

    public static function deleteWinner($award)
    {
    	$winners = self::where('award', $award)->get();
    	if ($winners->count()) {
    		$winners->each(function($item){
    			$item->delete();
    		});
    	}
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function generateName()
    {
    	if(array_key_exists($this->award, self::$awardNames)){
		    return self::$awardNames[$this->award];
		}
		return null;
    }

    public function getNameAttribute()
    {
    	$value = $this->generateName();
        $arr = explode('/', $value);
        if (count($arr) < 2) return $value;
        $locale = App::getLocale();
        if($locale == 'ru') return $arr[0];
        else return $arr[1];
    }

    public function getFeedbackAttribute()
    {
        if (!$this->data) return null;
        $data = json_decode($this->data);
        if (isset($data->feedback)) return $data->feedback;
        return null;
    }

}
