<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AdavaiSubscriber extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array 
     */ 
    protected $fillable = [
        'note_id',
        'user_id' 
     ];

     public static function getUserSubscribedNotes($userId, $active = true, $limit = null)
     {
     	$condition = $active ? '>=' : '<=';
     	$notes = AdavaiSubscriber::where('user_id', Auth::user()->id)
                               ->with(['note.adavaiki', 
                               		   'note.likes', 
                               		   'note.comments', 
                               		   'note.adavai.city',
                               		   'note.adavai.city.country'])
                               ->with(['note.adavai' => function($q) use($condition){
                                    $q->where('date', $condition, date('Y-m-d H:i:s'))->get();
                               }])
                               ->limit($limit)
                               ->get()
                               ->pluck('note');

        return $notes->filter(function($item){
            return $item->adavai == true;
        });
     }

    public function note()
    {
      return $this->belongsTo('App\Note', 'note_id');
    }
    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }

}
 