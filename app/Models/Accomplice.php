<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Mailer;
use App;

class Accomplice extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array 
     */ 
    protected $fillable = [
        'user_id',
        'accomplice_id'
     ];

    /**
     * Relation to User
     *
     * @return relation
     */
    public function user()
    {
    	if($this->accomplice_id == Auth::user()->id){
        	return $this->belongsTo('App\User', 'user_id');
    	}
        return $this->belongsTo('App\User', 'accomplice_id');
    }

    /**
     * get accomplice
     *
     * @return relation
     */
    public function getAccomplice($id)
    {
        if($this->accomplice_id == $id){
            return User::find($this->user_id);
        }
        return User::find($this->accomplice_id);
    }

    /**
     * User complices count
     *
     * @return boolean
     */
    public static function userAccomplices($id)
    {
        return Accomplice::where('user_id', $id)->orWhere('accomplice_id', $id)->get();
    }

    /**
     * Get by ID
     *
     * @return relation
     */
    public function get($id)
    {
    	$userId = Auth::user()->id;
        return Accomplice::where(function ($query) use ($userId) {
			    $query->where('user_id', $userId)
			          ->orWhere('accomplice_id', $userId);
			})->where(function ($query) use ($id){
			    $query->where('user_id', $id)
			          ->orWhere('accomplice_id', $id);
			})->first();
    }

    /**
     * Is complice
     *
     * @return boolean
     */
    public function isAccomplice($id)
    {
    	return $this->get($id) == true;
    }

    /**
     * Send mail to accomplices of user about b-day
     * 
     * @return 
     */
    public static function birthdayMail($user)
    {
        $accomplices = self::where('user_id', $user->id)->orWhere('accomplice_id', $user->id)->distinct()->get(['user_id', 'accomplice_id']);
        if ($accomplices->count() > 0) {
            foreach ($accomplices as $accomplice) {
                $mailer = new Mailer();

                $body = view('site.user.birthday-mail', [
                    'user' => $user, 
                    'type' => __('mail.accomplice')
                ])->render();
                if($user->locale) App::setLocale($user->locale);
                $mailer->send($accomplice->getAccomplice($user->id)->email, __('mail.accomplice.bday.subject'), $body);
            }
        }
    }

}
 