<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use App\Note;
use App\Mailer;
use App\Models\Mission\UserMission;
use App;

class Diary extends Model
{ 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mission_id',
        'user_id',
        'goal',
        'goal_for_week',
        'goal_daily',
        'write_period',
        'email_frequency',
        'should_finished_at'
    ]; 

    /**
     * Relation to User
     *
     * @return relation
     */ 
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Relation to Mission
     *
     * @return relation
     */ 
    public function mission()
    {
        return $this->belongsTo('App\Models\Mission\Mission', 'mission_id');
    }

    /**
     * User mission
     *
     * @return 
     */  
    public function userMission()
    {
        return UserMission::where(['user_id' => $this->user->id, 'mission_id' => $this->mission->id])->first();
    }
 
    /**
     * Diary notes
     *
     * @return relation
     */  
    public function notes()
    {
        return Note::whereHas('diary', function($q){
            $q->where(['type' => 'diary', 'owner_id' => $this->id]);
        })->orderBy('id', 'desc')->get();
    }

    /**
     * Get all finished ones
     *
     * @return query
     */
    public function scopeFinished($query)
    {
        return $query->whereDate('should_finished_at', '<=', Carbon::now());
    }

    /**
     * Get all active ones
     *
     * @return query
     */
    public function scopeActive($query)
    {
        return $query->whereDate('should_finished_at', '>=', Carbon::now());
    }

    /**
     * Relation to Mission
     *
     * @return relation
     */ 
    public function finishingAt()
    {
        if (is_null($this->should_finished_at)) return false;
        
        $date = Carbon::parse($this->should_finished_at);
        
        if ($date->isPast()) return false;
        return $date->diffForHumans();
    }

    /**
     * Calculate progress
     *
     * @return number
     */ 
    public function calculateProgress()
    {
        $from = Carbon::parse($this->created_at);
        $now = Carbon::now();
        $to = Carbon::parse($this->should_finished_at);

        $totalDays = $to->diffInDays($from);
        $passedDays = $from->diffInDays($now);

        if ($totalDays == 0 || $passedDays == 0) return 0;

        $progress = round(($passedDays*100)/$totalDays);
        
        return ($progress >= 100) ? 100 : $progress;
    }

    /**
     * Goal reached progress
     *
     * @return number
     */ 
    public function goalReachedProgress($allTime = false)
    {
        if ($allTime) $from = $this->created_at;
        else $from = Carbon::now()->subDays($this->email_frequency);
        $to = Carbon::now();
        $total = $this->notes()->whereBetween('created_at', [$from, $to]);

        $reached = $total->filter(function($item){
                            return $item->params->goal_reached == 1;
                        })->count();

        if ($reached == 0) return 0;
        
        $progress = round(($reached*100)/$total->count());

        return ($progress >= 100) ? 100 : $progress;        
    }

    /**
     * Is easy progress
     *
     * @return number
     */ 
    public function isEasyProgress($allTime = false)
    {
        if ($allTime) $from = $this->created_at;
        else $from = Carbon::now()->subDays($this->email_frequency);
        $to = Carbon::now();
        $total = $this->notes()->whereBetween('created_at', [$from, $to]);

        $reached = $total->filter(function($item){
                            return $item->params->is_easy == 1;
                        })->count();

        if ($reached == 0) return 0;
        
        $progress = round(($reached*100)/$total->count());
                        
        return ($progress >= 100) ? 100 : $progress;        
    }

    /**
     * Is easy progress
     *
     * @return number
     */ 
    public function motivationProgress($allTime = false)
    {
        if ($allTime) $from = $this->created_at;
        else $from = Carbon::now()->subDays($this->email_frequency);
        $to = Carbon::now();
        $total = $this->notes()->whereBetween('created_at', [$from, $to]);

        $reached = $total->filter(function($item){
                            return $item->params->is_motivation_enough == 1;
                        })->count();

        if ($reached == 0) return 0;
        
        $progress = round(($reached*100)/$total->count());

        return ($progress >= 100) ? 100 : $progress;
    }

    /**
     * Email the user about progress
     *
     * @return 
     */ 
    public function emailUser()
    { 
        $mailer = new Mailer();
        if($this->user->locale) App::setLocale($this->user->locale);
        $body = view('mail.diary-email', [
            'diary'       => $this->mission->title,
            'frequency'   => $this->email_frequency,
            'user'        => $this->user,
            'progress'    => $this->calculateProgress(),
            'goalReached' => $this->goalReachedProgress(false),
            'isEasy'      => $this->isEasyProgress(false),
            'motivation'  => $this->motivationProgress(false),
            'refLink'     => route('register').'/'.$this->user->nickname
        ])->render();
        if ($mailer->send($this->user->email, __('mail.report'), $body)) return true;
    }
 
    /**
     * Email the user to write
     *
     * @return 
     */ 
    public function remindUserToWrite()
    {
        $mailer = new Mailer();
        $body = __('mail.diary.reminder.text', ['name' => $this->user->nickname,'diary' => $this->mission->title]);
        if($this->user->locale) App::setLocale($this->user->locale);
        if ($mailer->send($this->user->email, __('mail.diary.reminder.title'), $body)) return true;
    }

    /**
     * Delete 
     *
     * @return bool
     */
    public static function boot() {
        parent::boot();

        self::deleting(function($item) {
            $item->notes()->each(function($note) {
                $note->delete();
            });
        });
    }
 
}
 
