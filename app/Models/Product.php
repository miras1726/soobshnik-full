<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Croppa;

class Product extends Model
{
	public $defaultAvatar = 'default.png';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'price',
        'price_star',
        'img'
    ];

    /**
     * Avatar
     *
     * @return string 
     */
    public function avatar()
    {
        if ($this->img != '' && !is_null($this->img)) {
            if (Storage::disk('site')->exists('img/products/' . $this->img)) {
                return $this->img;
            } else {
                return $this->defaultAvatar;
            }
        }
        return $this->defaultAvatar;
    }

    /**
     * Check if Group's avatar exists
     *
     * @return boolean
     */
    public function isAvatarExists()
    {
        return ($this->img != '' && !is_null($this->img) && Storage::disk('site')->exists('img/products/' . $this->img));
    }

    public static function delImg($img)
    {
        if(($img != '' && !is_null($img) && Storage::disk('site')->exists('img/products/' . $img)))
        return Croppa::delete('img/products/'.$img);
    }

    /**
     * Delete relation models
     *
     * @return bool
     */
    public static function boot() {
        parent::boot();

        self::deleting(function($item) {
            self::delImg($item->img);
        });
    }

}
 