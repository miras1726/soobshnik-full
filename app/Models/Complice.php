<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Mailer;
use App;

class Complice extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array 
     */ 
    protected $fillable = [
        'mission_id',
        'user_id', 
        'complice_id',
        'user_rating', 
        'complice_rating',
        'accepted'
     ];

    /**
     * Relation to User
     *
     * @return relation
     */
    public function user()
    {
        if($this->complice_id == Auth::user()->id){
        	return $this->belongsTo('App\User', 'user_id');
    	}
        return $this->belongsTo('App\User', 'complice_id');
    }

    /**
     * Relation to User
     *
     * @return relation
     */
    public function getComplice($id)
    {
        if($this->complice_id == $id){
            return User::find($this->user_id);
        }
        return User::find($this->complice_id);
    }

    /**
     * Relation to Mission
     *
     * @return relation
     */
    public function mission()
    {
        return $this->belongsTo('App\Models\Mission\Mission', 'mission_id');
    }

    /**
     * Get by ID
     *
     * @return relation
     */
    public static function get($id, $missionId = false, $withoutUnaccepted = true) 
    {
    	$userId = Auth::user()->id;
        return Complice::where(function ($query) use ($userId) {
			    $query->where('user_id', $userId)
			          ->orWhere('complice_id', $userId);
			})->where(function ($query) use ($id){
			    $query->where('user_id', $id)
			          ->orWhere('complice_id', $id);
			})->where(function($query) use ($missionId, $withoutUnaccepted){
                if($missionId){
                    $query->where('mission_id', $missionId);
                }
                if ($withoutUnaccepted) {
                    $query->where('accepted', 1);
                }
            })->first();
    }

    /**
     * Is complice
     *
     * @return boolean
     */
    public function isComplice($id, $missionId = false, $withoutUnaccepted = true)
    {
    	return self::get($id, $missionId, $withoutUnaccepted) == true;
    }

    /**
     * Is complice
     *
     * @return boolean
     */
    public static function isCompliceForAuthUser($id, $missionId = false, $withoutUnaccepted = true)
    {
        $complice = self::get($id, $missionId, $withoutUnaccepted);
        if (!$complice) return 'none';
        $self = $complice->user_id == Auth::user()->id ? true : false;

        if ($complice->accepted == 1) return 'complice';
        if ($self && $complice->accepted == 0) return 'requested';
        if (!$self && $complice->accepted == 0) return 'waiting';

        return 'none';
    }

    /**
     * Is complice
     *
     * @return boolean
     */
    public function getStatus($id, $missionId = false, $withoutUnaccepted = true)
    {
        $complice = self::get($id, $missionId, $withoutUnaccepted);
        return $complice->accepted;
    }

    /**
     * User complices count
     *
     * @return boolean
     */
    public static function userComplices($id, $missionId = false, $withoutUnaccepted = true)
    {
        return self::where(function($q) use ($id){
            $q->where('user_id', $id)->orWhere('complice_id', $id);
        })->where(function($q) use($missionId, $withoutUnaccepted){
            if($missionId) {
                $q->where('mission_id', $missionId);
            }
            if ($withoutUnaccepted) {
                $q->where('accepted', 1);
            }
        })->orderBy('mission_id')->get();

    }

    /**
     * Send mail to complices of user about b-day
     *
     * @return 
     */ 
    public static function birthdayMail($user)
    {
        $complices = self::where('user_id', $user->id)->orWhere('complice_id', $user->id)->distinct()->get(['user_id', 'complice_id']);
        if ($complices->count() > 0) {
            foreach ($complices as $complice) {
                $mailer = new Mailer();

                $body = view('site.user.birthday-mail', [
                    'user' => $user,
                    'type' => __('mail.complice')
                ])->render();
                if($user->locale) App::setLocale($user->locale);
                $mailer->send($complice->getComplice($user->id)->email, __('mail.complice.bday.subject'), $body);
            }
        }
    }

    /**
     * Identify if given user in user_rating or complice_rating field
     *
     * @return string
     */
    public function identifyUserRatingField($userId)
    {
        if($this->complice_id == $userId){
            return "user_rating";
        }
        return "complice_rating";
    }

    /**
     * Get complice rating
     *
     * @return string
     */
    public function getCompliceRating($userId = false)
    {
        $user = ($userId) ? $userId : Auth::user()->id;
        if($this->complice_id == $user){
            return $this->user_rating;
        }
        return $this->complice_rating;
    }

}
