<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Helpers\LocaleHelper;
use App;
use Date;
use App\Note;
use Carbon\Carbon;

class NoteAdavai extends Model
{
	protected $fillable = [
		'note_id',
		'city_id',
		'category_id',
		'limit',
		'check_age',
		'date',
	];

	protected $appends = [
		'date_formatted',
		'finished',
		'date_for_input',
		'editable_adavai',
		//'notifications_count',
		'notifications_link',
		'check_limit',
	];

	protected $table = 'note_adavai';

	public function note()
	{
		return $this->belongsTo('App\Note');
	}

	public function city()
	{
		return $this->belongsTo('App\City');
	}

	public function category()
	{
		return $this->belongsTo('App\Models\Mission\MissionCategory', 'category_id', 'id');
	}

	public function notifications()
	{
		return $this->hasMany('App\Models\Notification', 'target_id', 'note_id')->where(['type' => 'adavai'])->where(function($q){
			$this->note ? $q->where(['user_id' => $this->note->author_id]) : null;
		});
	}

	public function getDateFormattedAttribute()
	{
		$at = __('note.at');
        Date::setlocale(App::getLocale());
        return Date::parse($this->date)
                        ->setTimezone(LocaleHelper::userTimeZone())
                        ->format('j F Y ' .$at. ' H:i');
	}

	public function getNotificationsCountAttribute()
	{
		return $this->notifications->count();
	}

	public function getNotificationsLinkAttribute()
	{
		return route('notification.list', ['adavai', $this->note_id]);
	}

	public function getDateForInputAttribute()
	{
		return Date::parse($this->date)->format('Y-m-d\TH:i');
	}

	public function getFinishedAttribute()
	{
		$date = Carbon::parse($this->date);
		return Carbon::now()->greaterThan($date);
	}

	public function getEditableAdavaiAttribute()
	{	
		if ($this->finished) return false;
		return true;
	}

	public function getCheckLimitAttribute()
	{
		if ($this->limit <= $this->note->adavai_count) return false;
		return true;
	}


}
