<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

	protected $fillable = ['user_id', 'subject', 'text', 'answer'];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function date()
    {
    	return date('d.m.Y', strtotime($this->created_at));
    }

}
