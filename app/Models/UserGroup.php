<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'group_id'
    ];

    /**
     * Relation to group
     *
     * @return relation
     */ 
    public function group()
    {
    	return $this->belongsTo('App\Models\Group', 'group_id');
    }

    /**
     * Relation to user
     *
     * @return relation
     */ 
    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id');
    }

}
