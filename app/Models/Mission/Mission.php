<?php

namespace App\Models\Mission;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Cookie;
use Illuminate\Support\Carbon;
use App;
use Croppa;
use App\Note;

class Mission extends Model 
{
	public $defaultAvatar = 'default.png'; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'category_id',
        'description',
        'motivation',
        'img'
    ];  

    /**
     * Relation to mission
     * 
     * @return relation
     */ 
    public function category()
    {
    	return $this->belongsTo('App\Models\Mission\MissionCategory', 'category_id');
    }
 
    /**
     * Relation to UserMissions
     *
     * @return relation
     */
    public function userMissions()
    {
        return $this->hasMany('App\Models\Mission\UserMission', 'mission_id');
    }

    /**
     * Relation to MissionLink
     *
     * @return relation
     */
    public function links()
    {
        return $this->hasMany('App\Models\Mission\MissionLink', 'mission_id', 'id');
    }

    /**
     * Relation to Group
     *
     * @return relation
     */
    public function groups()
    {
        return $this->hasMany('App\Models\Group', 'mission_id', 'id');
    }

    public function photoReports()
    {
        return Note::where(['owner_id' => $this->id, 'type' => 'photo-report'])->get();
    }

    /**
     * Get title attribute
     *
     * @return string
     */
    public function getTitleOriginalAttribute()
    {
        return $this->attributes["title"];
    }

    /**
     * Mutate title attribute. Translate
     *
     * @return string
     */
    public function getTitleAttribute($value)
    {
        $arr = explode('/', $value);
        if (count($arr) < 2) return $value;
        $locale = App::getLocale();
        if($locale == 'ru') return $arr[0];
        else return $arr[1];
    }

     /**
     * User's avatar
     *
     * @return string
     */
    public function avatar()
    {
        if ($this->img != '' && !is_null($this->img)) {
            if (Storage::disk('site')->exists('img/missions/' . $this->img)) {
                return $this->img;
            } else {
                return $this->defaultAvatar;
            }
        }
        return $this->defaultAvatar;
    }

     /**
     * Generate motivation
     *
     * @return string
     */
    public function motivation()
    {
        if (!$this->attributes["motivation"]) return null;
        $t = Carbon::tomorrow()->diffInMinutes(Carbon::now());

        if(!Cookie::has('motivation_' . Auth::user()->id . '_' . $this->id)){
            Cookie::queue(Cookie::make('motivation_' . Auth::user()->id . '_' . $this->id, md5(time()), $t));
        }

        return view('site.mission.motivation', [
            'id' => $this->id,
            'title' => $this->title,
            'motivation' => $this->motivation
        ])->render();
    }

    /**
     * Get title attribute
     *
     * @return string
     */
    public function getMotivationOriginalAttribute()
    {
        return $this->attributes["motivation"];
    }

    /**
     * Mutate title attribute. Translate
     *
     * @return string
     */
    public function getMotivationAttribute($value)
    {
        $arr = explode('/', $value);
        if (count($arr) < 2) return $value;
        $locale = App::getLocale();
        if($locale == 'ru') return $arr[0];
        else return $arr[1];
    }

    public static function setImg($image)
    {
        $newFileName = md5(microtime()).'.'
                               .$image->getClientOriginalExtension();
        Storage::disk('site')->put('/img/missions/'.$newFileName, File::get($image));

        return $newFileName;
    }

    public static function delImg($img)
    {
        if(($img != '' && !is_null($img) && Storage::disk('site')->exists('img/missions/' . $img)))
        return Croppa::delete('img/missions/'.$img);
    }

    /**
     * Delete relation models
     *
     * @return bool
     */
    public static function boot() {
        parent::boot();

        self::deleting(function($item) {
            self::delImg($item->img);
        });
    }

}
