<?php

namespace App\Models\Mission;

use Illuminate\Database\Eloquent\Model;
use App;

class MissionLink extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'mission_id',
        'link'
    ]; 

	public function mission()
	{
		return $this->belongsTo('App\Models\Mission\Mission', 'mission_id', 'id');
	}

    /**
     * Get title attribute
     *
     * @return string
     */
    public function getTitleOriginalAttribute()
    {
        return $this->attributes["title"];
    }

    /**
     * Mutate title attribute. Translate
     *
     * @return string
     */
    public function getTitleAttribute($value)
    {
        $arr = explode('/', $value);
        if (count($arr) < 2) return $value;
        $locale = App::getLocale();
        if($locale == 'ru') return $arr[0];
        else return $arr[1];
    }
	
}
