<?php

namespace App\Models\Mission;

use Illuminate\Database\Eloquent\Model;
use App;

class MissionCategory extends Model
{
    /**
     * Relation to mission
     *
     * @return relation
     */ 
    public function missions()
    {
    	return $this->hasMany('App\Models\Mission\Mission', 'category_id');
    }

    /**
     * Get title attribute
     *
     * @return string
     */
    public function getTitleOriginalAttribute()
    {
        return $this->attributes["title"];
    }

    /**
     * Mutate title attribute. Translate
     *
     * @return string
     */
    public function getTitleAttribute($value)
    {
        $arr = explode('/', $value);
        if (count($arr) < 2) return $value;
        $locale = App::getLocale();
        if($locale == 'ru') return $arr[0];
        else return $arr[1];
    }

}
