<?php

namespace App\Models\Mission;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Complice;
use App\Models\Diary;

class UserMission extends Model
{

	protected $fillable = [
            'user_id', 
            'mission_id',
            'status',
            'review',
            'finished_at', 
            'motivation',
            'support',
            'punctuality',
            'communication',
            'completed',
            'completed_time'
        ];

    /**
     * Relation to mission
     *
     * @return relation
     */ 
    public function mission()
    {
    	return $this->belongsTo('App\Models\Mission\Mission', 'mission_id');
    }

    /**
     * Relation to user
     *
     * @return relation
     */ 
    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Relation to user
     *
     * @return relation
     */ 
    public function users()
    {
        return $this->hasMany('App\User', 'id');
    }

    /**
     * Relation to user
     *
     * @return relation
     */ 
    public function authUserComplices()
    {
        return Complice::userComplices(Auth::user()->id, $this->mission_id); 
    }

    /**
     * Relation to user
     *
     * @return relation
     */ 
    public function authUserMissionComplicesCount($missionId) 
    {
        return Complice::userComplices(Auth::user()->id, $missionId, false)->count(); 
    }

    /**
     * Related diary
     *
     * @return object
     */ 
    public function diary()
    {
        return Diary::where(['mission_id' => $this->mission_id, 'user_id' => Auth::user()->id])->first();
    }

}
