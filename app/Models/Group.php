<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Croppa;

class Group extends Model
{ 

	public $defaultAvatar = 'default.png';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array 
     */
    protected $fillable = [
        'title',
        'author_id',
        'description',
        'img',
        'mission_id',
    ];

    /**
     * The accessors to append to the model's array form.
     * 
     * @var array
     */
    protected $appends = ['thumbnail', 'user_name', 'avatar_thumb'];

    /**
     * Relation to UserGroup
     *
     * @return relation
     */ 
    public function subscribers()
    {
        return $this->hasMany('App\Models\UserGroup', 'group_id');
    }

    public function notes()
    {
        return $this->hasMany('App\Note', 'owner_id', 'id');
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id', 'id');
    }

    public function mission()
    {
        return $this->belongsTo('App\Mission\Mission', 'mission_id', 'id');
    }

    /**
     * Group's avatar
     *
     * @return string
     */
    public function avatar()
    {
        if ($this->img != '' && !is_null($this->img)) {
            if (Storage::disk('site')->exists('img/groups/' . $this->img)) {
                return $this->img;
            } else {
                return $this->defaultAvatar;
            }
        }
        return $this->defaultAvatar;
    }

    /** 
     * Get the thumb.
     *
     * @return string
     */
    public function getThumbnailAttribute()
    {
        return Croppa::url('public/img/groups/'.$this->avatar(), 60, 60);
    }

    /** 
     * Get the thumb.
     *
     * @return string
     */
    public function getUserNameAttribute()
    {
        return $this->attributes['title'];
    }

    /**
     * Get thumb.
     *
     * @return bool
     */
    public function getAvatarThumbAttribute()
    {
        return Croppa::url('public/img/groups/'.$this->avatar(), 60, 60);
    }

    /**
     * Subscribers count.
     *
     * @return string
     */
    public function subscribersCount()
    {
        return $this->subscribers->count();
    }

    /**
     * Check if current user is admin of this group.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return ($this->author_id == Auth::user()->id) ? 1 : 0;
    }

    /**
     * Check if Group's avatar exists
     *
     * @return boolean
     */
    public function isAvatarExists()
    {
        return ($this->img != '' && !is_null($this->img) && Storage::disk('site')->exists('img/groups/' . $this->img));
    }

    public static function setImg($image)
    {
        $newFileName = md5(microtime()).'.'
                               .$image->getClientOriginalExtension();
        Storage::disk('site')->put('/img/groups/'.$newFileName, File::get($image));

        return $newFileName;
    }

    public static function delImg($img)
    {
        if(($img != '' && !is_null($img) && Storage::disk('site')->exists('img/groups/' . $img)))
        return Croppa::delete('img/groups/'.$img);
    }

    /**
     * Delete relation models
     *
     * @return bool
     */
    public static function boot() {
        parent::boot();

        self::deleting(function($item) {
            $item->subscribers()->each(function($subscriber) {
                $subscriber->delete();
            });

            self::delImg($item->img);
        });
    }

}
