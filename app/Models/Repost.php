<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Note;

class Repost extends Model
{ 

    /** 
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'note_id'
    ]; 

	public function note()
	{
		return $this->hasOne('App\Note', 'id', 'note_id');
	}

	public function user()
	{
		return $this->belongsTo('App\Note', 'user_id');
	}

    /**
     * Check if auth user repost this note
     *
     * @return 
     */ 
    public static function userReposted($noteId)
    {
    	$repost = self::where(['user_id' => Auth::user()->id, 'note_id' => $noteId])->first();
    	return $repost == true;
    }

    /**
     *  Get user reposted notes
     *
     * @return 
     */ 
    public static function userRepostedNotes($userId)
    { 
        $relations = ['note', 'note.group', 'note.author', 'note.images', 'note.adavai', 'note.adavai.city', 'note.diary'];

        $notes = self::where('user_id', $userId)->with($relations)->get();

        return $notes->map(function($item){
            $note = clone $item->note;
            $note['repost'] = $item->id;
            return $note;
        });
    }

    public static function userReposts($userId)
    {
        return self::where('user_id', $userId)->get();
        //return self::where('user_id', $userId)->get(['id', 'note_id'])->pluck('note_id', 'id');
    }

/*
    public function getNotes(int $userNotes, Request $request)
    { 
        $reposts = collect();
        $reposts = Repost::userReposts($request->owner_id);

        $notes = Note::where(['owner_id' => $request->owner_id, 'type' => 'user'])
            ->whereIn('id', $reposts, 'or')
            ->get(['id']);

        $ids = $reposts->merge($notes->pluck('id'));

        $notes = Note::whereIn('id', $ids)
            ->where(function($q) use($request) {
                $request->search ? $q->where('content', 'like', '%'.$request->search_input.'%') : null;
            })
            ->withMainRelations()
            ->orderBy('id', 'desc')
            ->paginate(5);

        if ($userNotes) {
            $notes = $notes->filter(function($note){
                return $note->author_id == Auth::user()->id;
            });
        }

        $notes = NoteHelper::userLikesAndAdavais($notes);
        $notes = NoteHelper::markUserReposts($notes, $reposts);

        return response()->json([
            'notes' => $notes->values()->toArray()
        ], 200);
    }
*/

}
