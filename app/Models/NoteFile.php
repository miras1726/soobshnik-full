<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class NoteFile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note_id', 
        'file',  
        'name',  
    ];

    protected static $path = 'downloads/notes/';
    protected static $accessPath = 'public/downloads/notes/';

    protected $appends = ['short_name', 'file_path'];

    /**
     * Relation to note
     *
     * @return relation
     */
    public function note()
    {
        return $this->belongsTo('App\Note', 'note_id', 'id');
    }

    /**
     * Get file path
     *
     * @return bool
     */
    public function getFilePathAttribute()
    {
        return self::$accessPath.$this->file;
    }

    /**
     * Get short name
     * 
     * @return string
     */
    public function getShortNameAttribute()
    {
    	if (strlen($this->name) <= 10) return $this->name;
        return substr($this->name, 0, 10).'...';
    }

    public static function boot() {
        parent::boot();

        self::deleting(function($item) { // before delete() method call this
            $file = self::$path.$item->file;
            if($item->file != '' && !is_null($item->file) && Storage::disk('site')->exists($file)){
                Storage::disk('site')->delete($file);
            }
        });
    }

}
 