<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\AdavaiLicensePurchase;

class UserAdavaiProfile extends Model
{
	protected $fillable = [
		'user_id',
		'rating',
		'full_address',
		'passport',
		'verified',
		'enable_notifications',
		'auto_prolong',
		'license',
	];

	protected $attributes = [
		'enable_notifications' => 0,
	];

	protected $dates = [
		'license'
	];

	protected $appends = [
		'active_license'
	];

	protected $table = 'user_adavai_profile';

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function passportExists()
	{
		if ($this->passport && Storage::disk('site')->exists('img/users/passports/' . $this->passport)) return true;
		return false;
	}

	public function getPassport()
	{
		return 	'/public/img/users/passports/' . $this->passport;
	}

	public function deletePassportImg()
	{
		if ($this->passportExists()) {
			Storage::disk('site')->delete('img/users/passports/'.$this->passport);
		}
	}

	public function getActiveLicenseAttribute()
	{
		if (is_null($this->license)) return false;
		return $this->license->addMonth() >= now();
	}

	public function getNotesTotalLikesAttribute()
	{
		if (!$this->user) return 0;
		return $this->user->addedNotes()->where('type', 'adavai')->get()->sum('likes_count');
	}

	public function getForConfirmationAttribute()
	{
		return ($this->full_address && $this->passportExists() && !$this->verified) ? true : false;
	}

    public function setRating($rating)
    {
        $newRating = ($this->rating + ($rating * 0.10));
        
        return $this->update([
            'rating' => $newRating,
        ]);
    }

    public function buyLicense()
    {
    	if ($this->user->balance < 100) return false;
    	$balance = $this->user->update([
            'balance' => ($this->user->balance - 100)
        ]);
    	$balance ? AdavaiLicensePurchase::create(['user_id' => $this->user_id]) : null;
        return $balance ? $this->update([
            'license' => now(),
        ]) : null;
    }

    public function confirmProfile()
    {
    	return $this->update([
    		'verified' => 1
    	]);
    }

    public function scopeVerified($query, $active = 1)
    {
    	return $query->where('verified', $active)->whereNotNull('full_address')->whereNotNull('passport');
    }

    public function scopeLicensed($query)
    {
    	return $query->whereDate('license', '>=', now()->subMonth());
    }
}
