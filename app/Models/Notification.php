<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class Notification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'text',
        'link',
        'type',
        'target_id',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function date()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }
 
}
