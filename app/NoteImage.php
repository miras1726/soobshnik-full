<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Croppa;

class NoteImage extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note_id', 
        'img'  
    ];

    protected $path = '/public/img/notes/';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['thumbnail', 'full_image'];

    /**
     * Relation to note
     *
     * @return relation
     */
    public function note()
    {
        return $this->belongsTo('App\Note', 'note_id', 'id');
    }

    /**
     * Generate thumb
     *
     * @return bool
     */
    public function getThumbnailAttribute()
    {
        return Croppa::url('public/img/notes/'.$this->img, 200, 200);
    }

    /**
     * Get full image
     *
     * @return bool
     */
    public function getFullImageAttribute()
    {
        return $this->path.$this->img;
    }

    public static function boot() {
        parent::boot();

        self::deleting(function($item) {
            $img = $item->full_image;
            if($item->img != '' && !is_null($item->img) && Storage::disk('site')->exists($img)){
                Croppa::delete($img);
            }
        });
    }

}
  