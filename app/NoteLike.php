<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteLike extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note_id',
        'user_id'
    ];
    
    /**
     * Relation to user
     *
     * @return relation
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Relation to note
     *
     * @return relation
     */
    public function note()
    {
        return $this->belongsTo('App\Note')->with('author', 'owner', 'images', 'group');
    }

}
 