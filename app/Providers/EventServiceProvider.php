<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Events\BadWordFound;
use App\Events\NoteWasLiked;
use App\Events\CompliceAdded;
use App\Events\CompliceRefused;
use App\Events\AdavaiCreated;
use App\Events\UserJoinedToAdavai;
use App\Events\NotesFetched;
use App\Listeners\BadWordWarning;
use App\Listeners\DoAboutLikedNote;
use App\Listeners\NotifyCompliceRequest;
use App\Listeners\NotifyRefusedComplice;
use App\Listeners\NotifyAdavaiUsers;
use App\Listeners\NotifyAdavaiManager;
use App\Listeners\IncrementNotesView;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        BadWordFound::class => [
            BadWordWarning::class,
        ],
        NoteWasLiked::class => [
            DoAboutLikedNote::class,
        ],
        CompliceAdded::class => [
            NotifyCompliceRequest::class,
        ],
        CompliceRefused::class => [
            NotifyRefusedComplice::class,
        ],
        AdavaiCreated::class => [
            NotifyAdavaiUsers::class,
        ],
        UserJoinedToAdavai::class => [
            NotifyAdavaiManager::class,
        ],
        NotesFetched::class => [
            IncrementNotesView::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
