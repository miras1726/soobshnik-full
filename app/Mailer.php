<?php

namespace App;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer
{
    // soobsnik.info@gmail.com
    // soobsnik2020
	private $sender = 'soobsnik.info@gmail.com';
	public static $adminEmail = 'info@m-valiev.ru';            

    public function send($to, $subject, $body, $replyTo = false)
    {
        $mail = new PHPMailer(true); 
        $mail->CharSet = 'UTF-8';
        $mail->Encoding = 'base64';

        $mail->SMTPDebug = false;                                     
        $mail->isSMTP();                   
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->Host       = 'smtp.gmail.com';  
        $mail->SMTPAuth   = true;                                       
        $mail->Username   = 'soobsnik.info@gmail.com';                   
        $mail->Password   = 'soobsnik2020';                              
        $mail->SMTPSecure = 'tls';                            
        $mail->Port       = 587;      



    	$mail->setFrom($this->sender);
    	$mail->addAddress($to);
        if ($replyTo) $mail->addReplyTo($replyTo);
    	$mail->isHTML(true);                                 
	    $mail->Subject = $subject;
	    $mail->Body    = $body;
	    return $mail->send();
    }
}
  