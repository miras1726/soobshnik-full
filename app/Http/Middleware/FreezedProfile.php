<?php

namespace App\Http\Middleware;

use Closure;

class FreezedProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->freezed == 1) return redirect()->route('profile.retrieve.get');
        return $next($request);
    }
}
 