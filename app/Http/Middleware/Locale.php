<?php

namespace App\Http\Middleware;
use Closure;
use App;
use Cookie;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next 
     * @return mixed
     */
    public function handle($request, Closure $next)
    {    
        $raw_locale = Cookie::get('locale');  
        $locales = ['en', 'ru'];                

        if (in_array($raw_locale, $locales)) {   
            $locale = $raw_locale;                        
        }                                                    
        else {                    
            $currentDomain = $request->getHttpHost();
            $locale = ($currentDomain == env('DOMAIN_ENG')) ? 'en' : config('app.locale');                
        }

        App::setLocale($locale);                                 

        return $next($request);                                  
    }
}
