<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Illuminate\Support\Facades\Auth;

class LastSeen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return $next($request);
        }  

        $user = Auth::user();
        if($user->locale) App::setLocale($user->locale);   
      
        $user->update([
            'last_seen' => new \DateTime(),
        ]);
        return $next($request);
    } 
}
