<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiaryAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            'mission_id' => 'required|integer',
            'goal' => 'required|min:3',
            'goal_for_week' => 'required|min:3',
            'goal_daily' => 'required|min:3',
            'write_period' => 'required|in:1,7',
            'email_frequency' => 'required|in:7,30',
            'should_finished_at' => 'required|date'
        ];
    }
}
