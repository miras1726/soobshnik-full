<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckUserDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'nullable|email|unique:users',
            'nickname' => 'nullable|unique:users|regex:/^[a-z0-9_]*$/'
        ];
    }

    /**
     * Redetermine validate messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nickname.regex' => 'Псевдоним должен состоять из латинских букв, цифр и нижнего подчеркивания(в нижнем регистре)'
        ];
    }
}
