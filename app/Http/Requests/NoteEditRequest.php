<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoteEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required',
            'img.*' => 'nullable|file|mimes:jpg,jpeg,png,JPEG,gif|max:10240',
            'documents.*' => 'nullable|file|mimes:docx,pdf,epub,fb2,txt|max:10240'

        ];
    }

    public function messages()
    {
        return [
            'img.*.mimes' => __('error.mimes.error'),
            'img.*.mimes' => __('error.file.size.error'),
            'documents.*.mimes' => __('error.mimes.error'),
            'documents.*.size' => __('error.file.size.error'),
        ];
    }
}
