<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */ 
    public function rules()
    {
        return [
            'nickname' => 'nullable',
            'name' => 'nullable',
            'gender' => 'nullable|in:male,female,',
            'city' => 'required',
            'info' => 'nullable',
            'quote' => 'nullable',
            'born' => 'nullable',
            'skype' => 'nullable',
            'locale' => 'required|in:en,ru',
            'password' => 'nullable|min:6',
            'password_confirmation' => 'same:password'
        ];
    }
}
