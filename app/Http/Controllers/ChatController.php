<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Musonza\Chat\Models\MessageNotification;
use App\Models\Mission\Mission;
use App\Models\Complice;
use Musonza\Chat\Models\Conversation;
use Chat;
use App; 
use Date; 
use App\User; 
use App\Helpers\LocaleHelper;
use App\Models\MarkedUser;
use App\Note;


class ChatController extends Controller
{ 
    /**
     * Create a new controller instance. 
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
        Date::setlocale(App::getLocale());
    }

    /**
     * Chat page
     *
     * @return void
     */
    public function chat($missionId)
    {
        $mission = Mission::findOrFail($missionId);
        
        $isInMission = Auth::user()->missions()->where(['mission_id' => $mission->id, 'status' => 'active'])->first();

        if(!Auth::user()->isAdmin() && !$isInMission) return redirect()->route('home');

        $conversation = Conversation::where('data', 'regexp', '"mission":'.$mission->id.'[^0-9]')->first();

        if (!$conversation) {
            $participants = [Auth::user()->id];
            $conversation = Chat::createConversation($participants)->makePrivate(false);
            $conversation->update(['data' => ['mission' => $mission->id]]);
        } else {
            $user = $conversation->users()->where('id', Auth::user()->id)->first();
            if (!$user) Chat::conversation($conversation)->addParticipants(Auth::user());
        }

    	return view('site.chat.chat', [
            'conversation' => $conversation,
            'mission' => $mission,
            'authUserInMission' => $isInMission
        ]); 
    }

    /**
     * Get messages of conversation
     *
     * @return void
     */
    public function getMessages($id)
    {
    	$conversation = Chat::conversations()->getById($id);

        $messages = Chat::conversation($conversation)->setUser(Auth::user())->setPaginationParams(['perPage' => 10000000])->getMessages();

    	$messages = self::transformMessages($messages);

    	$messages = $this->signUnread($conversation, $messages);

    	return response()->json($messages->items());
    }

    /**
     * Identify unread messages position if present
     *
     * @return void
     */
    public function signUnread($conversation, $messages)
    {
    	$unread = MessageNotification::where(['conversation_id' => $conversation->id,
    									 'is_seen' => 0, 
    									 'user_id' => Auth::user()->id])->first();

    	if($unread){
	    	$messages->map(function($item) use ($unread){
	    		if ($item->id == $unread->message_id) $item->ref = 'unread_start';
	    	});
    		Chat::conversation($conversation)->setUser(Auth::user())->readAll();
		}
		return $messages;
    }

    /**
     * Get users of conversation
     *
     * @return void
     */
    public function getUsers($id, $missionId)
    {
        $admin = User::admin();
        if ($admin) {
            $isInMission = $admin->missions()->where(['mission_id' => $missionId, 'status' => 'active'])->first() ?? null;
        }
        
    	$conversation = Chat::conversations()->getById($id); 

        $users = $conversation->users()->get();

        $users->map(function($c) use ($missionId, $admin, $isInMission){ 
            
            if ($c->id != Auth::user()->id) {
                $c->marked = MarkedUser::isMarkedUserForAuthUser($c->id, $missionId);
                $c->complice = Complice::isCompliceForAuthUser($c->id, $missionId, false);
            } else {
                $c->marked = 'self';
            }

            if ($c->admin && $admin && $isInMission) $c->in_mission = true;     
 
        });

    	return response()->json($users->toArray());
    }

    /**
     * Get users of conversation
     *
     * @return void
     */
    public function sendMessage(Request $request)
    {
        $conversation = Chat::conversations()->getById($request->conversation);

        $message = Chat::message($request->body)
            ->from(Auth::user())
            ->to($conversation)
            ->send();
        $message->pretty_date = Date::parse($message->created_at)
                                    ->setTimezone(LocaleHelper::userTimeZone())
                                    ->format('j F Y H:i');

        return response()->json($message->toArray());
    }

    /**
     * Mark the message as read
     *
     * @return void
     */
    public function messageMarkAsRead($id)
    {
        $message = Chat::messages()->getById($id);
        Chat::message($message)->setUser(Auth::user())->markRead();
    }

    /**
     * Clear whole history for user
     *
     * @return void
     */
    public function messageClear($id)
    {
        $conversation = Chat::conversations()->getById($id);
        Chat::conversation($conversation)->setUser(Auth::user())->clear();
        return response()->json(['clear' => 1]);
    }

    /**
     * Transform outgoing messages
     *
     * @return collection
     */
    public static function transformMessages($messages)
    {
    	if($messages->count() > 0){
	    	$messages->map(function($item){
	    		$item->pretty_date = Date::parse($item->created_at)
                                    ->setTimezone(LocaleHelper::userTimeZone())
                                    ->format('j F Y H:i');
	    	});
		}
		return $messages;
    }

    public function sendNoteAsMessage(int $id, Request $request)
    {
        $note = Note::findOrFail($id);
        
        $chats = collect();
        foreach ($request->chats as $chat) {
            $con = Chat::conversations()->getById($chat);
            $con ? $chats->push($con) : null;    
        }
        $body =  json_encode([
            'note_id' => $note->id,
            'content_short' => $note->content_short,
            'url' => route('adavai.list', ['note' => $note->id]),
        ], JSON_UNESCAPED_UNICODE);

        foreach ($chats as $chat) {
            Chat::message($body)
                ->from(Auth::user())
                ->to($chat)
                ->type('adavai')
                ->send();
        }
        return response()->json([
            'sent' => true
        ]);
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/chat/mission/{missionId}', 'ChatController@chat')->name('chat');
        Route::get('/chat/get/messages/{id}', 'ChatController@getMessages')->name('chat.get.messages');
        Route::get('/chat/get/users/{id}/{missionId}', 'ChatController@getUsers')->name('chat.get.users');
        Route::post('/chat/send/message', 'ChatController@sendMessage')->name('chat.send.message');
        Route::get('/chat/message/markread/{id}', 'ChatController@messageMarkAsRead')->name('chat.message.mark.read');
        Route::get('/chat/message/clear/{id}', 'ChatController@messageClear')->name('chat.message.clear');
        Route::post('/chat/share/note/{id}', 'ChatController@sendNoteAsMessage')->name('chat.share.note');
    }

}
