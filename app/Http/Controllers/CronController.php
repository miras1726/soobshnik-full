<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Complice;
use App\Models\Accomplice;
use App\Models\Diary;
use App\Models\Award;
use App\Models\UserAdavaiProfile;
use App\Note;
use Illuminate\Support\Carbon;

class CronController extends Controller
{
    /*public static function adavaiManagerAward()
    {
        if (date('d-m') != '31-12') exit();
        $profiles = UserAdavaiProfile::whereHas('user')->with('user')->get();
        $profile = $profiles->where('notes_total_likes', $profiles->max('notes_total_likes'))->first();
        if ($profile->user) {
            Award::setWinner($profile->user, 'best_manager');
        }
    }*/

    public static function prolongLicencies()
    {
        $profiles = UserAdavaiProfile::whereHas('user')
                                        ->with('user')
                                        ->whereNotNull('license')
                                        ->where('auto_prolong', 1)
                                        ->get();
        $inactive = $profiles->where('active_license', false);
        $inactive->each(function($item){
            if (!$item->user->checkBalanceFor(100)) return true;
            $item->buyLicense();
        });
    }

    /**
     * Diary email reminder
     *
     * @return
     */
    public static function diaryReminder($period)
    {
        $today = Carbon::today();
        $now = Carbon::now();
        $diaries = Diary::whereDate('should_finished_at', '>=', $today->toDateString())->where('write_period', $period)->get();

        if ($diaries->count() > 0) {
            foreach ($diaries as $d) {
                if($period == 7){
                    $notesCount = Note::where(['type' => 'diary', 'owner_id' => $d->id])->whereRaw('created_at + interval 7 day >= ?', [$now])->count();
                }else{
                    $notesCount =  Note::where(['type' => 'diary', 'owner_id' => $d->id])->whereDate('created_at', $today)->count();
                }
                if ($notesCount == 0) {
                    $d->remindUserToWrite();
                }
            }
        }
    }

    /**
     * birthdayCheck
     *
     * @return redirect
     */
    public static function birthdayCheck()
    {
        $date = Carbon::today();
        $users = User::whereMonth('born', '=', $date->month)->whereDay('born', '=', $date->day)->get();

        if ($users->count() > 0) {
            foreach ($users as $user) {
                Complice::birthdayMail($user);
                Accomplice::birthdayMail($user);
            }
        }
    }

    /**
     * Check for deleting
     *
     * @return redirect
     */
    public static function profileCheck()
    {
        $now = Carbon::now();
        $users = User::where('freezed', 0)->whereRaw('last_seen + interval 30 day <= ?', [$now])->get();

        $today = Carbon::today();

        if ($users->count() > 0) {
            foreach ($users as $user) {
                if (!$user->admin) {
                    if (Carbon::parse($user->last_seen)->addDays(37)->diffInDays($today) == 0){
                        $user->update(['freezed' => 1]);
                    }elseif(Carbon::parse($user->last_seen)->addDays(30)->diffInDays($today) == 0){
                        $user->notifyAboutFreezing();
                    }
                }
            }
        }

    }

    /**
     * Diary emailing
     *
     * @return
     */
    public static function diaryEmailing($frequency)
    {
        $diaries = Diary::where('email_frequency', $frequency)->where('should_finished_at', '>=', Carbon::now()->toDateString())->get();
        if ($diaries->count() > 0) {
            foreach ($diaries as $d) {
                $d->emailUser();
            }
        }

    }

}
