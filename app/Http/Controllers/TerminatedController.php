<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Complice;
use App\Models\Accomplice;
use App\Models\Diary;
use App\Models\Award;
use App\Models\UserAdavaiProfile;
use App\Note;
use Illuminate\Support\Carbon;
use Cookie;
use Artisan;
use App\Models\MarkedUser; 
use App\Models\Repost;
use Chat;
use Musonza\Chat\Models\MessageNotification;
use App\Helpers\RobokassaHelper;

class TerminatedController extends Controller 
{
    /** 
     * Delete profile
     *
     * @return redirect
     */
    public function getProfileRetrieve()
    {
        return view('site.user.profile-retrieve');
    }

    /**
     * Delete profile
     * 
     * @return redirect
     */
    public function postProfileRetrieve()
    {
        Auth::user()->update(['freezed' => 0]);
        return redirect()->route('userProfile', [Auth::user()->nickname]);
    }

    public function setLocale($locale){
        if (in_array($locale, ['en', 'ru'])) {
            if(Auth::check()){
                Auth::user()->update(['locale' => $locale]);
            }
            Cookie::queue(Cookie::forever('locale', $locale));  
        }

        return redirect()->back();
    }

    public function updateLangJs(){
        Artisan::call('lang:js', ['--no-lib' => true]);
        return redirect()->back();
    }


    public function shareNote(int $id)
    {
        $note = Note::findOrFail($id);
        return view('layouts.share', [
            'note' => $note
        ]);
    }

    public function robokassaPayment(Request $request)
    {
        $mrhPass2 = RobokassaHelper::getPasswordSecond($request->getHttpHost());

        $userId = $request->Shp_user_id;
        $user = User::findOrFail($userId);
        $outSum = $request->OutSum;
        $invId = $request->InvId;

        $signature = strtoupper($request->SignatureValue);
        $mySignature = strtoupper(md5("$outSum:$invId:$mrhPass2:Shp_user_id=$user->id"));

        if ($mySignature != $signature)
        {
          echo "bad sign\n";
          exit();
        }

        if ($user->setMoney($outSum)) {
            $user->transactions()->create([
                'public_id' => 'robokassa',
                'amount' => $outSum
            ]);
            echo "OK$invId\n";
        }
    }


    /**
     * Prevent using io.
     *
     * @return view
     */ 
    public function ieDeny()
    {
        return view('ie_prevent');
    }

    /**
     * Define routes
     *
     * @return void 
     */
    public static function routes()
    {
        Route::post('/robokassa/payment', 'TerminatedController@robokassaPayment')->name('terminated.robokassa.payment');
        Route::get('/share/note/{id}', 'TerminatedController@shareNote')->name('terminated.share.note'); 
        Route::get('/profile/retrieve', 'TerminatedController@getProfileRetrieve')->name('profile.retrieve.get');
        Route::post('/profile/retrieve', 'TerminatedController@postProfileRetrieve')->name('profile.retrieve.post');
        Route::get('/setlocale/{locale}', 'TerminatedController@setLocale')->name('set.locale'); 
        Route::get('/update/lang/js', 'TerminatedController@updateLangJs');
        Route::get('/ie/deny', 'TerminatedController@ieDeny')->name('ie.deny'); 
    }
}
 