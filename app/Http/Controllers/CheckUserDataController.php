<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\CheckUserDataRequest;
use Illuminate\Support\Facades\Auth;
use Croppa;

class CheckUserDataController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Get check user data
     *
     * @return redirect
     */
    public function getCheckUserData()
    {
    	if (Auth::user()->email && Auth::user()->nickname) return redirect()->route('home');
        return view('auth.check-user-data');
    }

    /**
     * Post check user data
     *
     * @return redirect
     */
    public function postCheckUserData(CheckUserDataRequest $request)
    {
        Auth::user()->update($request->all());
        return redirect()->route('home');
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/check/user/data', 'CheckUserDataController@getCheckUserData')->name('check.user.data');
        Route::post('/check/user/data', 'CheckUserDataController@postCheckUserData')->name('check.user.data.post');
    }
}
