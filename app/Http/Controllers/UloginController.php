<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash; 
use App\User;
use Illuminate\Support\Carbon; 

class UloginController extends Controller
{


    public function login(Request $request)
    {
        // Get information about user.
        $data = file_get_contents('http://ulogin.ru/token.php?token=' . $request->token . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($data, TRUE);

        $authUser = User::where('identity', $user['identity'])->first();

        if (!isset($authUser->id)) {

	        
        	if ($user["email"] && User::emailExists($user["email"])) {
        		return redirect()
        					->route('login')
        					->with(['message' => __('message.email.exists'), 'alert' => 'alert-danger']);
        	}
	        $authUser = $this->createUser($user);
        }
        
        Auth::login($authUser, true);
        
        return redirect('/');
        
    }

	private function createUser($data)
	{
		if ($data["verified_email"]) $data["verified_email"] = Carbon::now();

		return User::create([
	        	'email' => $data["email"],
	        	'identity' => $data["identity"],
	        	'password' => Hash::make(str_random(8)),
	        	'name' => $data['first_name'] . ' ' . $data['last_name'],
	        	'email_verified_at' => $data["verified_email"]
	        ]);
	}

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
		Route::post('/auth/ulogin', 'UloginController@login')->name('ulogin');
    }
}
 