<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Models\Mission\Mission;
use App\Helpers\NoteHelper;
use App\Note;
use App\User;

class PhotoReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    } 

    /** 
     * Reports
     *
     * @return void
     */
    public static function reports($mission)
    {
        $mission = Mission::findOrFail($mission);
        $userJoined = (Auth::user()->missions()->where(['mission_id' => $mission->id, 'status' => 'finish'])->first()) ? 1 : 0;

        $photos = Mission::all()->filter(function($mission){
            return $mission->photoReports()->count() > 0;
        });

        return view('site.mission.photo-reports', [
        	'mission' => $mission,
        	'userJoined' => $userJoined,
            'photos' => $photos,
        ]);
    }    

    public function photoReports()
    {
        $photos = Mission::all()->filter(function($mission){
            return $mission->photoReports()->count() > 0;
        }); 
        return view('site.photo_reports', [
            'photos' => $photos,
        ]); 
    }

    public function adavaiReports($noteId)
    {
        $note = Note::whereHas('adavai')->findOrFail($noteId);

        return view('site.adavai.photo-reports', [
            'note' => $note
        ]);
    }

    public function adavaiUserReports($userId)
    {
        $user = User::whereHas('adavaiProfile')->findOrFail($userId);

        return view('site.adavai.user-photo-reports', [
            'user' => $user
        ]);
    }

    public function getAdavaiReports(int $targetId, $type)
    {
        $relations = ['author', 'images', 'files', 'owner'];
        $notes = Note::with($relations)->withCount(['comments', 'likes', 'adavai'])->where(['type' => 'adavai-report'])->where(function($q) use ($type, $targetId) {
            $type == 'user' ? $q->where('author_id', $targetId) : $q->where('owner_id', $targetId);
        })->orderBy('id', 'desc')->get();
        $notes = NoteHelper::userLikesAndAdavais($notes);
        return response()->json([
            'notes' => $notes->toArray()
        ]);
    }

    public function getMissionReports(int $missionId = null)
    {
        $relations = ['author', 'images', 'files', 'owner'];
        $notes = Note::with($relations)->withCount(['comments', 'likes', 'adavai'])->where(['type' => 'photo-report'])->where(function($q) use ($missionId) {
            $missionId ? $q->where('owner_id', $missionId) : null;
        })->orderBy('id', 'desc')->get();
        $notes = NoteHelper::userLikesAndAdavais($notes);
        return response()->json([
            'notes' => $notes->toArray()
        ]);
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/photo-report/{mission}', 'PhotoReportController@reports')->name('photo-report.mission');
        Route::get('/reports/photo', 'PhotoReportController@photoReports')->name('photo.reports');
        Route::get('/reports/adavai/{noteId}', 'PhotoReportController@adavaiReports')->name('adavai.reports');
        Route::get('/reports/adavai/user/{userId}', 'PhotoReportController@adavaiUserReports')->name('adavai.user.reports');
        Route::get('/reports/adavai/reports/{targetId}/{type}', 'PhotoReportController@getAdavaiReports')->name('adavai.reports.get'); 
        Route::get('/reports/mission/reports/{missionId?}', 'PhotoReportController@getMissionReports')->name('mission.reports.get');
    }
}
 