<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Http\Requests\DiaryAddRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Models\Diary;
use App\Models\Repost;
use App\Models\Mission\MissionCategory;
use Illuminate\Support\Carbon;
 
class DiaryController extends Controller
{
    /**
     * Create a new controller instance. 
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }

    /**
     * Show auth user diary list
     * 
     * @return void
     */
    public static function list()
    {
    	$diaries = Diary::with('mission')->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
    	return view('site.diary.list', [
            'diaries' => $diaries,
    	]); 
    }

    /**
     * Add diary
     *
     * @return view
     */ 
    public function getAdd()
    {
        $categories = Auth::user()->missions()->where('status', 'active')->get();
        return view('site.diary.add', [ 
        	'categories' => $categories
        ]);
    }

    /**
     * Post add diary
     * 
     * @return redirect
     */ 
    public function postAdd(DiaryAddRequest $request)
    {
        /*$diary = Diary::where(['mission_id' => $request->mission_id, 'user_id' => Auth::user()->id])->exists();
    	
        if ($diary) {
            return redirect()->back()->with([
                'message' => __('diary.diary.exists'), 'alert' => 'alert-danger'
            ]);
        }*/

        if (Carbon::parse($request->should_finished_at)->isPast()) {
            return redirect()->back()->with([
                'message' => __('diary.date.is.in.past'), 'alert' => 'alert-danger'
            ]);
        }

        Auth::user()->diaries()->create($request->all());
    	return redirect()->route('diary.list'); 
    }
 
    /**
     * Show single diary
     *
     * @return view
     */ 
    public function single($id)
    {
    	$diary = Auth::user()->diaries()->findOrFail($id);
        $finished = Auth::user()->diaries()->with('mission')->finished()->get();
        $active = Auth::user()->diaries()->with('mission')->active()->get();

    	return view('site.diary.single', [
    		'diary' => $diary,
            'finished' => $finished,
            'active' => $active,
    	]);
    }

    /**
     * Get write diary note
     *
     * @return view
     */ 
    public function getWrite($id)
    {
    	$diary = Auth::user()->diaries()->findOrFail($id);
    	
    	return view('site.diary.write', [
    		'diary' => $diary
    	]);
    }

    /**
     * Post write diary note
     *
     * @return view
     */ 
    public function postWrite($id, Request $request)
    {
    	$diary = Auth::user()->diaries()->findOrFail($id);
    	
    	$note = Auth::user()
    			->addedNotes()
    			->create([
					'owner_id' => $diary->id,
					'content' => $request->content,
					'type' => 'diary'
				]);
    	$note = $note->with(['params'])->latest()->first();
    	
    	if ($note) $note->params()->create($request->all());

    	return redirect()->route('diary.single', [$diary->id]);
    }

    /**
     * Get edit diary note
     *
     * @return view
     */ 
    public function getNoteEdit($id)
    {
    	$note = Auth::user()->addedNotes()->findOrFail($id);
    	
    	return view('site.diary.note-edit', [
    		'note' => $note->load('params')
    	]);
    }

    /**
     * Post edit diary note
     *
     * @return view
     */ 
    public function postNoteEdit($id, Request $request)
    {
    	$note = Auth::user()->addedNotes()->findOrFail($id)->load('params');
    	
    	$edit = $note->update(['content' => $request->content]);
    	
    	if ($edit) $note->params()
    					->update([
    						'goal_reached' => $request->goal_reached,
					        'is_easy' => $request->is_easy,
					        'is_motivation_enough' => $request->is_motivation_enough
						]);

    	return redirect()->route('diary.single', [$note->diary->id]);
    }

    /**
     * Share note
     *
     * @return 
     */ 
    public function noteShare($id)
    {
    	$note = Auth::user()->addedNotes()->findOrFail($id);

    	if (Repost::userReposted($note->id)) return redirect()->back();
 
    	$repost = $note->reposts()->create(['user_id' => Auth::user()->id]);

    	return redirect()->back();
    }

    /**
     * Unshare note
     *
     * @return 
     */ 
    public function noteUnshare($id)
    {
    	$note = Auth::user()->addedNotes()->findOrFail($id);

    	if (!Repost::userReposted($note->id)) return redirect()->back();
    	
    	$repost = $note->reposts()->where(['user_id' => Auth::user()->id])->first();

    	$repost->delete();

    	return redirect()->back();
    }

   /**
     * Delete diary
     *
     * @return 
     */ 
    public function getDelete($id)
    {
        $diary = Diary::findOrFail($id);

        if ($diary->user->id === Auth::user()->id) {
            $diary->delete();
        } 


        return redirect()->route('diary.list');
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/diary/list', 'DiaryController@list')->name('diary.list');
        Route::get('/diary/add', 'DiaryController@getAdd')->name('diary.add.get');
        Route::post('/diary/add', 'DiaryController@postAdd')->name('diary.add.post');
        Route::get('/diary/{id}', 'DiaryController@single')->name('diary.single');
        Route::get('/diary/write/{id}', 'DiaryController@getWrite')->name('diary.write.get');
        Route::post('/diary/write/{id}', 'DiaryController@postWrite')->name('diary.write.post');
        Route::get('/diary/note/edit/{id}', 'DiaryController@getNoteEdit')->name('diary.note.edit.get');
        Route::post('/diary/note/edit/{id}', 'DiaryController@postNoteEdit')->name('diary.note.edit.post');
        Route::get('/diary/note/share/{id}', 'DiaryController@noteShare')->name('diary.note.share');
        Route::get('/diary/note/unshare/{id}', 'DiaryController@noteUnshare')->name('diary.note.unshare');
        Route::get('/diary/delete/{id}', 'DiaryController@getDelete')->name('diary.delete.get');
    }
}
 