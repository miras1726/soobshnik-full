<?php

namespace App\Http\Controllers\Mission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Models\Mission\Mission;
use App\Models\Mission\MissionCategory;
use App\Models\Mission\UserMission; 
use Carbon\Carbon;
use App\Models\Accomplice;
use App\Models\Complice;
use App\Models\Diary;
use App\Models\Group;
use App\Note;
use App\Helpers\NoteHelper;
use Musonza\Chat\Models\Conversation;
use Chat;
use Musonza\Chat\Models\ConversationUser;

class MissionController extends Controller
{
    /**  
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }

    /**
     * Show list of missions
     *
     * @return void
     */ 
    public function list()
    {
    	$categories = MissionCategory::all();
        return view('site.mission.missions', [
        	'categories' => $categories
        ]);
    }

    /**
     * Show single mission
     *
     * @return view
     */ 
    public function single($id)
    {
    	$mission = Mission::withCount('groups')->findOrFail($id);

        $userJoined = (Auth::user()->missions()->where(['mission_id' => $id, 'status' => 'active'])->first()) ? 1 : 0;
        $activeMissions = UserMission::where(['mission_id' => $id, 'status' => 'active'])->count();
        $doneMissions = UserMission::where(['mission_id' => $id, 'status' => 'finish'])->count();

        $groups = Group::where('mission_id', $mission->id)->limit(4)->get();

        $groups = $groups->sortByDesc(function($item){
            return $item->subscribersCount();
        })->values()->all();

        return view('site.mission.mission', [
        	'mission' => $mission,
            'userJoined' => $userJoined,
            'doneMissions' => $doneMissions,
            'activeMissions' => $activeMissions,
            'groups' => $groups,
        ]);
    } 

    /**
     * Join mission
     *
     * @return view
     */  
    public function join($id)
    {
        if (Auth::user()->missions()->where('status', 'active')->count() >= 2) {
            return redirect()->back()->with(['message' => __('message.mission.limit.error'),
                                             'alert' => 'alert-danger'
                                         ]);
        }
    	
        $mission = Auth::user()->missions()->where(['id' => $id, 'status' => 'active'])->first();

    	if (!$mission) {
    		Auth::user()->missions()->create([
    			'user_id' => Auth::user()->id,
    			'mission_id' => $id
    		]);
    	} 

        return redirect()->back();
    }

    /**
     * Post report
     *
     * @return view
     */ 
    public function postReport(Request $request)
    {
        $mission = Auth::user()->missions()->where(['id' => $request->id, 'status' => 'active'])->firstOrFail();

        $request->validate([
            'completed' => 'required',
            'completed_time' => 'required',
            'review' => 'required',
            'status' => 'required|in:refuse,finish'
        ]);

        $request->merge([
            'finished_at' => Carbon::now()
        ]);

        $mission->update($request->all());
        
        $this->rateUserComplices($request, $mission->mission_id);

        $this->finishMission($mission->mission_id);

        if ($request->status == 'finish') Auth::user()->setRating(0.4);

        return redirect()->route('mission.list');
    }

    /**
     * Rate user complices
     *
     * @return view
     */ 
    public function rateUserComplices(Request $request, $missionId)
    {
        $complices = Complice::userComplices(Auth::user()->id, $missionId);
 
        if ($complices->count() > 0) {
            foreach ($complices as $complice) {
                if($request->has('complice_' . $complice->id)) {
                    $rating = $request->input('complice_' . $complice->id);
                    $complice->update([
                        $complice->identifyUserRatingField(Auth::user()->id) => $rating
                    ]);
                    if ($rating == 0) $newRating = 0;
                    else $newRating = $rating / 10; 
                    $complice->user->setRating($newRating);
                }
            }
        }
        return true;
    } 

    /**
     * Report 
     *
     * @return view
     */ 
    public function getReport($id, $action) 
    {
        $mission = Auth::user()->missions()->where(['mission_id' => $id, 'status' => 'active'])->firstOrFail();

        if (!$mission) return redirect()->route('mission.list');
        return view('site.mission.report', [
            'mission' => $mission,
            'action' => $action,
            'complices' => Complice::userComplices(Auth::user()->id, $mission->mission->id)
        ]);
    }

    /**
     * User missions 
     *
     * @return view
     */ 
    public function userMission($type)
    {
        $userMissions = Auth::user()->missions()->where(['status' => $type])->get();
        $words = ['active' => __('mission.current'), 'finish' => __('mission.completed'), 'refuse' => __('mission.refused')];

        return view('site.mission.done-missions', [
            'userMissions' => $userMissions,
            'title' => $words[$type]
        ]);
    }

    /**
     * Finish the mission
     *
     * @return view
     */ 
    public function finishMission($missionId)
    {
        $userComplices = Complice::userComplices(Auth::user()->id, $missionId);
        
        if ($userComplices->count() > 0) {
            foreach ($userComplices as $complice) {
                $accomplice = new Accomplice();
                if($accomplice->isAccomplice($complice->user->id)) continue;
                Accomplice::create(['user_id' => $complice->user_id, 'accomplice_id' => $complice->complice_id]);
            }
        }
        $this->removeMissionChat($missionId);

    }

    public function removeMissionChat($missionId)
    {
        $conversation = Conversation::where('data', 'regexp', '"mission":'.$missionId.'[^0-9]')->first();
        $check = ConversationUser::where(['conversation_id' => $missionId, 'user_id' => Auth::user()->id])->first();
        if ($conversation && $check) {
            Chat::conversation($conversation)->removeParticipants([Auth::user()]);
        }
    }

    /**
     * Resume the mission
     *
     * @return view
     */ 
    public function resumeMission($missionId)
    {
        $mission = Auth::user()->missions()->findOrFail($missionId);

        if (Auth::user()->missions()->where('status', 'active')->count() >= 2) {
            return redirect()->back()->with(['message' => __('message.mission.limit.error'),
                                             'alert' => 'alert-danger'
                                         ]);
        }

        $mission->update(['status' => 'active']); 

        return redirect()->route('mission.user', ['active']);
    }

    /**
     * Get mission archive
     *
     * @return view
     */ 
    public function getArchive($id)
    {
        $mission = UserMission::findOrFail($id);

        $diary = Diary::where(['mission_id' => $mission->mission_id, 'user_id' => Auth::user()->id])->first();

        if (!$diary || ($mission->status == 'active')) return redirect()->route('mission.list');

        return view('site.mission.archive', [
            'mission' => $mission,
            'diary' => $diary
        ]); 

    }

    /**
     * Start again the mission
     *
     * @return redirect
     */ 
    public function startAgain($id)
    {
        $uMission = UserMission::findOrFail($id);

        if (Auth::user()->missions()->where('status', 'active')->count() >= 2) {
            return redirect()->back()->with(['message' => __('message.mission.limit.error'),
                                             'alert' => 'alert-danger'
                                         ]);
        }

        if ($uMission->status != 'refuse') return redirect()->route('mission.list');

        $uMission->status = 'active';
        $uMission->review = null;
        $uMission->finished_at = null;
        $uMission->motivation = null;
        $uMission->support = null;
        $uMission->punctuality = null;
        $uMission->communication = null;
        $uMission->completed = null;
        $uMission->completed_time = null;
        $uMission->created_at = Carbon::now();

        if ($uMission->diary()) $uMission->diary()->delete();

        $uMission->update();

        //return redirect()->route('mission.user', ['active']);
        return redirect()->route('mission.single', [$uMission->mission->id]);
    }   
 
    /**
     * Get mission notes.
     *
     * @return response
     */
    public function getMissionNotes(int $id)
    { 
        $notes = Note::where(['owner_id' => $id, 'type' => 'mission'])
            ->withMainRelations()
            ->orderBy('id', 'desc')
            ->paginate(5);

        $notes = NoteHelper::userLikesAndAdavais($notes);

        return response()->json([
            'notes' => $notes->values()->toArray()
        ], 200);
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
		Route::get('/mission/list', 'Mission\MissionController@list')->name('mission.list');
		Route::get('/mission/{id}', 'Mission\MissionController@single')->name('mission.single');
        Route::get('/mission/{id}/join', 'Mission\MissionController@join')->name('mission.join');
        Route::get('/mission/{id}/report/{action}', 'Mission\MissionController@getReport')->name('mission.report.get');
        Route::post('/mission/report', 'Mission\MissionController@postReport')->name('mission.report.post');
        Route::get('/mission/list/{type}', 'Mission\MissionController@userMission')->name('mission.user');
        Route::get('/mission/resume/{id}', 'Mission\MissionController@resumeMission')->name('mission.resume');
        Route::get('/mission/archive/{id}', 'Mission\MissionController@getArchive')->name('mission.archive.get');
        Route::get('/mission/again/{id}', 'Mission\MissionController@startAgain')->name('mission.start.again');
        Route::get('/mission/{id}/get/notes', 'Mission\MissionController@getMissionNotes')->name('mission.get.notes');
    }
}
 