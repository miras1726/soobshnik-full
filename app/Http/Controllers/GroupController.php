<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\Mission\Mission;
use App\Models\Mission\MissionCategory;
use App\Models\Group;
use App\Models\UserGroup;
use App\Note;
use Croppa;
use Illuminate\Support\Collection;
use App\Helpers\NoteHelper;

class GroupController extends Controller 
{
	
    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }
    
	/**
     * Show single group
     *
     * @return view
     */
	public function single($id)
	{
		$group = Group::findOrFail($id);
		$userJoined = (Auth::user()->groups()->where('group_id', $id)->first()) ? 1 : 0;

		return view('site.group.group', [
			'group' => $group,
			'userJoined' => $userJoined
		]);
	}

    /**
     * Join group
     *
     * @return view
     */ 
    public function join($id)
    {
    	$group = Auth::user()->groups()->where('group_id', $id)->first();

    	if (!$group) {
    		Auth::user()->groups()->create([
    			'user_id' => Auth::user()->id,
    			'group_id' => $id
    		]);
    	} 

        return redirect()->back();
    }

    /**
     * Leave group
     *
     * @return view
     */ 
    public function leave($id)
    {
    	$group = Auth::user()->groups()->where('group_id', $id)->first();

    	if ($group) {
    		$group->delete();
    	} 

        return redirect()->back();
    }

    /**
     * List of groups
     *
     * @return view
     */ 
    public function list(Request $request)
    {
        $groups = Auth::user()->groups()->orderBy('created_at', 'desc')->get();

        return view('site.group.list', [
            'groups' => $groups
        ]);
    }

    /**
     * Search
     *
     * @return view
     */ 
    public function search(Request $request)
    {
        $groups = Group::where('title', 'like', '%'.$request->search.'%')->get();

        return view('site.group.search', [
            'groups' => $groups
        ]);
    }

    /**
     * Add group
     *
     * @return view
     */ 
    public function getAdd()
    {
        //$missions = Mission::select('id', 'title')->get();
        $categories = MissionCategory::with('missions')->withCount('missions')->get();
        return view('site.group.add', [
            'categories' => $categories
        ]);
    }

    /**
     * Add group
     *
     * @return view
     */ 
    public function postAdd(Request $request)
    {

        $request->validate([
            'mission_id' => 'required',
            'title' => 'required',
            'description' => 'nullable',
            'image' => 'nullable|file|mimes:jpeg,jpg,png,JPEG'
        ]);

        if($request->image){
            $img = Group::setImg($request->image); 
            
            $data = ['img' => $img,
                     'author_id' => Auth::user()->id];
        } else {
            $data = ['author_id' => Auth::user()->id];
        }

        $request->merge($data);

        if($group = Group::create($request->all())){
            Auth::user()->groups()->create(['group_id' => $group->id]);
            return redirect()->route('group.list');
        }
    }

    /**
     * Edit group
     *
     * @return view
     */ 
    public function getEdit($id)
    {
        $group = Group::findOrFail($id);
        if(!$group->isAdmin()) return redirect()->route('group.list');

        return view('site.group.edit', [
            'group' => $group
        ]);
    }

    /**
     * Add group
     *
     * @return view 
     */ 
    public function postEdit(Request $request)
    {
        $group = Group::findOrFail($request->id);

        if(!$group->isAdmin()) return redirect()->route('group.list');

        $request->validate([
            'title' => 'required',
            'description' => 'nullable',
            'image' => 'nullable|file|mimes:jpeg,jpg,png,JPEG'
        ]);

        if($request->image){
            $img = Group::setImg($request->image);   
            Group::delImg($group->img); 
            $data = ['img' => $img];
            $request->merge($data);
        }

        if($group->update($request->all())){
            return redirect()->route('group.list');
        }
    }



    public function delete($id)
    {
        $group = Group::findOrFail($id);
        if(!$group->isAdmin()) return redirect()->route('group.list');
        
        if($group->delete()) return redirect()->route('group.list');
    }

    /**
     * Get group notes.
     *
     * @return response
     */
    public function getGroupNotes(int $id)
    { 
        $notes = Note::whereHas('group')
            ->where(['owner_id' => $id, 'type' => 'group'])
            ->withMainRelations()
            ->orderBy('id', 'desc')
            ->paginate(2);

        $notes = NoteHelper::userLikesAndAdavais($notes);

        return response()->json([
            'notes' => $notes->values()->toArray()
        ], 200);
    }


    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/group/list', 'GroupController@list')->name('group.list');
        Route::get('/group/search', 'GroupController@search')->name('group.search');
        Route::get('/group/add', 'GroupController@getAdd')->name('group.add.get');
        Route::post('/group/add', 'GroupController@postAdd')->name('group.add.post');
        Route::get('/group/edit/{id}', 'GroupController@getEdit')->name('group.edit.get');
        Route::post('/group/edit', 'GroupController@postEdit')->name('group.edit.post');
        Route::get('/group/delete/{id}', 'GroupController@delete')->name('group.delete');
        Route::get('/group/{id}', 'GroupController@single')->name('group.single');
		Route::get('/group/{id}/join', 'GroupController@join')->name('group.join');
        Route::get('/group/{id}/leave', 'GroupController@leave')->name('group.leave');
		Route::get('/group/{id}/get/notes', 'GroupController@getGroupNotes')->name('group.get.notes');
    }
}
 