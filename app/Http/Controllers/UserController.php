<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route; 
use App\Http\Requests\SettingsRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Croppa;
use Cookie;
use App\Country; 
use App\User;
use App;
use App\Helpers\RobokassaHelper;

class UserController extends Controller
{

    /**
     * Create a new controller instance. 
     *
     * @return void
     */ 
    public function __construct()
    {  
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }


    /**
     * Show the settings page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getSettings()
    {
        $countries = Country::all();
    	return view('site.user.settings', [
            'countries' => $countries
        ]);
    } 

    /**
     * Update user settings
     *
     * @return redirect
     */  
    public function postSettingsUpdate(SettingsRequest $request)
    {
        if (!is_null($request->password)) {
            $request->merge(['password' => Hash::make($request->password)]);
        } else {
            $request->request->remove('password');
        } 

        $update = User::find(Auth::user()->id)->update($request->only([
            "name",
            "gender",
            "born",
            "city",
            "skype",
            "locale",
            "timezone",
            "info",
            "quote",
            "password",
        ]));

        if (in_array($request->locale, ['en', 'ru'])) {
            Cookie::queue(Cookie::forever('locale', $request->locale));  
        }

        if ($request->timezone) Cookie::queue(Cookie::forever('userTimeZoneFromSettings', $request->timezone));

        if ($update) {
            return redirect()->back();  
        }
    }
 
    /**
     * Show user profile
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getProfile($nickname)
    {
        $user = User::where(['nickname' => $nickname])->firstOrFail();
        $missions = $user->missions()->where('status', 'active')->get();

        if ($user->isOwnProfile()) {  
            return view('site.user.my-profile', [
                'missions' => $missions
            ]);
        } else {
            return view('site.user.profile', [
                'user' => $user,
                'missions' => $missions,
                'markedUser' => Auth::user()->markedUsers()->where([
                    'marked_user_id' => $user->id
                ])->first()
            ]);
        
        } 

    }

   /**
     * Define routes
     *
     * @return void
     */
    public function postAvatarUpload(Request $request)
    {
        
        $request->validate([
            'avatar' => 'required|file|mimes:jpeg,jpg,png,JPEG',
        ]);

        $newFileName = md5(time()).'.'
                           .$request->avatar->getClientOriginalExtension();

        Storage::disk('site')->put('img/users/'.$newFileName, File::get($request->avatar));

        \App\User::find(Auth::user()->id)->update(['img' => $newFileName]);
        return json_encode([
                'status' => 1,
                'message' => __('message.error.occured'),
                'link' => url('/'),
                'callbackFunction' => false
            ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Delete avatar
     *
     * @return redirect
     */
    public function getAvatarDelete()
    {
        if (Auth::user()->isAvatarExists()) {
            Croppa::delete('img/users/'.Auth::user()->img);
        }

        \App\User::find(Auth::user()->id)->update(['img' => null]);
        return redirect()->back();
    }

    /**
     * Delete profile
     *
     * @return redirect
     */
    public function getProfileDelete()
    {
        return view('site.user.profile-delete');
    }

    /**
     * Delete profile
     *
     * @return redirect
     */
    public function postProfileDelete()
    {
        Auth::user()->update(['freezed' => 1]);
        return redirect()->back();
    }

    /**
     * Mark the given user
     *
     * @return redirect
     */
    public function markUser(Request $request)
    {
        $user = User::findOrFail($request->id);
        $alreadyExists = Auth::user()->markedUsers()->where([
                                        'marked_user_id' => $request->id,
                                        'mission_id' => $request->mission_id
                                    ])->first();

        if ($alreadyExists || Auth::user()->id == $request->id) return redirect()->back();
        
        Auth::user()->markedUsers()->create([
            'mission_id' => $request->mission_id,
            'marked_user_id' => $request->id
        ]);

        return redirect()->back();
    }

    /**
     * Unmark the given user
     *
     * @return redirect
     */
    public function unmarkUser(Request $request)
    {
        $user = User::findOrFail($request->id);
        $marked = Auth::user()->markedUsers()->where([
                            'mission_id' => $request->mission_id,
                            'marked_user_id' => $user->id
                        ])->first();

        if (!$marked || Auth::user()->id == $request->id) return redirect()->back();
        
        $marked->delete();

        return redirect()->back();
    } 

    public function getReplenishBalance()
    {
        return view('site.user.balance-replenish', [
            'user' => Auth::user(),
        ]);
    }

    public function postReplenishBalance(Request $request)
    {
        $user = Auth::user();
        $mrhLogin = RobokassaHelper::getLogin($request->getHttpHost());
        $mrhPass1 = RobokassaHelper::getPassword($request->getHttpHost());
        $isTest = RobokassaHelper::getTestMode($request->getHttpHost());
        $invId = 678678;
        $sum = $request->sum;
        $invDesc = "Пополнение баланса";
        $crc = md5("$mrhLogin:$sum:$invId:$mrhPass1:Shp_user_id=$user->id");
        $link = "https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=$mrhLogin&OutSum=$sum&InvoiceID=$invId&Description=$invDesc&SignatureValue=$crc&Culture=$user->locale&IsTest=$isTest&Shp_user_id=$user->id";
        
        return redirect($link);
    }

    public function replenishStatus($status)
    {
        $message = $status == 'success' ? __('user.balance.resplenish.success') : __('message.smth.went.wrong');
        return view('site.user.replenish-status', [
            'message' => $message,
            'status' => $status
        ]);
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
		Route::get('/settings', 'UserController@getSettings')->name('settings');
        Route::post('/settings/update', 'UserController@postSettingsUpdate')->name('settingsUpdate');
        Route::get('/{nickname}', 'UserController@getProfile')->name('userProfile');
        Route::post('/avatar/upload', 'UserController@postAvatarUpload')->name('postAvatarUpload');
        Route::get('/avatar/delete', 'UserController@getAvatarDelete')->name('getAvatarDelete');
        Route::get('/profile/delete', 'UserController@getProfileDelete')->name('profile.delete.get');
        Route::post('/profile/delete', 'UserController@postProfileDelete')->name('profile.delete.post');
        Route::post('/user/mark', 'UserController@markUser')->name('user.mark');
        Route::post('/user/unmark', 'UserController@unmarkUser')->name('user.unmark');
        Route::get('/user/balance/replenish', 'UserController@getReplenishBalance')->name('get.replenish.balance');
        Route::post('/user/balance/replenish', 'UserController@postReplenishBalance')->name('post.replenish.balance');
        Route::get('/user/balance/replenish/{status}', 'UserController@replenishStatus')->name('replenish.status');
    }
}