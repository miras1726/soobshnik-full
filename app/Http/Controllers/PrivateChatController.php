<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Musonza\Chat\Models\MessageNotification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Events\NewMessage;
use Chat;
use App;
use Date;    
use App\User;
use App\Helpers\LocaleHelper;

class PrivateChatController extends Controller  
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
        Date::setlocale(App::getLocale());
    }

    /**
     * Chat page
     *
     * @return void
     */
    public function chat($id = false)
    {	
        if ($id == Auth::user()->id) return redirect()->route('private-chat');

    	$user = ($id) ? User::where('id', $id)->where('id', '!=', Auth::user()->id)->first() : false;
        
    	if ($user) { 
    		$conversation = $this->getConversationWith($user);

    		$selected = ($conversation) ? $conversation : $this->createConversationWith($user->id);
    	} else {
    		$selected = 0;
    	}
        
    	return view('site.chat.private-chat', [
    		'selected' => $selected
    	]);
    }

    /**
     * Get messages of conversation
     *
     * @return void
     */
    public function getMessages($id)
    {
    	$conversation = Chat::conversations()->getById($id);

        $messages = Chat::conversation($conversation)->setUser(Auth::user())->setPaginationParams(['perPage' => 10000000])->getMessages();

    	$messages = self::transformMessages($messages);

    	$messages = $this->signUnread($conversation, $messages);

    	return response()->json($messages->items());
    } 

    /**
     * Get user conversation
     *
     * @return void
     */
    public function getConversations($selected = false) 
    {
        $conversations = Chat::conversations()->common([Auth::user()->id]);

        $cc = $conversations->load('users'); 

        $cc = $cc->each(function($item, $key) use ($cc, $selected){
            $item->users->map(function($user) use ($item, $cc, $key){
                if($user->id != Auth::user()->id) $item->companion = $user; 
            });
            if($item->private == 0) $cc->forget($key); 

            $item->unreadCount = Chat::conversation($item)->setUser(Auth::user())->unreadCount();
            if ($item->getMessages(Auth::user(), 1000000000)->count() == 0 && $selected != $item->id) unset($cc[$key]);
            return $item;
        });

        return response()->json($cc->toArray());

    }

    /**
     * Send message
     *
     * @return void
     */
    public function sendMessage(Request $request)
    {
        $conversation = Chat::conversations()->getById($request->conversation);
        
        if($request->type == 'file'){
            $request->validate([
                'body' => 'nullable|file|mimes:jpg,jpeg,png,JPEG,mpga|max:10240',
            ]);

            $body =  json_encode(['name' => $request->body->getClientOriginalName(),
                      'file' => $this->uploadFile($request->body)
                     ], JSON_UNESCAPED_UNICODE);
        }else {
            $body = $request->body;
        }

        $message = Chat::message($body)
            ->from(Auth::user())
            ->to($conversation)
            ->type($request->type)
            ->send();

        broadcast(new NewMessage($message));

        $message->pretty_date = Date::parse($message->created_at)
                                ->setTimezone(LocaleHelper::userTimeZone())
                                ->format('j F Y H:i'); 

        return response()->json($message->toArray());
    }

    /**
     * Upload file
     *
     * @return void
     */
    public function uploadFile($file)
    {
        $hash = substr(md5(microtime()), 0, 10);
        $newFileName = $hash.'.'.$file->getClientOriginalExtension();
        Storage::disk('site')->put('img/chat/'.$newFileName, File::get($file));
        return $newFileName;
    }

    /**
     * Identify unread messages position if present
     *
     * @return void
     */
    public function signUnread($conversation, $messages)
    {
    	$unread = MessageNotification::where(['conversation_id' => $conversation->id,
    									 'is_seen' => 0, 
    									 'user_id' => Auth::user()->id])->first();

    	if($unread){
	    	$messages->map(function($item) use ($unread){
	    		if ($item->id == $unread->message_id) $item->ref = 'unread_start';
	    	});
    		Chat::conversation($conversation)->setUser(Auth::user())->readAll();
		}
		return $messages;
    }

    /**
     * Transform outgoing messages
     *
     * @return collection
     */
    public static function transformMessages($messages)
    {
    	if($messages->count() > 0){
	    	$messages->map(function($item){
                $item->pretty_date = Date::parse($item->created_at)
                                    ->setTimezone(LocaleHelper::userTimeZone())
                                    ->format('j F Y H:i');
	    		if($item->type != 'text') $item->body = json_decode($item->body);
	    	});
		}
		return $messages;
    } 

    /**
     * Create conversation
     *
     * @return 
     */
    public function createConversationWith($userId)
    {
    	$participants = [Auth::user()->id, $userId];
		return Chat::createConversation($participants)->makePrivate();
    }

    /**
     * Get conversation between
     *
     * @return 
     */
    public function getConversationWith($user)
    {
    	return Chat::conversations()->between(Auth::user(), $user);
    }

    /**
     * Clear whole history for user
     *
     * @return void
     */
    public function messageClear($id)
    {
        $conversation = Chat::conversations()->getById($id);
        Chat::conversation($conversation)->setUser(Auth::user())->clear();
        return response()->json(['clear' => 1]);
    }

    /**
     * Define routes 
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/private-chat/with/{id?}', 'PrivateChatController@chat')->name('private-chat');
        Route::get('/private-chat/get/messages/{id}', 'PrivateChatController@getMessages')->name('private-chat.get.messages');
        Route::get('/private-chat/get/conversations/{selected?}', 'PrivateChatController@getConversations')->name('private-chat.get.conversations');
        Route::post('/private-chat/send/message', 'PrivateChatController@sendMessage')->name('private-chat.send.message');
        Route::get('/private-chat/message/markread/{id}', 'PrivateChatController@messageMarkAsRead')->name('private-chat.message.mark.read');
        Route::get('/private-chat/message/clear/{id}', 'PrivateChatController@messageClear')->name('private-chat.message.clear');
    }

}
 