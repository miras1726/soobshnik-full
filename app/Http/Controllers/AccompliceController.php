<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Models\Accomplice;

class AccompliceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }

    /**
     * Accomplice list
     *
     * @return redirect
     */
    public function list()
    {
        $accomplices = Accomplice::userAccomplices(Auth::user()->id);

        return view('site.user.accomplice-list', [
            'accomplices' => $accomplices
        ]);
    }

    /**
     * Accomplice delete
     *
     * @return redirect
     */
    public function delete($id)
    {
    	$accomplice = new Accomplice();
    	if(!$accomplice->isAccomplice($id)) return redirect()->back();
    	$accomplice = $accomplice->get($id);

		$accomplice->delete();
		return redirect()->back();
    }
    
    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/accomplice/list', 'AccompliceController@list')->name('accomplice.list');
        Route::get('/accomplice/delete/{id}', 'AccompliceController@delete')->name('accomplice.delete');
        Route::get('/accomplice/add/{id}/{mission}', 'AccompliceController@add')->name('accomplice.add');
    }
}
