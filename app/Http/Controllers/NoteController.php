<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\NoteAddRequest;
use App\Http\Requests\NoteEditRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Note;
use App\User;
use App\Models\MarkedUser; 
use App\Models\Repost;
use App\Models\Report;
use App\Models\NoteFile;
use App\Models\Settings;
use App\Helpers\StringFilter;
use App\Helpers\NoteHelper;
use App\Events\BadWordFound;

class NoteController extends Controller
{

    /**
     * Create a new controller instance.
     * 
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    } 

    /**
     * Get user notes.
     *
     * @return response
     */
    public function getNotes(int $userNotes, Request $request)
    { 
        $reposts = collect();
        $reposts = Repost::userReposts($request->owner_id);

        $notes = Note::where(['owner_id' => $request->owner_id, 'type' => 'user'])
            ->get(['id', 'created_at']);

        $ids = $notes->pluck('created_at', 'id')->union($reposts->pluck('created_at', 'note_id'))->sort()->reverse();
       

        $notes = Note::whereIn('id', $ids->keys())
            ->where(function($q) use($request) {
                $request->search ? $q->where('content', 'like', '%'.$request->search_input.'%') : null;
            })
            ->withMainRelations()
            ->orderByRaw('FIELD(id, '.implode(',', $ids->keys()->toArray()).')')
            ->paginate(5);

        if ($userNotes) {
            $notes = $notes->filter(function($note){
                return $note->author_id == Auth::user()->id;
            });
        }

        $notes = NoteHelper::userLikesAndAdavais($notes);
        $notes = NoteHelper::markUserReposts($notes, $reposts->pluck('note_id', 'id'));

    	return response()->json([
            'notes' => $notes->values()->toArray()
    	], 200);
    }

    /**
     * delete user note
     *
     * @return 
     */
    public function getDeleteNote($id, $redirect = false)
    {	

    	$note = \App\Note::findOrFail($id);
    	 
    	if (!$note->canManage()) exit();

    	$deleted = $note->delete();

        if($redirect) return redirect()->back();

    	return response()->json([
    		'deleted' => $deleted
    	], 200);
    }

    /**
     * add note
     *
     * @return 
     */ 
    public function postAddNote(NoteAddRequest $request)
    {

        if(StringFilter::filter($request->content)){ 
            return response()->json([
                'denied' => true
            ], 200); 
        }

        if ($request->type == 'group') {
            $note = Note::create($request->all());
        } else {
            $note = Auth::user()->addedNotes()->create($request->all());
        }

        $this->uploadImages($note, $request->img);
        $this->uploadFiles($note, $request->documents);
        
        return response()->json([
            'note' => $note->withMainRelations()->latest()->first()->toArray()
        ], 200);
    }

    private function uploadImages(Note $note, $images)
    {
        if($images){
            foreach($images as $file){

                $newFileName = md5(microtime()).'.'.$file->getClientOriginalExtension();

                if (Storage::disk('site')->put('img/notes/'.$newFileName, File::get($file))) {
                    $note->images()->create(['img' => $newFileName]);
                }
            }
        }
    }

    private function uploadFiles(Note $note, $files)
    {
        if($files){
            foreach($files as $file){
                $newFileName = md5(microtime()).'.'.$file->getClientOriginalExtension();

                if (Storage::disk('site')->put('downloads/notes/'.$newFileName, File::get($file))) {
                    $note->files()->create([
                        'file' => $newFileName,
                        'name' => $file->getClientOriginalName(),
                    ]);
                }
            }
        }
    }
 
    /**
     * edit note
     *
     * @return 
     */
    public function postEditNote(NoteEditRequest $request)
    {
        $note = \App\Note::where('id', $request->id)->with('images')->first();

        if (!$note->canManage()) exit();

        $note->update(['content' => $request->content]);

        $this->uploadImages($note, $request->img);
        $this->uploadFiles($note, $request->documents);

        return response()->json([
    		'note' => $note->fresh()->load('images', 'files')->toArray()
    	], 200);
    }

    /**
     * Delete image
     *
     * @return void
     */
    public function delNoteImage(Request $request){
        $image = \App\NoteImage::findOrFail($request->id);
        $deleted = $image->delete();

        return response()->json([
            'deleted' => $deleted
        ], 200);
    }

    /**
     * Delete file
     *
     * @return void
     */
    public function delNoteFile(Request $request){
        $note = Note::findOrFail($request->note_id);
        if (!$note->canManage()) exit();
        $file = $note->files()->findOrFail($request->id);
        $deleted = $file->delete();

        return response()->json([
            'deleted' => $deleted
        ], 200);
    }

    /**
     * Report
     *
     * @return redirect
     */
    public function report($id, Request $request){
        if (!Report::where(['type' => 'note', 'item_id' => $id])->first()) {
            Report::create([
                'item_id' => $id,
                'user_id' => Auth::user()->id,
                'type' => 'note',
                'report' => 1
            ]); 
        }

        $settings = Settings::first();
        $settings->report_count = $settings->report_count + 1;
        $settings->update();

    }
 
    /**
     * Get user liked notes
     *
     * @return redirect
     */
    public function showUserLiked(){
        return view('site.user.liked-notes');
    }

    /**
     * Get user liked notes
     *
     * @return redirect 
     */
    public function getUserLiked(Request $request){
        $notes = Auth::user()->noteLikes()
                             ->whereHas('note')
                             ->with(['note' => function($q){
                                $q->withMainRelations();
                             }])
                             ->paginate(5)
                             ->pluck('note')
                             ->flatten();
        $notes = NoteHelper::userLikesAndAdavais($notes);
        /*
        if($notes->count() <= 0) exit;

        $notes = $notes->filter(function ($item) {
            return Note::find($item->id);
        }); */

        return response()->json([
            'notes' => $notes->values()->toArray()
        ], 200);
    }

   /**
     * Detach
     *
     * @return redirect 
     */
    public function detach($id){
        $repost = Auth::user()->reposts()->find($id);

        $detached = $repost->delete() ? true : false;

        return response()->json([
            'detached' => $detached
        ], 200);
    }

   /**
     * Share
     *
     * @return redirect 
     */
    public function share($id){
        $repost = Auth::user()->reposts()->create(['note_id' => $id]);

        $shared = $repost ? true : false;

        return response()->json([
            'shared' => $shared
        ], 200);
    }

    public function getNote(int $id) 
    {
        $note = Note::withMainRelations()->findOrFail($id);
        
        $notes = collect();
        $note = NoteHelper::userLikesAndAdavais($notes->push($note));

        return response()->json([
            'note' => $note[0]->toArray()
        ], 200);

    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
		Route::post('/notes/get/{userNotes}', 'NoteController@getNotes')->name('notes.get');
		Route::get('/note/delete/{id}/{redirect?}', 'NoteController@getDeleteNote')->name('note.delete');
		Route::post('/note/add', 'NoteController@postAddNote')->name('note.add');
        Route::post('/note/edit', 'NoteController@postEditNote')->name('note.edit');
        Route::post('/note/img/delete', 'NoteController@delNoteImage')->name('note.img.delete');
        Route::post('/note/file/delete', 'NoteController@delNoteFile')->name('note.file.delete');
        Route::get('/note/report/{id}', 'NoteController@report')->name('note.report');
        Route::get('/note/user/liked/show', 'NoteController@showUserLiked')->name('note.user.liked.show');
        Route::get('/note/user/liked/get', 'NoteController@getUserLiked')->name('note.user.liked.get');
        Route::get('/note/detach/{id}', 'NoteController@detach')->name('note.detach');
        Route::get('/note/share/{id}', 'NoteController@share')->name('note.share');
		Route::get('/note/get/{id}', 'NoteController@getNote')->name('note.get');
    }
}
