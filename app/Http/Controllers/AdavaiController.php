<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Models\Mission\MissionCategory;
use App\Country;
use App\Note;
use App\User;
use App\Models\Report;
use App\Models\AdavaiSubscriber;
use App\Models\Settings;
use App\Models\Award;
use App\Models\UserAdavaiProfile;
use App\Models\AdavaiLicensePurchase;
use App\Events\AdavaiCreated; 
use App\Events\UserJoinedToAdavai;
use App\Events\NotesFetched;
use App\Helpers\NoteHelper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AdavaiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }

    /**
     * Accomplice list
     *
     * @return redirect
     */
    public function list()
    {
        $activeAdavai = AdavaiSubscriber::getUserSubscribedNotes(Auth::user()->id, true, 3);
        $inactiveAdavai = AdavaiSubscriber::getUserSubscribedNotes(Auth::user()->id, false, 3);

        return view('site.adavai.list', [
            'activeAdavai' => $activeAdavai,
            'inactiveAdavai' => $inactiveAdavai,
        ]);
    }

    /**
     * Adavai profile
     *
     * @return redirect
     */
    public function profile($nickname = false)
    {
        $user = $nickname ? User::with('createdGroups')->where('nickname', $nickname)->firstOrFail() : Auth::user()->load('createdGroups');
        $categories = MissionCategory::all();
        $countries = Country::with('cities')->get(); 


        $user->adavaiProfile ?: $user->adavaiProfile()->create(['enable_notifications' => 1]);

        return view('site.adavai.profile', [
            'user' => $user,
            'own' => $user->id == Auth::user()->id,
            'categories' => $categories,
            'countries' => $countries,
        ]);
    }

    /**
     * Join
     *
     * @return redirect
     */
    public function join($id)
    {
        $note = Note::whereHas('adavai')->findOrFail($id);

        $join = Auth::user()->adavaiki()->where(['note_id' => $note->id])->first();
        $joined = false;

        if ($join) {
            $join->delete(); 
            event(new UserJoinedToAdavai($note, $joined, Auth::user()));
        } else {
            if ($note->adavai->check_limit) {
                Auth::user()->adavaiki()->create(['note_id' => $note->id]);
                $joined = true;
                event(new UserJoinedToAdavai($note, $joined, Auth::user()));
            } else {
                $joined = 'limit';
            }
        }

        return response()->json([
            'joined' => $joined
        ], 200);
    }

    public function create()
    {
        $categories = MissionCategory::all();
        $countries = Country::with('cities')->get(); 

        return view('site.adavai.create', [
            'categories' => $categories,
            'countries' => $countries,
        ]);
    }

    public function store(Request $request)
    {
        if (!Auth::user()->adavaiProfile->verified) {
            return redirect()->route('adavai.profile')->with([
                'message' => __('adavai.not_able_to_add'),
                'alert' => 'alert-danger'
            ]);
        }
        $request->validate([
            'city_id' => 'required',
            'category_id' => 'required',
            'limit' => 'required|integer|min:1',
            'date' => 'required',
            'content' => 'required',
        ]);

        $note = Note::create([
            'owner_id' => 0,
            'author_id' => Auth::user()->id,
            'content' => $request->content,
            'type' => 'adavai' 
        ]);

        $note->adavai()->create($request->only([
            'city_id',
            'category_id',
            'limit',
            'check_age',
            'date',
        ]));

        event(new AdavaiCreated($note));

        $note->reposts()->create(['user_id' => Auth::user()->id]);

        return redirect()->route('adavai.profile', [Auth::user()->nickname, 'note' => $note->id]);
    }

    public function getUserAdavaiNotes(Request $request)
    {
        $user = User::findOrFail($request->profile_id);

        $notes = $user->addedNotes()
                            ->whereHas('adavai')
                            ->where(['type' => 'adavai'])
                            ->get(['id']);
        
        $notes = Note::where(function($q) use ($request) {
                            $request->search ? $q->where('content', 'like', '%'.$request->search_input.'%') : null;
                        })
                        ->withMainRelations()
                        ->whereIn('id', $notes->pluck('id'))
                        ->orderBy('id', 'desc')
                        ->paginate(5);                            

        $notes = NoteHelper::userLikesAndAdavais($notes);
        event(new NotesFetched($notes));
        return response()->json([
            'notes' => $notes->values()->toArray()
        ], 200);
    }

    public function getAdavaiNotes(int $userNotes, Request $request)
    {
        $notes = Note::whereHas('adavai')
                        ->where(['type' => 'adavai'])
                        ->get(['id']);

        $notes = Note::where(function($q) use ($request) {
                            $request->search ? $q->where('content', 'like', '%'.$request->search_input.'%') : null;
                        })
                        ->withMainRelations()
                        ->whereIn('id', $notes->pluck('id'))
                        ->orderBy('id', 'desc')
                        ->paginate(5);  
        
        $notes = $this->adavaiSortDefault($notes);

        if ($userNotes) {
            $notes = $notes->filter(function($note){
                return $note->author_id == Auth::user()->id;
            });
        }

        $notes = NoteHelper::userLikesAndAdavais($notes);
        event(new NotesFetched($notes));
        return response()->json([
            'notes' => $notes->sortByDesc('order')->values()->toArray()
        ], 200);
    }

    private function adavaiSortDefault($notes)
    {
        return $notes->each(function($i){
            $i->order += $i->likes->count() ? $i->likes->count() / 100 : 0;
            $i->order += $i->subscriptions->count() ? $i->subscriptions->count() / 100 : 0;
            if (Auth::user()->city == $i->adavai->city_id) $i->order += 2;
            if ($i->author->adavaiProfile->active_license) $i->order += 1;
        });
    }

    public function report(int $id)
    {
        if (!Report::where(['type' => 'adavai', 'item_id' => $id])->first()) {
            Report::create([
                'item_id' => $id,
                'user_id' => Auth::user()->id,
                'type' => 'adavai',
                'report' => 1
            ]); 
        }

        $settings = Settings::first();
        $settings->report_count = $settings->report_count + 1;
        $settings->update();

        return response()->json([
            'success' => true
        ]);
    }

    public function edit(int $id)
    {
        $note = Auth::user()->addedNotes()->whereHas('adavai')->findOrFail($id);
        $countries = Country::with('cities')->get();
        $categories = MissionCategory::all();

        return view('site.adavai.edit', [
            'note' => $note,
            'countries' => $countries,
            'categories' => $categories,
        ]);
    }

    public function update(int $id, Request $request)
    {
        $note = Auth::user()->addedNotes()->whereHas('adavai')->findOrFail($id);
        if (!$note->adavai->editable_adavai) {

            return redirect()->route('adavai.edit', $note->id)->with([
                'message' => __('adavai.not_editable'),
                'alert' => 'alert-danger'
            ]);
        }

        $note->update($request->only(['content']));
        $note->adavai()->update($request->only([
            'category_id',
            'city_id',
            'date',
            'limit',
            'check_age',
        ]));

        return redirect()->back();  
    }

    public function leave(int $id)
    {
        $note = Note::findOrFail($id);
        $subscription = Auth::user()->adavaiki()->where(['note_id' => $note->id])->firstOrFail();
        $subscription ? $subscription->delete() : null;
        event(new UserJoinedToAdavai($note, false, Auth::user()));

        return redirect()->back(); 
    }

    public function profileSettings()
    {
        $user = Auth::user();
        $user->adavaiProfile ?: $user->adavaiProfile()->create();

        return view('site.adavai.settings', [
            'user' => $user,
        ]);
    }

    public function profileSettingsUpdate(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'passport' => 'file|mimes:jpeg,jpg,png,JPEG',
        ]);

        if ($request->passport) {
            $passport = md5(time()).'.'.$request->passport->getClientOriginalExtension();
            Storage::disk('site')->put('img/users/passports/'.$passport, File::get($request->passport));
            $user->adavaiProfile->deletePassportImg();
        } else {
            $passport = $user->adavaiProfile->passport;
        }

        $user->adavaiProfile()->update([
            'full_address' => $request->address,
            'passport' => $passport ?? null,
            'enable_notifications' => $request->enable_notifications ?? 0,
            'auto_prolong' => $request->auto_prolong ?? 0,
        ]);

        return redirect()->back()->with([
            'alert' => 'alert-success',
            'message' => __('message.adavai.profile.update'),
        ]);
    }

    public function subscribers(int $noteId)
    {
        $note = Note::whereHas('adavai')->findOrFail($noteId);
        
        $users = $note->adavaiki()->whereHas('user')->with('user')->distinct()->get()->pluck('user');
    
        return view('site.adavai.subscribers', [
            'users' => $users
        ]);
    }   

    /**
    @todo translate
    */
    public function buyLicense(int $userId)
    {
        $user = User::whereHas('adavaiProfile')->findOrFail($userId);
        if ($user->balance < 100) {
            return redirect()->back()->with([
                'alert' => 'alert-danger',
                'message' => __('message.balance.not.enough'), 
            ]);
        }
        $buy = $user->adavaiProfile->buyLicense();

        $message = $buy ? __('adavai.you.bought.license') : __('adavai.license.buy.error');

        return redirect()->back()->with([
            'alert' => $buy ? 'alert-success' : 'alert-danger',
            'message' => $message,
        ]); 
    }

    public function userBank()
    {
        $user = Auth::user()->load(['awards' => function($q){
            $q->where('award', 'best_manager');
        }]); 
        $profiles = UserAdavaiProfile::whereHas('user')->with('user')->get()->where('active_license', true)->count();
        $sum = AdavaiLicensePurchase::totalPrizeFotThisYear(); 
        $best = Award::with('user')->where('award', 'best_manager')->first();

        return view('site.user.bank', [
            'user' => $user,
            'profiles' => $profiles,
            'sum' => $sum, 
            'best' => $best, 
        ]);
    }

    public function awardLeaveFeedback(Request $request)
    {
        $award = Auth::user()->awards()->where('award', 'best_manager')->firstOrFail();
        
        $award->update([
            'data' => $request->feedback ? json_encode(["feedback" => $request->feedback]) : null
        ]);

        return redirect()->back();
    }
 
    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::post('/adavai/get/user/notes', 'AdavaiController@getUserAdavaiNotes')->name('get.user.adavai.notes');
        Route::post('/adavai/get/notes/{userNotes}', 'AdavaiController@getAdavaiNotes')->name('get.adavai.notes');
        Route::get('/adavai/list', 'AdavaiController@list')->name('adavai.list');
        Route::get('/adavai/profile/{nickname?}', 'AdavaiController@profile')->name('adavai.profile');
        Route::get('/adavai/join/{id}', 'AdavaiController@join')->name('adavai.join');
        Route::post('/adavai/store', 'AdavaiController@store')->name('adavai.store');
        Route::get('/adavai/report/{id}', 'AdavaiController@report')->name('adavai.report');
        Route::get('/adavai/edit/{id}', 'AdavaiController@edit')->name('adavai.edit');
        Route::put('/adavai/update/{id}', 'AdavaiController@update')->name('adavai.update');
        Route::get('/adavai/leave/{id}', 'AdavaiController@leave')->name('adavai.leave');
        Route::get('/adavai/profile-settings', 'AdavaiController@profileSettings')->name('adavai.profile.settings');
        Route::put('/adavai/profile-settings', 'AdavaiController@profileSettingsUpdate')->name('adavai.profile.settings.update');
        Route::get('/adavai/subscribers/{noteId}', 'AdavaiController@subscribers')->name('adavai.subscribers');
        Route::get('/adavai/create', 'AdavaiController@create')->name('adavai.create');
        Route::post('/adavai/license/buy/{userId}', 'AdavaiController@buyLicense')->name('adavai.buy.license');
        Route::get('/adavai/user/bank', 'AdavaiController@userBank')->name('adavai.user.bank');
        Route::post('/adavai/award/leave/feedback', 'AdavaiController@awardLeaveFeedback')->name('adavai.award.leave.feedback');
    }
} 
