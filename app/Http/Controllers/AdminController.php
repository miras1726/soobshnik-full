<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Country;
use App\City; 
use App\User;
use App\Note;
use App\Models\Settings;
use App\Models\Notification;
use App\Models\Group; 
use App\Models\Report;
use App\Models\Order;
use App\Models\Mission\Mission; 
use App\Models\Mission\MissionLink; 
use App\Models\Mission\MissionCategory; 
use App\Models\Product;  
use App\Models\Question;  
use App\Models\UserAdavaiProfile;
use App\Models\Award;
use App\Models\AdavaiLicensePurchase;
use App\Helpers\FileManager;
use Croppa;
 
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */  
    public function __construct()
    {
        $this->middleware(['auth', 'admin']); 
    }

    /**
     * Index page.
     *
     * @return void
     */ 
    public function index()
    {
        $users = User::count();
        $notes = Note::count();
        $groups = Group::count();
        $products = Product::count();
        $missions = Mission::count();

        return view('admin.index', [
            'users' => $users,
            'notes' => $notes,
            'groups' => $groups,
            'products' => $products,
            'missions' => $missions
        ]);
    }

    /**
     * Country page.
     *
     * @return void
     */ 
    public function country()
    {
    	$countries = Country::orderBy('id', 'desc')->get();

        return view('admin.country.index', [
        	'countries' => $countries
        ]);
    }

    /**
     * Add Country page.
     *
     * @return void
     */ 
    public function getAddCountry()
    {
        return view('admin.country.add');
    }

    /**
     * Post Country.
     *
     * @return 
     */ 
    public function postAddCountry(Request $request)
    {
        $request->validate([
        	'title' => 'required'
        ]);
        Country::create($request->all());
        return redirect()->route('admin.country');
    }

    /**
     * Edit Country page.
     *
     * @return void
     */ 
    public function getEditCountry($id)
    {
        $country = Country::findOrFail($id);

        return view('admin.country.edit', [
        	'country' => $country
        ]);
    }

    /**
     * Post Country.
     *
     * @return 
     */ 
    public function postEditCountry($id, Request $request)
    {
        $request->validate([
        	'title' => 'required'
        ]);
        $country = Country::findOrFail($id);
        $country->update($request->all());
        return redirect()->route('admin.country');
    }

    /**
     * Delete.
     *
     * @return void
     */ 
    public function getDeleteCountry($id)
    {
        $country = Country::findOrFail($id);
        $country->delete();
        return redirect()->route('admin.country');
    }

    /**
     * Country page.
     *
     * @return void
     */ 
    public function city()
    {
    	$countries = City::orderBy('id', 'desc')->get();

        return view('admin.city.index', [
        	'countries' => $countries
        ]);
    }

    /**
     * Add City page.
     *
     * @return void
     */ 
    public function getAddCity()
    {
    	$countries = Country::all();

        return view('admin.city.add', [
        	'countries' => $countries
        ]);
    }

    /**
     * Post Country.
     *
     * @return 
     */ 
    public function postAddCity(Request $request)
    {
        $request->validate([
        	'title' => 'required',
        	'country_id' => 'required|integer'
        ]);
        City::create($request->all());
        return redirect()->route('admin.city');
    }

    /**
     * Edit Country page.
     *
     * @return void
     */ 
    public function getEditCity($id)
    {
        $city = City::findOrFail($id);
        $countries = Country::all();

        return view('admin.city.edit', [
        	'city' => $city,
        	'countries' => $countries
        ]);
    }

    /**
     * Post Country.
     *
     * @return 
     */ 
    public function postEditCity($id, Request $request)
    {
        $request->validate([
        	'title' => 'required'
        ]);
        $city = City::findOrFail($id);
        $city->update($request->all());
        return redirect()->route('admin.city');
    }

    /**
     * Delete.
     *
     * @return void
     */ 
    public function getDeleteCity($id)
    {
        $city = City::findOrFail($id);
        $city->delete();
        return redirect()->route('admin.city');
    }

    /**
     * Users page.
     *
     * @return view
     */ 
    public function user()
    {
    	$users = User::orderBy('id', 'desc')->paginate(50);

        return view('admin.user.index', [
        	'users' => $users
        ]);
    }

    /**
     * Edit user page.
     *
     * @return view
     */ 
    public function getEditUser($id)
    {
    	$user = User::findOrFail($id);
        $countries = Country::all();

        return view('admin.user.edit', [
        	'user' => $user,
        	'countries' => $countries
        ]);
    }

    /**
     * Edit user page.
     *
     * @return view
     */ 
    public function postEditUser(Request $request, $id)
    {
    	$user = User::findOrFail($id);

        $user->update($request->all());
        return redirect()->route('admin.user');
    }

    /**
     * Delete.
     *
     * @return 
     */ 
    public function getDeleteUser($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('admin.user');
    }

    /**
     * Mission
     *
     * @return 
     */ 
    public function mission()
    {
        $missions = Mission::orderBy('id', 'desc')->get();

        return view('admin.mission.index', [
        	'missions' => $missions
        ]);
    }

    /**
     * Add Mission page.
     *
     * @return 
     */ 
    public function getAddMission()
    {
    	$categories = MissionCategory::all();

        return view('admin.mission.add', [
        	'categories' => $categories
        ]);
    }

    /**
     * Post Mission.
     *
     * @return 
     */ 
    public function postAddMission(Request $request)
    {
        $request->validate([
        	'title' => 'required',
        	'category_id' => 'required|integer'
        ]);

        if($request->image){
            $img = Mission::setImg($request->image); 
            
            $request->merge(['img' => $img]);
        }

        Mission::create($request->all());
        return redirect()->route('admin.mission'); 
    }

    /**
     * Edit mission page.
     *
     * @return view
     */ 
    public function getEditMission($id)
    {
    	$mission = Mission::findOrFail($id);
        $categories = MissionCategory::all();

        return view('admin.mission.edit', [
        	'mission' => $mission,
        	'categories' => $categories
        ]);
    }

    /**
     * Edit mission post.
     *
     * @return view
     */ 
    public function postEditMission(Request $request, $id)
    {
    	$mission = Mission::findOrFail($id);

        if($request->image){
            $img = Mission::setImg($request->image);   
            Mission::delImg($mission->img); 
            $request->merge(['img' => $img]);
        }

        $mission->update($request->all());
        return redirect()->route('admin.mission');
    }

    /**
     * Delete.
     *
     * @return 
     */ 
    public function getDeleteMission($id)
    {
        $mission = Mission::findOrFail($id);
        $mission->delete();
        return redirect()->route('admin.mission');
    }

    /**
     * Product
     *
     * @return 
     */ 
    public function product()
    {
        $products = Product::orderBy('id', 'desc')->get();

        return view('admin.product.index', [
            'products' => $products
        ]);
    }

    /**
     * Add Product page.
     *
     * @return 
     */ 
    public function getAddProduct()
    {
        return view('admin.product.add');
    }

    /**
     * Post Product.
     *
     * @return 
     */ 
    public function postAddProduct(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'price' => 'required|integer',
            'price_star' => 'required|integer',
            'image' => 'nullable|file|mimes:jpg,jpeg,png,JPEG'
        ]);

        if ($request->image) $request->merge(['img' => FileManager::upload($request->image, 'img/products/')]);

        Product::create($request->all());
        return redirect()->route('admin.product');
    } 

    /**
     * Edit product page.
     *
     * @return view
     */ 
    public function getEditProduct($id)
    {
        $product = Product::findOrFail($id);

        return view('admin.product.edit', [
            'product' => $product
        ]);
    }

    /**
     * Edit product post.
     *
     * @return view
     */ 
    public function postEditProduct(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        if ($request->image) {
            if ($product->isAvatarExists()) {
                Croppa::delete('img/products/'.$product->img);
            }

            $request->merge(['img' => FileManager::upload($request->image, 'img/products/')]);

        }

        $product->update($request->all());

        return redirect()->route('admin.product');
    }

    /**
     * Delete.
     *
     * @return 
     */ 
    public function getDeleteProduct($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('admin.product');
    }

    /**
     * Delete note.
     *
     * @return 
     */ 
    public function getDeleteNote($id)
    {
        $note = Note::findOrFail($id);

        $deleted = $note->delete();

        return response()->json([
            'deleted' => $deleted
        ], 200);
    }

    /**
     * Group
     *
     * @return 
     */ 
    public function group()
    {
        $groups = Group::orderBy('id', 'desc')->get();

        return view('admin.group.index', [
            'groups' => $groups
        ]);
    }

    /**
     * Delete group.
     *
     * @return 
     */ 
    public function getDeleteGroup($id)
    {
        $group = Group::findOrFail($id);

        $deleted = $group->delete();

        return redirect()->back();
    }

    /**
     * Report.
     *
     * @return 
     */ 
    public function report()
    {
        $settings = Settings::first();
        $settings->report_count = 0;
        $settings->update(); 

        return view('admin.report.index');
    }

    /**
     * Badword.
     *
     * @return 
     */  
    public function badword()
    {
        return view('admin.report.badword');
    }


    /**
     * Get report notes.
     *
     * @return 
     */
    public function getReportNotes(Request $request)
    { 
        $notes = Report::notes(); 

        return response()->json([
            'notes' => $notes->toArray()
        ], 200);
    }

    /**
     * Get report notes.
     *
     * @return 
     */
    public function getBadWordNotes(Request $request)
    { 
        $notes = Report::badWordNotes(); 
 
        return response()->json([
            'notes' => $notes->toArray()
        ], 200);
    }

    /**
     * Post bad word list.
     *
     * @return 
     */
    public function getBadwordList(Request $request)
    { 
        $list = Settings::first();

        $badWords = ($list) ? $list->bad_words : null;

        return view('admin.report.badword-list', [
            'list' => $badWords 
        ]);
    }
    /**
     * Post bad word list.
     *
     * @return 
     */
    public function postBadwordList(Request $request)
    { 
        $settings = Settings::first();

        if (!$settings) {
            $settings = Settings::create(['bad_words' => null]);
        } 

        $settings->update(['bad_words' => $request->bad_words]);
        return redirect()->route('admin.badword');
    }

   /**
     * Add Mission page.
     *
     * @return 
     */ 
    public function getAddMissionLink($missionId)
    {
        return view('admin.mission.link-add', [
            'missionId' => $missionId
        ]);
    }

    /**
     * Post Mission.
     *
     * @return 
     */ 
    public function postAddMissionLink(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'link' => 'required',
            'mission_id' => 'required|integer'
        ]);
        MissionLink::create($request->all());
        return redirect()->route('admin.mission.edit.get', [$request->mission_id]);
    }

    /**
     * Delete.
     *
     * @return 
     */ 
    public function getDeleteMissionLink($id)
    {
        $link = MissionLink::findOrFail($id);
        $link->delete();
        return redirect()->route('admin.mission.edit.get', [$link->mission_id]);
    }

    /**
     * get orders.
     *
     * @return view
     */ 
    public function getOrders()
    {
        $orders = Order::orderBy('id', 'desc')->get();
        $settings = Settings::first();

        $settings->order_count = 0;
        $settings->update();

        return view('admin.product.orders', [
            'orders' => $orders
        ]);
    }

    /**
     * Order delete
     *
     * @return redirect
     */ 
    public function getOrderDelete($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();
        return redirect()->route('admin.orders');
    }

    /**
     * Question list
     *
     * @return view
     */ 
    public function getQuestions()
    {
        $questions = Question::orderBy('answer')->orderBy('created_at', 'DESC')->paginate(10);

        $settings = Settings::first();

        $settings->question_count = 0;
        $settings->update();

        return view('admin.question.list', [
            'questions' => $questions
        ]);
    }

    /**
     * Get question answer
     *
     * @return redirect
     */ 
    public function getAnswerToQuestion($id)
    {
        $question = Question::findOrFail($id);

        return view('admin.question.answer', [
            'question' => $question 
        ]);
    }

    /**
     * Post question answer
     *
     * @return redirect
     */ 
    public function postAnswerToQuestion(Request $request)
    {
        $question = Question::findOrFail($request->id);

        $question->answer = $request->text;
        $question->update(); 

        if($question->user->locale) App::setLocale($question->user->locale);
        Notification::create([
            'user_id' => $question->user->id,
            'title' => __('ask.got.question.title'),  
            'text' => __('ask.got.question.body'),
            'link' => route('app.ask.get') 
        ]);

        return redirect()->route('admin.get.questions');  
    }

    /**
     * Get question answer
     *
     * @return redirect
     */ 
    public function getQuestionDelete($id)
    {
        $question = Question::findOrFail($id);
        $question->delete();

        return redirect()->route('admin.get.questions');
    }

    public function getAdavaiProfiles($type = false)
    {
        $profiles = UserAdavaiProfile::whereHas('user')->with('user')->where(function($q) use ($type){
            if ($type == 'for-confirm') $q->verified(0);
            elseif($type == 'verified') $q->verified();
            elseif($type == 'licensed') $q->licensed();
        })->paginate(50);

        return view('admin.adavai.profiles', [
            'profiles' => $profiles,
            'totalPrize' => AdavaiLicensePurchase::totalPrizeFotThisYear()
        ]);
    }

    public function postConfirmAdavaiProfile(int $userId)
    {
        $profile = UserAdavaiProfile::where('user_id', $userId)->firstOrFail();
        $profile->confirmProfile();
        
        $user = User::find($userId);

        if($user->locale) App::setLocale($user->locale);
        Notification::create([
            'user_id' => $user->id,
            'title' => __('message.adavai.profile.confirmed.title'),
            'text' => __('message.adavai.profile.confirmed.text'),
            'link' => route('adavai.profile', $user->nickname),
        ]);

        return redirect()->back();
    }

    public function getSetAdavaiAward(int $userId, $award)
    {
        $user = User::findOrFail($userId); 
        Award::setWinner($user, $award);

        if($user->locale) App::setLocale($user->locale);
        Notification::create([ 
            'user_id' => $user->id,
            'title' => __('message.adavai.award.won.title'),
            'text' => __('message.adavai.award.won.text'),
            'link' => route('userProfile', $user->nickname),
        ]);

        return redirect()->back();
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/admin/index', 'AdminController@index')->name('admin.index'); 
        Route::get('/admin/country', 'AdminController@country')->name('admin.country');
        Route::get('/admin/city', 'AdminController@city')->name('admin.city');
        Route::get('/admin/country/add', 'AdminController@getAddCountry')->name('admin.country.add.get');
        Route::post('/admin/country/add', 'AdminController@postAddCountry')->name('admin.country.add.post');
        Route::get('/admin/country/delete/{id}', 'AdminController@getDeleteCountry')->name('admin.country.delete.get');
        Route::get('/admin/country/edit/{id}', 'AdminController@getEditCountry')->name('admin.country.edit.get');
        Route::post('/admin/country/edit/{id}', 'AdminController@postEditCountry')->name('admin.country.edit.post');

        Route::get('/admin/city/add', 'AdminController@getAddCity')->name('admin.city.add.get');
        Route::post('/admin/city/add', 'AdminController@postAddCity')->name('admin.city.add.post');
        Route::get('/admin/city/delete/{id}', 'AdminController@getDeleteCity')->name('admin.city.delete.get');
        Route::get('/admin/city/edit/{id}', 'AdminController@getEditCity')->name('admin.city.edit.get');
        Route::post('/admin/city/edit/{id}', 'AdminController@postEditCity')->name('admin.city.edit.post');

        Route::get('/admin/user', 'AdminController@user')->name('admin.user');
        Route::get('/admin/user/delete/{id}', 'AdminController@getDeleteUser')->name('admin.user.delete.get');
        Route::get('/admin/user/edit/{id}', 'AdminController@getEditUser')->name('admin.user.edit.get');
        Route::post('/admin/user/edit/{id}', 'AdminController@postEditUser')->name('admin.user.edit.post');

        Route::get('/admin/mission', 'AdminController@mission')->name('admin.mission');
        Route::get('/admin/mission/add', 'AdminController@getAddMission')->name('admin.mission.add.get');
        Route::post('/admin/mission/add', 'AdminController@postAddMission')->name('admin.mission.add.post');
        Route::get('/admin/mission/delete/{id}', 'AdminController@getDeleteMission')->name('admin.mission.delete.get');
        Route::get('/admin/mission/edit/{id}', 'AdminController@getEditMission')->name('admin.mission.edit.get');
        Route::post('/admin/mission/edit/{id}', 'AdminController@postEditMission')->name('admin.mission.edit.post');

        Route::get('/admin/product', 'AdminController@product')->name('admin.product');
        Route::get('/admin/product/add', 'AdminController@getAddProduct')->name('admin.product.add.get');
        Route::post('/admin/product/add', 'AdminController@postAddProduct')->name('admin.product.add.post');
        Route::get('/admin/product/delete/{id}', 'AdminController@getDeleteProduct')->name('admin.product.delete.get');
        Route::get('/admin/product/edit/{id}', 'AdminController@getEditProduct')->name('admin.product.edit.get');
        Route::post('/admin/product/edit/{id}', 'AdminController@postEditProduct')->name('admin.product.edit.post');
        Route::get('/admin/orders', 'AdminController@getOrders')->name('admin.orders');
        Route::get('/admin/order/delete/{id}', 'AdminController@getOrderDelete')->name('admin.order.delete');

        Route::get('/admin/note/delete/{id}', 'AdminController@getDeleteNote')->name('admin.delete.note');

        Route::get('/admin/group', 'AdminController@group')->name('admin.group');
        Route::get('/admin/group/delete/{id}', 'AdminController@getDeleteGroup')->name('admin.group.delete.get');
        
        Route::get('/admin/report', 'AdminController@report')->name('admin.report'); 
        Route::get('/admin/reports/get', 'AdminController@getReportNotes')->name('admin.get.reports');
        
        Route::get('/admin/badword/reports/get', 'AdminController@getBadWordNotes')->name('admin.get.badword.reports');
        Route::get('/admin/badword', 'AdminController@badword')->name('admin.badword');
        Route::get('/admin/badword/list', 'AdminController@getBadwordList')->name('admin.badword.list.get');
        Route::post('/admin/badword/list', 'AdminController@postBadwordList')->name('admin.badword.list.post');

        Route::get('/admin/mission/link/add/{missionId}', 'AdminController@getAddMissionLink')->name('admin.mission.link.add.get');
        Route::post('/admin/mission/link/add', 'AdminController@postAddMissionLink')->name('admin.mission.link.add.post');
        Route::get('/admin/mission/link/delete/{id}', 'AdminController@getDeleteMissionLink')->name('admin.mission.link.delete.get');

        Route::get('/admin/question', 'AdminController@getQuestions')->name('admin.get.questions');
        Route::get('/admin/question/answer/{id}', 'AdminController@getAnswerToQuestion')->name('admin.answer.to.question.get');
        Route::post('/admin/question/answer', 'AdminController@postAnswerToQuestion')->name('admin.answer.to.question.post');
        Route::get('/admin/question/delete/{id}', 'AdminController@getQuestionDelete')->name('admin.question.delete');

        Route::get('/admin/adavai/profiles/{type?}', 'AdminController@getAdavaiProfiles')->name('admin.adavai.profiles.get');
        Route::post('/admin/adavai/profiles/confirm/{userId}', 'AdminController@postConfirmAdavaiProfile')->name('admin.confirm.adavai.profile.post');
        Route::get('/admin/adavai/award/{userId}/{award}', 'AdminController@getSetAdavaiAward')->name('admin.adavai.award.get');

    }

}
 