<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\NoteLike;
use App\Helpers\StringFilter;
use App\Events\BadWordFound;
use App\Events\NoteWasLiked;
use App\Note;

class NoteCommentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }

    /**
     * Get all comments of note
     *
     * @return 
     */
    public function getNoteComments($noteId)
    {
    	$comments = \App\NoteComment::where('note_id', $noteId)->with('user')->get();

    	return response()->json([
    		'comments' => $comments->toArray()
    	], 200);
    }

    /**
     * add note
     * @see this 
     * @return 
     */
    public function postAddNoteComment(Request $request)
    {
    	$comment = Auth::user()->noteComments()->create($request->all());

        if(StringFilter::filter($request->text)){
            event(new BadWordFound(Auth::user(), 'www.google.com'));
        }

    	return response()->json([
    		'comment' => $comment->with('user')->latest()->first()->toArray()
    	], 200);
    }

    /**
     * delete note comment
     *
     * @return 
     */
    public function getDeleteNoteComment($id)
    {	

    	$comment = \App\NoteComment::findOrFail($id);
    	
        if (!Auth::user()->isAdmin()){
            if (!$comment->canManage($comment->user_id)) exit();
        }

    	$deleted = $comment->delete();

    	return response()->json([
    		'deleted' => $deleted
    	], 200);
    }

    /**
     * like note
     *
     * @return response
     */
    public function noteLike($id)
    {   
        $note = Note::findOrFail($id);
        $like = $note->likes()->where(['note_id' => $id, 'user_id' => Auth::user()->id])->first();

        if ($like) {
            $like->delete();
            $liked = false;
        } else {
            $like = $note->likes()->create(['user_id' => Auth::user()->id]);
            $liked = true;
            event(new NoteWasLiked($note));
        }

        return response()->json([
            'liked' => $liked
        ], 200); 
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
		Route::get('/note/comments/{noteId}', 'NoteCommentController@getNoteComments')->name('note.comments');
		Route::post('/note/comment/add', 'NoteCommentController@postAddNoteComment')->name('note.comment.add');
        Route::get('/note/comment/delete/{id}', 'NoteCommentController@getDeleteNoteComment')->name('note.comment.delete');
		Route::get('/note/{id}/like', 'NoteCommentController@noteLike')->name('note.like');
    }
}
