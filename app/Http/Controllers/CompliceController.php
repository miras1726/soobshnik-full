<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Mission\UserMission;
use App\Models\Mission\Mission;
use App\Models\Complice;
use Illuminate\Support\Carbon;
use App\Events\CompliceAdded;
use App\Events\CompliceRefused;

class CompliceController extends Controller
{

    /**
     * Create a new controller instance.
     * 
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }

    /**
     * Complice list
     *
     * @return redirect
     */
    public function compliceList()
    {
        $complices = Complice::userComplices(Auth::user()->id, false, false)->sortBy('accepted')->sortBy('mission_id');
        
        $userMissions = Auth::user()->missions()->with('mission')->distinct('mission_id')->get(['mission_id']); 

        return view('site.user.complice-list', [
            'complices' => $complices,
            'userMissions' => $userMissions,
        ]);
    }

    /**
     * Complice search
     *
     * @return redirect
     */ 
    public function compliceSearch(Request $request)
    {
        $complices = UserMission::where(['mission_id' => $request->mission, 'status' => 'active']);
        
        $complices = $complices->whereHas('user', function($q) use($request){

            if ($request->nickname) { 
                $q->where('nickname', $request->nickname);
            }

            if ($request->city) { 
                $q->where('city', $request->city);
            }

            if ($request->gender) {
                $q->where('gender', $request->gender);
            }

            if ($request->from) {
                $q->where('born', '<=', Carbon::now()->subYears($request->from));
            }

            if ($request->to) {
                $q->where('born', '>=', Carbon::now()->subYears($request->to));
            }
            $q->where('id', '!=', Auth::user()->id);
        })->get();

        return view('site.user.complice-search', [
            'complices' => $complices,
            'missionId' => $request->mission
        ]); 
    }

    /**
     * Complice delete
     *
     * @return redirect
     */
    public function delete($id, $missionId)
    {
    	$complice = new Complice();
    	if(!$complice->isComplice($id, $missionId, false)) return redirect()->back();
    	$complice = $complice->get($id, $missionId, false);

		$complice->delete();
		return redirect()->back();
    }

    /**
     * Complice accept
     *
     * @return redirect
     */
    public function accept($id, $missionId)
    {
        if(Complice::userComplices(Auth::user()->id, $missionId)->count() >= 3) {
            return redirect()->back()->with(['message' => __('message.complice.count.limit'),
                                 'alert' => 'alert-danger'
                             ]);
        }
        $complice = new Complice();
        if(!$complice->isComplice($id, $missionId, false)) return redirect()->back();
        $complice = $complice->get($id, $missionId, false);

        $complice->update(['accepted' => 1]);
        return redirect()->back();
    }

    /**
     * Complice add
     *
     * @return redirect
     */ 
    public function add($id, $mission)
    {
    	if(Complice::userComplices(Auth::user()->id, $mission)->count() >= 3) {
            return redirect()->back()->with(['message' => __('message.complice.count.limit'),
                                 'alert' => 'alert-danger'
                             ]);
        }
    	$user = User::findOrFail($id);
    	$mission = Mission::findOrFail($mission);

        if(!Auth::user()->missions()->where('mission_id', $mission->id)->first()){
            return redirect()->back()->with(['message' => __('message.user.not.in.mission'),
                                 'alert' => 'alert-danger'
                             ]);
        }

    	$complice = new Complice(); 
    	
    	if(!$complice->isComplice($id, $mission->id)){
    		$complice = Complice::create([
    			'mission_id' => $mission->id,
    			'user_id' => Auth::user()->id,
    			'complice_id' => $user->id
    		]);
            event(new CompliceAdded($complice));
    	}
		return redirect()->back();
    }

    /**
     * Complice remove with rating
     *
     * @return redirect
     */
    public function getRefuseComplice($id, $missionId)
    {
        $complice = new Complice();
        if(!$complice->isComplice($id, $missionId, false)) return redirect()->back();
        $complice = $complice->get($id, $missionId, false);

        return view('site.user.complice-refuse', [
            'complice' => $complice,
            'missionId' => $missionId
        ]);

    }

    /**
     * Refusing from complice with rating
     *
     * @return redirect
     */
    public function postRefuseComplice(Request $request)
    {
        $complice = new Complice();
        if(!$complice->isComplice($request->id, $request->mission_id, false)) return redirect()->back();
        $complice = $complice->get($request->id, $request->mission_id, false);
        $mission = Mission::find($request->mission_id);

        if($request->has('complice_' . $complice->id)) {
            $rating = $request->input('complice_' . $complice->id);
            if ($rating == 0) $newRating = 0;
            else $newRating = $rating / 10; 
            $complice->user->setRating($newRating);
        }
        event(new CompliceRefused($complice, $rating, $mission, $request->reason));
        $complice->delete();

        return redirect()->route('complice.list');
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/complice/list', 'CompliceController@compliceList')->name('complice.list');
        Route::get('/complice/search', 'CompliceController@compliceSearch')->name('complice.search');
        Route::get('/complice/delete/{id}/{missionId}', 'CompliceController@delete')->name('complice.delete');
        Route::get('/complice/add/{id}/{mission}', 'CompliceController@add')->name('complice.add');
        Route::get('/complice/accept/{id}/{missionId}', 'CompliceController@accept')->name('complice.accept');
        Route::get('/complice/refuse/{id}/{missionId}', 'CompliceController@getRefuseComplice')->name('complice.refuse.get');
        Route::post('/complice/refuse', 'CompliceController@postRefuseComplice')->name('complice.refuse.post');
    }
}
