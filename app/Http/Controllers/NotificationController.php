<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Note;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }

    /**
     * List of user notifications
     * 
     * @return view
     */
    public function list($type = false, $targetId = false)
    {
    	$notifications = Auth::user()->notifications()->where(function($query) use ($type, $targetId) {
            $type ? $query->where(['type' => $type, 'target_id' => $targetId]) : null;
        })->orderBy('id', 'desc')->paginate(50);

    	return view('site.notification.list', [ 
    		'notifications' => $notifications
    	]);
    }

    /**
     * Delete user notification
     * 
     * @return redirect
     */
    public function delete($id)
    {
    	$notification = Auth::user()->notifications()->findOrFail($id);
    	$notification->delete();
    	return redirect()->back();
    }

    /**
     * Clean user notification
     * 
     * @return redirect
     */
    public function clean($type = false, $targetId = false)
    {
    	$notifications = Auth::user()->notifications()->where(function($query) use ($type, $targetId) {
            $type ? $query->where(['type' => $type, 'target_id' => $targetId]) : null;
        })->get();

    	if ($notifications->count() <= 0) return redirect()->route('notification.list');
    	
        $notifications->each(function($item){
    		$item->delete();
    	});
    	return redirect()->back();
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
		Route::get('/notification/list/{type?}/{targetId?}', 'NotificationController@list')->name('notification.list');
		Route::get('/notification/delete/{id}', 'NotificationController@delete')->name('notification.delete');
		Route::get('/notification/clean/{type?}/{targetId?}', 'NotificationController@clean')->name('notification.clean');
    }
}
