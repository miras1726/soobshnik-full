<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Order;
use App\Models\Settings;
use Session;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }

    /**
     * List of products.
     *
     * @return void
     */ 
    public function list()
    {
    	$products = Product::paginate(8);
    	 
    	return view('site.product.list', [
    		'products' => $products
    	]);
    }

    public function buy($id, Request $request)
    {
        $product = Product::findOrFail($id);
        $type = $request->money ? 'money' : 'balance';
        $productPrice = $request->money ? 'price' : 'price_star';

        if (Auth::user()->{$type} <= $product->{$productPrice}) {
            return redirect()->back()->with(['message' => __('message.'.$type.'.not.enough'), 'alert' => 'alert-danger']);
        }

        if (Order::create(['user_id' => Auth::user()->id, 'product_id' => $product->id])) {
            $newBalance = (Auth::user()->{$type} - $product->{$productPrice});

            Auth::user()->update([
                $type => $newBalance
            ]);

            $settings = Settings::first();
            $settings->order_count = $settings->order_count + 1;
            $settings->update();
        }

        return redirect()->back()->with(['message' => __('message.just.bought', ['product' => $product->title]), 'alert' => 'alert-success']);
    }

    /**
     * List of products.
     *
     * @return void
     */ 
    public function myProducts()
    {
        $orders = Order::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
         
        return view('site.product.my-products', [
            'orders' => $orders
        ]);
    }

    /**
     * Define routes
     *
     * @return void
     */
    public static function routes()
    {
        Route::get('/product/list', 'ProductController@list')->name('product.list');
        Route::get('/product/buy/{id}', 'ProductController@buy')->name('product.buy');
        Route::get('/product/my', 'ProductController@myProducts')->name('product.my');
    }

}
  