<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session; 
use App\User;
use App\Mailer;
use App\Note;
use App\Models\Question;
use App\Models\Settings;

class HomeController extends Controller
{  
    /** 
     * Create a new controller instance. 
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'check.user.data', 'verified', 'last.seen']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */ 
    public function index()
    {
        $nickname = Auth::user()->nickname;
        return redirect()->route('userProfile', [$nickname]);
    }

    public function getAsk() 
    {
        $questions = Auth::user()->questions()->orderBy('answer')->orderBy('created_at', 'DESC')->paginate(10);
        return view('site.ask', [
            'questions' => $questions
        ]);
    }
 
    public function postAsk(Request $request)
    {
        if ($request->body && $request->subject) {
            $mailer = new Mailer();
            
            $body = view('mail.ask', [
                'subject' => $request->subject,
                'body' => $request->body
            ])->render(); 

            $question = Question::create([
                'user_id' => Auth::user()->id,
                'subject' => $request->subject,
                'text' => $request->body
            ]);

            if ($question) {
                $settings = Settings::first();
                $settings->question_count = $settings->question_count + 1;
                $settings->update();
            } 

            if($mailer->send(User::admin()->email, $request->subject, $body, Auth::user()->email))
            { 
                Session::flash('message', __('message.question.received'));
            }
            return redirect()->back();
        }
    } 

    /**
     * Radio list.
     *
     * @return view
     */ 
    public function radio()
    {
        return view('site.radio');
    }
}
