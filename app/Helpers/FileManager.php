<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class FileManager
{
    public static function upload($file, $path)
    {
    	if ($file) {
    		$newFileName = md5(microtime()).'.'.$file->getClientOriginalExtension();
    		Storage::disk('site')->put($path.$newFileName, File::get($file));
    		return $newFileName;
    	}
    } 
 
    public static function delete($file, $path)
    {
        if($file != ''){
            if (Storage::disk('site')->exists($path . $file)) {
                return Storage::disk('site')->delete($path.$file);
            }
            return false;
        }
    }
}
 