<?php

namespace App\Helpers;
use Cookie;


class RobokassaHelper
{

	public static function identifyDomain($domain)
	{
		switch ($domain) {
			case env('DOMAIN_ENG'):
				return 'ENG';
				break;
			case env('DOMAIN_COM'):
				return 'COM';
				break;
			default:
				return 'RU';
				break;
		}
	}

	public static function getLogin($domain)
	{
		$site = self::identifyDomain($domain);
		return env('ROBOKASSA_MERCHANT_LOGIN_'.$site);
	}

	public static function getPassword($domain)
	{
		$site = self::identifyDomain($domain);
		return env('ROBOKASSA_PASSWORD_1_'.$site);
	}

	public static function getPasswordSecond($domain)
	{
		$site = self::identifyDomain($domain);
		return env('ROBOKASSA_PASSWORD_2_'.$site);
	}

	public static function getTestMode($domain)
	{
		$site = self::identifyDomain($domain);
		return env('ROBOKASSA_TEST_MODE_'.$site);
	}
  
 
}
 