<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Cache;

class NoteHelper
{
	public static function userLikesAndAdavais($notes)
	{
		if (!$notes) exit;
        $likes = Auth::user()->noteLikes()->whereIn('note_id', $notes->pluck('id'))->get(['note_id'])->pluck('note_id');
        $subscriptions = Auth::user()->adavaiki()->whereIn('note_id', $notes->pluck('id'))->get(['note_id'])->pluck('note_id');

        $notes->each(function($note) use ($likes, $subscriptions){
            $note["user_liked"] = $likes->contains($note->id) ? true : false;
            $note["user_adavai"] = $subscriptions->contains($note->id) ? true : false;
        });

        return $notes;
	}

	public static function markUserReposts($notes, $reposts)
	{
        $notes->each(function($note) use ($reposts){
        	foreach ($reposts as $key => $val) {
        		if ($note->id == $val) $note["repost"] = $key;
        	}
        });
        return $notes;
	}

    public static function userViewedNote($noteId, $userId)
    {
        $views = Cache::get('note_user_views_'.$noteId);
        if ($views) {
            return $views->contains($userId); 
        }
        return false;
    }

    public static function updateNoteViewers($noteId, $userId)
    {
        $views = Cache::get('note_user_views_'.$noteId);
        $updated = $views ? $views->push($userId) : collect([$userId]); 
        Cache::put('note_user_views_'.$noteId, $updated, now()->addDays(30));
    }
 
}
 