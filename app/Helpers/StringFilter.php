<?php

namespace App\Helpers;

use App\Models\Settings;

class StringFilter
{
	public static function filter($haystack, $offset=0)
	{
	    
	    $needle = Settings::select('bad_words')->first();

	    if (!$needle) return false;

	    $needle = explode(",", $needle->bad_words);

	    foreach($needle as $query) {
	        if(strpos($haystack, trim($query), $offset) !== false) return true; // stop on first true result
	    }
	    return false; 
	}

	public static function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

}
 