<?php

namespace App\Helpers;
use Cookie;


class LocaleHelper
{
	public static function userTimeZone()
	{
		if (Cookie::has('userTimeZoneFromSettings')) {
			
			return Cookie::get('userTimeZoneFromSettings');

		} else {
        	return config('app.timezone');
        }
	}
  
 
}
 