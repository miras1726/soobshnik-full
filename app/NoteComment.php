<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Support\Facades\Auth;
use App;
use Date;
use App\Helpers\LocaleHelper;

class NoteComment extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'note_id',
        'user_id',
        'text'
        
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        Date::setlocale(App::getLocale());
    }


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['posted_at'];

    /**
     * Relation to note
     *
     * @return relation
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Relation to comment
     *
     * @return relation
     */
    public function note()
    {
        return $this->belongsTo('App\Note', 'note_id', 'id');
    }

    /**
     * Get the administrator flag for the user.
     *
     * @return bool
     */
    public function getPostedAtAttribute()
    {
        return Date::parse($this->attributes['created_at'])
                ->setTimezone(LocaleHelper::userTimeZone())
                ->format('j F Y H:i');
    }

    /**
     * Check if user can edit/delete
     *
     * @return boolean
     */
    public function canManage($userId)
    {
        return Auth::user()->id === $userId;
    }

}
