<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissionLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mission_links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('mission_id')->unsigned();
            $table->string('link');
            $table->timestamps();

        $table->foreign('mission_id')
            ->references('id')
            ->on('missions')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mission_links');
    }
}
