<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissionIdToMarkedUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marked_users', function (Blueprint $table) {
            $table->integer('mission_id')->default(0)->after('marked_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marked_users', function (Blueprint $table) {
            $table->dropColumn('mission_id');
        });
    }
}
