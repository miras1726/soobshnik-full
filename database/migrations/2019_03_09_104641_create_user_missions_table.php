<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_missions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('mission_id')->unsigned();
            $table->enum('status', ['active', 'refuse', 'finish'])->default('active');
            $table->text('review')->nullable();
            $table->date('finished_at')->nullable();
            $table->double('motivation')->nullable();
            $table->double('support')->nullable();
            $table->double('punctuality')->nullable();
            $table->double('communication')->nullable();
            $table->integer('completed')->nullable();
            $table->integer('completed_time')->nullable();
            $table->timestamps();

        $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

        $table->foreign('mission_id')
            ->references('id')
            ->on('missions')
            ->onDelete('cascade');

        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_missions');
    }
}
