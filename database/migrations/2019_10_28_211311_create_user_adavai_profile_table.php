<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAdavaiProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_adavai_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->float('rating')->default(0);
            $table->text('full_address')->nullable();
            $table->string('passport')->nullable();
            $table->integer('verified')->default(0);
            $table->timestamp('license')->nullable();
            $table->integer('auto_prolong')->default(0);
            $table->integer('enable_notifications')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_adavai_profile');
    }
}
