<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mission_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('complice_id')->unsigned();
            $table->float('user_rating')->default(0);
            $table->float('complice_rating')->default(0);
            $table->integer('accepted')->default(0);
            $table->timestamps();

        $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

        $table->foreign('complice_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

        $table->foreign('mission_id')
            ->references('id')
            ->on('missions')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complices');
    }
}
