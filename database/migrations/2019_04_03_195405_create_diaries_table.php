<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mission_id')->nullable()->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('goal')->nullable();
            $table->text('goal_for_week')->nullable();
            $table->text('goal_daily')->nullable();
            $table->integer('write_period')->nullable();
            $table->integer('email_frequency')->nullable();
            $table->date('should_finished_at')->nullable();
            $table->timestamps();

        $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

        $table->foreign('mission_id')
            ->references('id')
            ->on('missions')
            ->onDelete('set null');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diaries');
    }
}
