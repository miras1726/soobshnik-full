<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nickname')->unique()->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('name')->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->string('city')->nullable();
            $table->text('quote')->nullable();
            $table->text('info')->nullable();
            $table->date('born')->nullable();
            $table->string('img')->nullable();
            $table->string('identity')->nullable();
            $table->integer('freezed')->nullable()->default(0);
            $table->float('rating')->default(0);
            $table->float('balance')->default(0);
            $table->float('money')->default(0);
            $table->string('skype')->nullable();
            $table->integer('admin')->default(0);
            $table->dateTime('last_seen')->nullable();
            $table->enum('locale', ['ru', 'en'])->default('ru');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
