<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MissionCategoriesTableSeeder extends Seeder
{
	private $categories = [
		'Здоровье/Health',
		'Саморазвитие/Personal development',
		'Тимуровцы/Helpful Hand',
		'Другое/Other'
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach($this->categories as $cat){
	        DB::table('mission_categories')->insert([
	            'title' => $cat,
	        ]);
    	}
    }
}
