<?php 

return [

	'shop' => 'Store',
	'buy' => 'Buy',
 
	//accomplice-list
	'accomplices' => 'Associates', 
	'delete' => 'Delete',

	//complice-list
	'complices' => 'Accomplices',
	'cancel.request' => 'Cancel',
	'accept.request' => 'Accept',
	'decline.request' => 'Decline',
	'remove.complice' => 'Remove',
	'remove.complice' => 'Remove',
	'search' => 'Search',
	'waiting' => 'Waiting',
  
	//search results
	'search.results' => 'Search results',
	'hook' => 'Hook',
	'remove' => 'Remove',
 
	//liked
	'liked.notes' => 'Liked',  

	//my-profile
	'my.profile' => 'My profile',
	'delete' => 'Delete',
	'upload' => 'Upload',
	'upload.image' => 'Upload image',
	'missions' => 'Missions',
	'name' => 'Name',
	'male' => 'Male',
	'female' => 'Female',
	'born' => 'Date&nbsp;of&nbsp;birth',
	'registration.date' => 'Registration&nbsp;date',
	'city' => 'Location',
	'info' => 'Information',

	//profile-delete
	'profile.delete' => 'Delete profile',
	'profile.delete.text' => 'You will lose all your missions, accomplices, marked users, rating, balance. You can retrieve your account in 3 months otherwise your profile will be deleted forever.',
	'continue' => 'Continue',

	//profile-retrieve
	'retrieve.profile' => 'Retrieve profile',
	'retrieve' => 'Retrieve',
	'welcome.back.text' => 'Welcome back, You can retrieve your account by clicking button above',

	//profile
	'profile.user' => 'User profile',
	'profile' => 'Profile',
	'mark' => 'Mark',
	'unmark' => 'Unmark',

	//settings
	'profile.settings' => 'Profile settings',
	'nickname' => 'Nickname',
	'gender' => 'Gender',
	'personal.statement' => 'Personal statement',
	'none.selected' => 'None selected',
	'save' => 'Save',
	'locale' => 'Language',
	'en' => 'English',
	'ru' => 'Russian',

	//short-info
	'on.site.days' => 'Joined :days days ago',
	'completed.missions' => 'Missions completed',

	'refuse.title' => 'Removing the complice',
	'refuse.reason' => 'Reason',

	'new.password' => 'New password',
	'new.password.accept' => 'Confirm new password',
	'timezone' => 'Timezone',
	'not.selected' => 'Not selected',

	'balance.replenish' => 'Balance replenish',
	'balance.current' => 'Current balance',
	'roubles' => 'roubles',
	'sum' => 'Sum',
	'replenish' => 'Replenish',
	'balance.resplenish.success' => 'Your payment successfully accepted',
];
