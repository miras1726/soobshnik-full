<?php

return [


	'edit' => 'Edit',
	'delete' => 'Delete',
	'report' => 'Report',
	'confirm' => 'Confrim please',
	'save' => 'Save',
	'close' => 'Close',
	'your.comment' => 'Your comment',
	'add' => 'Add',
	'detach' => 'Detach from wall',
	'share' => 'Share',
	'from.diary' => 'diary',

	'new.note' => 'New note',
	'send' => 'Send',
	'your.text' => 'Your text...',

	'fancy.btn' => 'Fancy', 
	'fancy.close' => 'Close', 

	'all.notes' => 'All notes',
	'my.notes' => 'My notes',
	'new.note' => 'New note',
	'search' => 'Search',
	'at' => '\a\t',
 
]; 