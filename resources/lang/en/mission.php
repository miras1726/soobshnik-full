<?php

return [
	//done missions
	'missions' => 'Missions',
	'finish' => 'Finish',
	'finished' => 'Finished',
	'review' => 'Review',
	'complices' => 'Accomplices', 
	'started' => 'Started',

	//mission
	'completed.time' => 'Mission accomplished',
	'on.mission' => 'On mission now',
	'photo.reports' => 'Photo-reports',
	'photo.reports.text' => 'In order to add photo reports you should accomplish any mission. To add visit mission page then click "Photo-reports"',
	'refuse' => 'Postpone',
	'join' => 'Join',  
	'useful.links' => 'Useful links',   

	//report
	'is.completed' => 'Is mission completed?',
	'mission.report' => 'Report about mission',
	'is.completed.in.time' => 'Is mission completed in desired time?', 
	'yes' => 'Yes', 
	'no' => 'No',
	'punctuality' => 'Punctuality', 
	'support' => 'Support',
	'motivation' => 'Motivation',  
	'communication' => 'Communication',
	'send' => 'Send', 

	//vue component
	'create.new' => 'Add new',
	'edit' => 'Edit',
	'delete' => 'Delete',
	'report' => 'Report',
	'actual.news' => 'Relevant news',

	//status
	'current' => 'Current',
	'completed' => 'Completed',
	'refused' => 'Postponed',

	'open.chat' => 'Open chat',

	'resume' => 'Resume',

	'started.at' => 'Started',
	'finished.at' => 'Finished',
	
	'archive' => 'Archive',
	'start.again' => 'Start again',

];