<?php

return array (
  'new' => 
  array (
    'group' => 'New group',
  ),
  'title' => 'Title',
  'description' => 'Description',
  'image' => 'Image',
  'send' => 'Create',
  'edit' => 'Edit',
  'delete' => 'Delete',
  'interesting' => 
  array (
    'pages' => 'Interesting pages',
  ),
  'subscribers' => 'subscribers',
  'subscribe' => 'Subscribe',
  'unsubscribe' => 'Unsubscribe',
  'create' => 'Create',
  'manage' => 'Manage',
  'add' => 'Add',
  'report' => 'Report',
  'nothing.found' => 'Nothing found',
  'actual' => 
  array (
    'news' => 'Relevant news',
  ),
);
