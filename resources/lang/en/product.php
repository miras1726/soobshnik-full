<?php 

return [
	'shop' => 'Store',
	'buy' => 'Buy',
	'my.products' => 'My purchase',
	'spend.stars' => 'Spend stars',
	'spend.money' => 'Spend money',
	'lack.of.money.or.star' => 'Balance or/and stars are not enough',

]; 