<?php

return [
	'mimes.error' => 'Only :values files allowed',
	'file.size.error' => 'File size must not exceed :size Kilobytes',
];