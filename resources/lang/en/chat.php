 <?php

return [

	'chat' => 'Chat',
	'chats' => 'Chats',
	'all.chats' => 'All chats',
	'search' => 'Search',
	'clear.history' => 'Clear history and delete chat',
	'message' => 'Message',
	'send' => 'Send',
	'total.participants' => 'Total participants',
	'participants' => 'participants',
	'just.now' => 'Just now',
	'mission.chats' => 'Mission chats',
	'tet.a.tet' => 'Tet-a-tet',
];