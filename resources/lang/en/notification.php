<?php

return [


	'notifications' => 'Notifications',
	'clear.all' => 'Clear all',
	'empty' => 'You do not have any notifications',
	'remove' => 'Remove',

]; 