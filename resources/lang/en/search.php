<?php

return [

	'mission' => 'Mission',
	'city' => 'City',
	'any' => 'Any',
	'age' => 'Age',
	'from' => 'from',
	'to' => 'to',
	'gender' => 'Gender',
	'male' => 'Male',
	'female' => 'Female',
	'search' => 'Search',
	'nickname' => 'Nickname',
];  