<?php

return [


	'ask' => 'Ask question',
	'subject' => 'Subject',
	'tech.support' => 'Technical support',
	'verification' => 'Verification',
	'wishes' => 'Suggestions for improving',
	'other' => 'Other',
	'text' => 'Text',
	'send' => 'Send',
	'my.questions' => 'My questions',
	'got.question.title' => 'New answer',
	'got.question.body' => 'You have received answer for your question, click link to read it',
];  