<?php

return [

	'complice.count.limit' => 'You can hook only 3 accomplices per mission',
	'user.not.in.mission' => 'You are not in this mission',
	'question.received' => 'Question has been sent. You will receive answer by email in three days',
	'balance.not.enough' => 'Your stars is not enough',
	'money.not.enough' => 'Your balance is not enough',
	'just.bought' => 'You have bought :product, within 3 days you will be contacted by the administrator for ordering',
	'email.exists' => 'User with this email address is already exists',
	'error.occured' => 'Error occured',
	'nickname.regex.error' => 'Nickname must consist of latin letters, digits and underscores(in lower case)',
	'mission.limit.error' => 'You can undertake only 2 missions at the same time',
	'badword.warning' => 'Bad word detected', 
	'complice.request' => ':nickname offers you to be an accomplices', 
	'complice.request.title' => 'New accomplice', 

	'complice.refused' => ':nickname removed you from accomplices in mission :mission and rated :rating <br> <b>:reason<b>', 
	'complice.refused.title' => 'Accomplice removed you', 

	'loading' => 'Loading...',

	'new.adavai.notification.title' => 'New fancy',
	'new.adavai.notification.text' => 'New fancy in your city, check out now',

	'adavai.profile.update' => 'Information sent for verification',

	'adavai.notification.joined.title' => 'Someone joined to your fancy',
	'adavai.notification.joined.text' => ':user - :content_short',

	'adavai.notification.left.title' => 'Someone left your fancy',
	'adavai.notification.left.text' => ':user - :content_short',

	'smth.went.wrong' => 'Something went wrong',

	'not.found' => 'Nothing found',

	'adavai.profile.confirmed.title' => 'Your organizer profile confirmed',
	'adavai.profile.confirmed.text' => 'Now you can add fancies',

	'adavai.award.won.title' => 'You\'ve won an award',
	'adavai.award.won.text' => 'Go to profile in order to see award',
];      