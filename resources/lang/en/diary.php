<?php

return [
	'create.new.diary' => 'Create new diary',
	'new.diary' => 'New diary',
	'mission' => 'Mission',
	'date.ending' => 'Date of finishing',
	'goal' => 'Main goal',
	'week.goal' => 'Goal for week',
	'daily.goal' => 'Daily goal',
	'how.often.fill' => 'How often<br> would you like to fill in the diary',
	'once.day' => 'Once per day',
	'once.week' => 'Once per week',
	'once.month' => 'Once per month',
	'how.often.receive' => 'How often would you like<br> to receive an email report',
	'send' => 'Send',

	//list 
	'diaries' => 'Diaries',  
	'my.diaries' => 'My diaries',
	'create' => 'Create',
	'open' => 'Open',
	'finished' => 'Finished', 
	'finishing' => 'Finishing',
	'notes' => 'notes',

	//note-edit
	'edit.note' => 'Edit note',
	'goal.reached' => 'Have you achieved your daily/weekly target?',
	'is.easy' => 'Was it easy to achieve your daily/weekly target?',
	'motivation.enough' => 'Did you get enough motivation  from your accomplices?',
	'review' => 'Review',
	'send' => 'Send',

	//single
	'diary' => 'Diary',
	'edit' => 'Edit',
	'delete' => 'Delete',
	'refresh' => 'Update',
	'share' => 'share',
	'remove' => 'remove',
	
	'refused' => 'Postponed',

	//write
	'diary.update' => 'Update diary',

	'progress.title' => 'Progress',
	'goal.reached.title' => 'Goal reached',
	'is.easy.title' => 'Is easy?',
	'motivation.enough.title' => 'Motivation',

	'diary.exists' => 'Diary already exists',
	'yes' => 'Yes',
	'no' => 'No',

	'date.is.in.past' => 'No',

	'date.is.in.past' => 'Finishing date is in past or today',
	'active.diaries' => 'Active diaries',
	'filled.diaries' => 'Filled diaries',
];