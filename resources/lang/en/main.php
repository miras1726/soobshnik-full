<?php

return array (
  'missions' => 'Missions',
  'fancy' => 'Fancy?!',
  'ask' => 'Support',
  'profile' => 'Profile',
  'diary' => 'Diary',
  'my' => 
  array (
    'missions' => 'My missions',
  ),
  'current' => 
  array (
    'missions' => 'Current missions',
  ),
  'done' => 
  array (
    'missions' => 'Completed missions',
  ),
  'refuse' => 
  array (
    'missions' => 'Postponed missions',
  ),
  'chats' => 'Chats',
  'complices' => 'Accomplices',
  'accomplices' => 'Associates',
  'interesting' => 
  array (
    'pages' => 'Interesting pages',
  ),
  'liked' => 'Liked',
  'share' => 
  array (
    'soc' => 'Share',
  ),
);
