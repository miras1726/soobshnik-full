<?php

return [
	'report.sent' => 'Report have been sent',
	'report.sent.text' => 'Thank you, we will consider your report in short time',
	'close' => 'Close',

	'badword.modal.title' => 'Forbidden',
	'badword.modal.body' => 'Your text contains forbidden words',

	'shared.title' => 'Shared',
	'shared.text' => 'You have shared with this note on your wall',


]; 