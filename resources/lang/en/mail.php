<?php

return [
	//diary-email
	'report.for.days' => 'Your report for :frequency days',
	'personal.ref.link' => 'Your personal ref link',
	'mission.progress' => 'Mission progress',
	'goal.reaching' => 'Achieving the goal in the selected period',
	'level' => 'Difficulty',
	'motivation.complice' => 'Motivation of accomplices',

	//password-reset
	'greeting' => 'Hello',
	'you.reveiced.mail' => 'You received this mail since you requested password reset',
	'reset.btn' => 'Reset password',
	'ignor' => 'If you did not want to reset you password, just ignore this mail',
	'go.link' => 'Click',

	//profile-freezing
	'profile.freezing' => 'Hello :nickname, <br>
						   You have been inactive for 30 days and after 7 days your account will be frezzed<br>
						   Login in order to cancel the proccess',

	//verify
	'account.activation' => 'Account activation',
	'click.link.to.activate' => 'To activate Your account click link below',
	'activate.btn' => 'Activate', 
	'click.link' => 'Click link',			 		   

	//birthday
	'bday.mail' => 'Today his/her birthday celebrating your',
	'accomplice.bday.subject' => 'Your associate is celebrating bithday',
	'complice.bday.subject' => 'Your accomplice is celebrating bithday',
	'complice' => 'accomplice',
	'accomplice' => 'associate',

	'you.received.mail.activation' => 'You have received an activation email, please check your email',
	'you.received.mail' => 'Please check your email',
	'reset.password' => 'Password reset',
	'account.will.freezed' => 'Account will be freezed soon',
	'report' => 'Diary report',

	'diary.reminder.title' => "Don't forget to fill out the diary",
	'diary.reminder.text' => "Hello :name, do not forget to fiil out your diary :diary",

];  