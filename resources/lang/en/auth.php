<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    //login
    'login.with.social' => 'Login with',
    'or' => 'or',
    'email.post' => 'Email',
    'password' => 'Password',
    'remember' => 'Remember me',
    'signin' => 'Sign in',
    'forgot.password' => 'Forgot password?',
    'not.registered' => 'Not registered yet?',
    'register.in.sec' => 'Register in 30 sec',

    //check-user-data
    'fill.fields' => 'Fill the fields below',
    'send' => 'Send',
    'nickname' => 'Nickname',

    //register
    'password.one.more' => 'Password one more time',
    'register' => 'Registration',
    'captcha' => 'Captcha',
    'to.register' => 'Register',
    'already.have.account' => 'Already have an account?',
    'accept.terms' => 'By proceeding I accept the',
    'user.terms' => 'user terms and conditions',

    //email-verify
    'verify.email' => 'Verify your E-mail',
    'verify.text' => 'In order to continue, you need to verify your Email address, please check your mail',
    'send.one.more' => 'Resend',
    'logout' => 'Logout',

    //password/reset
    'password.reset' => 'Retrieve password',
    'remember.password' => 'Remember password?',

    //password/reset page
    'reset.password' => 'Reset password'



];
 