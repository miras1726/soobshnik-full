<?php

return array (
  'new' => 
  array (
    'group' => 'Новая страница',
  ),
  'title' => 'Название',
  'description' => 'Описание',
  'image' => 'Изображение',
  'send' => 'Создать',
  'edit' => 'Изменить',
  'delete' => 'Удалить',
  'interesting' => 
  array (
    'pages' => 'Интересные страницы',
  ),
  'subscribers' => 'участников',
  'subscribe' => 'Подписаться',
  'unsubscribe' => 'Отписаться',
  'create' => 'Создать',
  'manage' => 'Управление',
  'add' => 'Добавить',
  'report' => 'Пожаловаться',
  'nothing.found' => 'Ничего не найдено',
  'actual' => 
  array (
    'news' => 'Актуальные новости',
  ),
);
