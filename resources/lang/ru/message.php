<?php

return [

	'complice.count.limit' => 'Можно завербовать только 3 сообщника на миссию',
	'user.not.in.mission' => 'Вы не выполняете данную миссию',
	'question.received' => 'Вопрос отправлен. Вы получите ответ на почту в течение трёх дней',
	'balance.not.enough' => 'У вас недостаточно звезд',
	'money.not.enough' => 'У вас недостаточно баланса',
	'just.bought' => 'Вы приобрели :product, в течении 3 дней с Вами свяжется администратор для оформления заказа', 
	'email.exists' => 'Пользователь с таким email адресом уже существует',
	'error.occured' => 'Произшошла ошибка',
	'nickname.regex.error' => 'Псевдоним должен состоять из латинских букв, цифр и нижнего подчеркивания(в нижнем регистре)',
	'mission.limit.error' => 'Одновременно можно выполнять 2 миссии',
	'badword.warning' => 'Замечено матерное слово', 
	'complice.request' => ':nickname предлагает вам быть сообщниками', 
	'complice.request.title' => 'Новый сообщник', 
	
	'complice.refused' => ':nickname отказался от вас как сообщника в миссии :mission и поставил оценку :rating <br> <b>:reason<b>', 
	'complice.refused.title' => 'Сообщник отказался от Вас', 

	'loading' => 'Загрузка...',

	'new.adavai.notification.title' => 'Адавайка рядом',
	'new.adavai.notification.text' => 'Новая адавайка в вашем городе',

	'adavai.profile.update' => 'Ваши данные отправлены на проверку',

	'adavai.notification.joined.title' => 'Кто то присоединился к вашей адавайке',
	'adavai.notification.joined.text' => ':user - :content_short',

	'adavai.notification.left.title' => 'Кто то покинул Вашу адавайку',
	'adavai.notification.left.text' => ':user - :content_short',

	'smth.went.wrong' => 'Что-то пошло не так',

	'not.found' => 'Ничего не найдено',

	'adavai.profile.confirmed.title' => 'Ваш профиль организатора подтвержден',
	'adavai.profile.confirmed.text' => 'Теперь вы можете добавлять адавайки',

	'adavai.award.won.title' => 'Вы выиграли награду',
	'adavai.award.won.text' => 'Перейдите в профиль чтобы посмотреть',

];      