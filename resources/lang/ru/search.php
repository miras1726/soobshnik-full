<?php

return [

	'mission' => 'Миссия',
	'city' => 'Город',
	'any' => 'Не важно',
	'age' => 'Возраст',
	'from' => 'от',
	'to' => 'до',
	'gender' => 'Пол',
	'male' => 'Мужской',
	'female' => 'Женский',
	'search' => 'Найти',
	'nickname' => 'Псевдоним',

];  