<?php

return [
	//done missions
	'missions' => 'Миссии',
	'finish' => 'Завершить',
	'finished' => 'Завершена',
	'review' => 'Отзыв',
	'complices' => 'Сообщники',
	'started' => 'Начата',

	//mission
	'completed.time' => 'Миссию выполнили',
	'on.mission' => 'Сейчас выполняют миссию', 
	'photo.reports' => 'Фотоотчёты',
	'photo.reports.text' => 'Для того чтобы добавить Вы должны выполнить любую миссию. Добавить можно на странице миссии перейдя по ссылке "Фотоотчёты"',
	'refuse' => 'Прервать',  
	'join' => 'Присоединиться',
	'useful.links' => 'Полезные ссылки',
 
	//report
	'is.completed' => 'Выполнена ли миссия?',
	'mission.report' => 'Отчет о миссии',
	'is.completed.in.time' => 'Выполнена ли миссия в рамках установленного срока?',
	'yes' => 'Да',
	'no' => 'Нет',
	'punctuality' => 'Пунктуальность',
	'support' => 'Поддержка',
	'motivation' => 'Мотивация', 
	'communication' => 'Общение', 
	'send' => 'Отправить',
 
	//vue component
	'create.new' => 'Создать новость',
	'edit' => 'Изменить',
	'delete' => 'Удалить',
	'report' => 'Пожаловаться',
	'actual.news' => 'Актуальные новости',

	//status
	'current' => 'Текущие',
	'completed' => 'Выполненные',
	'refused' => 'Прерванные',

	'open.chat' => 'Открыть чат',

	'resume' => 'Возобновить',

	'started.at' => 'Начата',
	'finished.at' => 'Завершена',
	
	'archive' => 'Архив',
	'start.again' => 'Начать заново',

];