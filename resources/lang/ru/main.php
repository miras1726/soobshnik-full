<?php

return array (
  'missions' => 'Миссии',
  'fancy' => 'А давай!',
  'ask' => 'Задать вопрос',
  'profile' => 'Профиль',
  'diary' => 'Дневник',
  'my' => 
  array (
    'missions' => 'Мои миссии',
  ),
  'current' => 
  array (
    'missions' => 'Текущие миссии',
  ),
  'done' => 
  array (
    'missions' => 'Выполненные миссии',
  ),
  'refuse' => 
  array (
    'missions' => 'Прерванные миссии',
  ),
  'chats' => 'Чаты',
  'complices' => 'Сообщники',
  'accomplices' => 'Единомышленники',
  'interesting' => 
  array (
    'pages' => 'Интересные страницы',
  ),
  'liked' => 'Понравившиеся',
  'share' => 
  array (
    'soc' => 'Поделиться в соц сетях',
  ),
);
