<?php

return [

	'chat' => 'Чат',
	'chats' => 'Чаты',
	'all.chats' => 'Все чаты',
	'search' => 'поиск',
	'clear.history' => 'Очистить историю и удалить чат',
	'message' => 'Сообщение',
	'send' => 'Отправить',
	'total.participants' => 'Всего участников',
	'participants' => 'участников',
	'just.now' => 'Только что',
	'mission.chats' => 'Чаты миссий',
	'tet.a.tet' => 'Тет-а-тет',

]; 