<?php 

return [

	'shop' => 'Магазин',
	'buy' => 'Купить',

	//accomplice-list
	'accomplices' => 'Единомышленники', 
	'delete' => 'Удалить',

	//complice-list
	'complices' => 'Сообщники', 
	'cancel.request' => 'Отозвать',
	'accept.request' => 'Принять', 
	'decline.request' => 'Отклонить',
	'remove.complice' => 'Отказаться от сообщника', 
	'search' => 'Поиск',
	'waiting' => 'Ожидает',

	//search results
	'search.results' => 'Результаты поиска',
	'hook' => 'Завербовать', 
	'remove' => 'Удалить',

	//liked
	'liked.notes' => 'Понравившиеся', 

	//my-profile
	'my.profile' => 'Мой профиль',
	'delete' => 'Удалить',
	'upload' => 'Загрузить',
	'upload.image' => 'Загрузить изображение',
	'missions' => 'Миссии',
	'name' => 'Имя',
	'male' => 'Мужской',
	'female' => 'Женский',
	'born' => 'Дата&nbsp;рождения',
	'registration.date' => 'Дата&nbsp;регистрации',
	'city' => 'Город',
	'info' => 'Информация',

	//profile-delete
	'profile.delete' => 'Удалить профиль',
	'profile.delete.text' => 'Все ваши миссии, сообщники,отмеченные участники, рейтинг, накопленные звезды-рейтинг теряются.Частично восстановиться можно в течение трёх месяцев или профиль будет удалён окончательно и бесповоротно',
	'continue' => 'Продолжить',

	//profile-retrieve
	'retrieve.profile' => 'Восстановить профиль',
	'retrieve' => 'Восставновить',
	'welcome.back.text' => 'Добро пожаловать обратно, Вы можете восстановить профиль обратно нажав кнопку ниже',

	//profile
	'profile.user' => 'Профиль пользователя ',
	'profile' => 'Профиль',
	'mark' => 'Отметить',
	'unmark' => 'Удалить из отмеченных',

	//settings
	'profile.settings' => 'Настройки профиля',
	'nickname' => 'Псевдоним',
	'gender' => 'Пол',
	'personal.statement' => 'Статус',
	'none.selected' => 'Не выбрано',
	'save' => 'Сохранить',
	'locale' => 'Язык',
	'en' => 'Английский',
	'ru' => 'Русский',

	//short-info
	'on.site.days' => 'На сайте :days дней',
	'completed.missions' => 'Выполнено миссии',

	'refuse.title' => 'Отказ от сообщника',
	'refuse.reason' => 'Причина',

	'new.password' => 'Новый пароль',
	'new.password.accept' => 'Подтвердите пароль',
	'timezone' => 'Временная зона',
	'not.selected' => 'Не выбрано',

	'balance.replenish' => 'Пополнить баланс',
	'balance.current' => 'Сейчас на счету',
	'roubles' => 'руб',
	'sum' => 'Сумма',
	'replenish' => 'Пополнить',
	'balance.resplenish.success' => 'Ваша оплата успешно завершена'
];