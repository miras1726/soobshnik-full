<?php

return [
	'mimes.error' => 'Файлы должны быть формата :values',
	'file.size.error' => 'Размер файла не должен превышать :size Килобайт(а)',
];