import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        adavaiNotes: [],
        userNotes: [],
        groupNotes: [],
        missionNotes: [],
        likedNotes: [],
        reportNotes: [],
        photoNotes: [],
        sharingNoteID: null,
    },
});