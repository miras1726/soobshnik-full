import lang from 'lang.js';
import translations from '../../public/messages';

const Lang = new lang({
	messages: translations
}); 

export default Lang; 