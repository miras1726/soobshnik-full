export const noteScrollMixin = {
	updated: function () {
	  this.$nextTick(function () {
		this.scroll_Scroll();
	  });
	},
	data(){
		return {
	        scroll_ToNote: true,
	        page: 1,
			loadNotes: true,
		}
	},
	methods:{
		scroll_Scroll(){
			const queryNote = this.scroll_GetParam('note');
			if (!Number.isInteger(parseInt(queryNote))) return;
			if (!this.scroll_ToNote || !queryNote) return; 
			this.scroll_ToNoteFunc(queryNote);	
		},
		scroll_ToNoteFunc(queryNote){
			if (!$("#note-"+queryNote).length) return;
			$('html, body').animate({
		        scrollTop: parseInt($("#note-"+queryNote).offset().top) - 75
		    }, 1000);
		    this.scroll_ToNote = false;
		},
		scroll_GetParam(param){
	  		const urlParams = new URLSearchParams(window.location.search);
			return urlParams.get(param);
		},
	}
}  