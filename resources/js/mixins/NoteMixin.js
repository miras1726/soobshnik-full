import route from '../route'; 
export const noteMixin = {
	computed:{
		noteList(){
			return this.$store.state[this.storeNoteType];
		}
	},
	methods:{
		report(noteId){
			axios.get(route('note.report', [noteId]))
				.then((response) => {
					$("#reported-modal").modal('show');
				});
		},
		detach(repostId, index){
			axios.get(route('note.detach', [repostId]))
				.then((response) => {
					if(response.data.detached){
						this.$store.state[this.storeNoteType].splice(index, 1, 1);
					}
			});
		},
		deleteNote(noteId, index){
			if (confirm("Вы уверены?")) {
				axios.get(route('note.delete', [noteId]))
					.then((response) => {
						if(response.data.deleted){
							const note = this.noteList.find(note => {
								return note.id == noteId;
							});
							this.noteList.splice(this.noteList.indexOf(note), 1);
						}
				});
			}
		},
		adminDelete(noteId, index){
			if (confirm("Вы уверены?")) {
				axios.get(route('admin.delete.note', [noteId]))
					.then((response) => {
						const note = this.noteList.find(note => {
							return note.id == noteId;
						});
						this.noteList.splice(this.noteList.indexOf(note), 1);
					});
			}
		},
		editNote(noteId, index){
			const note = this.noteList.find(note => {
				return note.id == noteId;
			});
			this.$set(note, 'edit', true);
		},
		noteEdited(data){
			const note = this.noteList.find(note => {
				return note.id == data.note.id;
			});
			this.$set(note, 'edit', false);
			this.$set(note, 'images', data.note.images);
			this.$set(note, 'files', data.note.files);
			this.hideComments = true;
		},
		closeComments(){
			var d = new Date();
			var n = d.getMilliseconds();
			Vue.set(self, 'close_comments', n);
			return n;
		},
	} 
}  