
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('es6-promise').polyfill();

import route from './route';  
import Lang from './lang';  
import axios from 'axios';  
import { store } from './store/store';  

require('./bootstrap');
 
window.Vue = require('vue');

Vue.config.productionTip = false;
Vue.config.silent = true;
Vue.use(require('vue-filter-pretty-bytes'));
/** 
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('notes-component', require('./components/Note/NotesComponent.vue').default);
Vue.component('note-comments-component', require('./components/Note/NoteCommentsComponent.vue').default);
Vue.component('mission-post', require('./components/MissionPostComponent.vue').default);
Vue.component('group-post', require('./components/GroupPostComponent.vue').default);
Vue.component('photo-report', require('./components/PhotoReportComponent.vue').default);
Vue.component('adavai-notes', require('./components/AdavaiComponent.vue').default);
Vue.component('liked-notes', require('./components/Note/LikedNotesComponent.vue').default);
Vue.component('public-chat', require('./components/PublicChat.vue').default);
Vue.component('private-chat', require('./components/PrivateChat.vue').default);
Vue.component('report-notes', require('./components/ReportNotes.vue').default);
Vue.component('note-search-component', require('./components/NoteSearchComponent.vue').default);
Vue.component('note-share-component', require('./components/Note/NoteShareComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({ 
    el: '#app',
    store
});


/* Custom js */

$("#show-side-menu").click(function(e){
	e.preventDefault();
	$("#side-menu").toggle();
	$("#site-overlay").toggle();
});
$("#site-overlay").click(function(){
	$("#side-menu").toggle();
	$("#site-overlay").toggle();
});
$("#show-right-nav").click(function(e){
	e.preventDefault();
	$("#top-right-navigation").toggle();
}); 

$(".ajax-form").submit(function(event){
	
	event.preventDefault();
	var form = $(this);
	var formData = new FormData(this);
	if (!form) return;

	$.ajax({
		url: form.attr('action'),
		type: form.attr('method'),
		data: formData,
		processData: false,
    	contentType: false,
		headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
		success: function(response){

			var obj = jQuery.parseJSON( response );
			
			if (obj.status != 1) {
				alert(obj.message);
			}

			if (obj.link) {
				window.location.href = obj.link;
			}

			if (obj.callbackFunction) {
				eval(obj.callbackFunction+'(obj, form)');
			}


		},
		error: function(){
			alert("Произошла ошибка, попробуйте позже");
		},
		complete: function(){

		}
	});

});

$(".confirm-required").click(function(){
	return confirm('Вы уверены?');
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip({html: true})
});

$(".progress-outer").each(function(r,s){var e,n=$(s).find(".progress-inner").width()/$(s).find(".progress-inner").parent().width()*100;e=n<10?"progress-min":n<70&&10<n?"progress-med":"progress-max",$(s).addClass(e)});