<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ url('/public/css/app.css?') . time() }}" rel="stylesheet">
    
    <!-- Fancyapp -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    @yield('css')
</head>
<body>
    <div id="app" class="h-100">
        @yield('main') 
    </div>

    <!-- Scripts -->
    <script src="{{ url('/public/js/app.js?') . time() }}" ></script>
    <!-- Fancyapp -->
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js" defer></script>
    <script type="text/javascript" src="{{ url('/public/vendor/jsvalidation/js/jsvalidation.js')}}" defer></script>
    <script type="text/javascript" language="javascript">
        var orruMyRad = "154,256,336,612,1094,2571,3499,28,35,46,126,315,882,2943,3486,2417";
        var orruBlockWidth = 240, orruImgKol = 3;
    </script>
    <script src="https://onlayn-radio.ru/script/rad.js" type="text/javascript" defer="defer"></script>
    <script src="https://cdn.jsdelivr.net/npm/sharer.js@latest/sharer.min.js"></script>

<script language="javascript">
if (window.document.documentMode && window.location.pathname !== '/ie/deny') {
  window.location.href = '/ie/deny';
}
</script>

    @yield('js-app')
</body>
</html>
