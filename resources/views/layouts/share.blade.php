<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <title>{{ $note->content_short }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="{{ $note->content }}">


        <meta property="og:url" content="{{ route('terminated.share.note', $note->id) }}">
        <meta property="og:site_name" content="{{ route('adavai.list') }}">
        <meta property="og:image" content="{{ url('/public/img/logo.png') }}">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1291">
        <meta property="og:image:height" content="315">

        <meta name="msapplication-TileColor" content="#7C9F58">
        <meta name="theme-color" content="#7C9F58">
</head>
<body>
        <script type="text/javascript">
                window.location.href = "<?= route('adavai.list', ['note' => $note->id]) ?>";
        </script>
</body>
</html> 