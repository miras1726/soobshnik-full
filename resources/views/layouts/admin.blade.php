 @extends('layouts.app')

@section('main')
	<header class="mb-1 fixed-top">
		<div class="col-12 py-3 px-4" id="header">
			<div class="row">
				<div class="col-1 d-md-none p-0 text-center"> 
					<a href="#" id="show-side-menu"><i class="fas fa-bars"></i></a>
				</div>
				<div class="col-md-2 col-10 col-offset-1 text-center">
					<a href="/"><img src="/public/img/logo.png" class="img-fluid" alt="logo"></a>
				</div>
				<div class="col-md-7 d-md-block d-none">
				</div>
				<div class="col-lg-3 col-md-4 col-12 text-right d-lg-block" id="top-right-navigation">
						
					<ul class="nav top-right-pages justify-content-end">
					  <li class="nav-item">
					    <a class="nav-link" href="{{ route('logout') }}"
					    		onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
					    	<i class="fas fa-sign-out-alt"></i>
					    </a>
					    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
					  </li>
					</ul>  
				</div>
				<div class="col-1 col-md-3 p-0 text-center text-md-right d-lg-none">
					<a href="#" id="show-right-nav">
						<i class="fas fa-th"></i>
					</a>
				</div>
			</div>
		</div>	
  </header>
  <div class="col-12" id="content">
	  <div class="row">
	  	
		  <aside class="col-lg-2 col-md-3 p-0 d-md-block " id="side-menu">
		  		<div class="col-12 m-0 p-2 sticky-top side-menu">

		  			<div class="col-12 mt-2 px-4">
		  				<ul class="side-nav">
		  					<li>
		  						<a href="{{ route('admin.index') }}">Админ панель</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.country') }}">Страны</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.city') }}">Города</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.user') }}">Пользователи</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.mission') }}">Миссии</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.product') }}">Продукты</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.orders') }}">Заказы 
		  							@widget('admin.show.count', ['property' => 'order_count']) 
		  						</a>
		  					</li> 
		  					<li>
		  						<a href="{{ route('admin.group') }}">Группы</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.report') }}">Жалобы
									@widget('admin.show.count', ['property' => 'report_count']) 
		  						</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.get.questions') }}">Вопросы
									@widget('admin.show.count', ['property' => 'question_count']) 
		  						</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.badword') }}">Слова-паразиты</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('admin.adavai.profiles.get') }}">Организаторы</a>
		  					</li>
		  				</ul>
		  			</div>
		  		</div>
		  </aside>

		  @yield('content')
	  
	  </div>
  </div>

<div id="site-overlay"></div>

@include('site.modals')

@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="/public/css/emojionearea.min.css">
	<link rel="stylesheet" type="text/css" href="/public/css/star-rating-svg.css" defer>
@endsection

@section('js-app')
<script type="text/javascript" src="/public/js/emojionearea.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $(".emoji-enabled").emojioneArea(); 
  });

$(function() {

  // basic use comes with defaults values
  $(".star-rating-readonly").starRating({
  	readOnly: true,
    starSize: 15,
    useGradient: false,
    activeColor: '#F7AA28', 
    ratedColor: '#F7AA28',
    starShape: 'rounded'
  });

});
  
</script>
<script src="/public/js/jquery.star-rating-svg.min.js" defer></script>
@yield('js')
@endsection 
