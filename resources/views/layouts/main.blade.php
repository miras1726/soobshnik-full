@extends('layouts.app')

@section('main')
	<header class="mb-1 fixed-top">
		<div class="col-12 py-3 px-4" id="header">
			<div class="row" id="header-inner">
				<div class="col-1 d-md-none p-0 text-center"> 
					<a href="#" id="show-side-menu"><i class="fas fa-bars"></i></a>
				</div>
				<div class="col-md-2 col-10 col-offset-1 text-center">
					<a href="/"><img src="/public/img/logo.png" class="img-fluid" alt="logo"></a>
				</div> 
				<div class="col-md-7 d-md-block d-none">
					<ul class="nav top-pages">
					  <li class="nav-item">
					    <a class="nav-link" href="{{ route('mission.list') }}">{{ __('main.missions') }}</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="{{ route('adavai.list') }}">{{ __('main.fancy') }}</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="{{ route('app.ask.get') }}">{{ __('main.ask') }}</a>
					  </li>
					  @if(Auth::user()->isAdmin())
						<li class="nav-item">
					    	<a class="nav-link" href="{{ route('admin.index') }}">Админ-панель</a>
					  	</li>
					  @endif
					</ul>
				</div>
				<div class="col-lg-3 col-md-4 col-12 text-right d-lg-block" id="top-right-navigation">
						
					<ul class="nav top-right-pages justify-content-end">
					  <li class="nav-item">
					    <a class="nav-link color-gray" href="{{ route('set.locale', [((Cookie::get('locale') == 'ru') ? 'en' : 'ru')]) }}">
					    	{{ (Cookie::get('locale') == 'ru') ? 'en' : 'ru' }}
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link {{ (Auth::user()->notifications->count() > 0) ? 'text-danger' : '' }}" href="{{ route('notification.list') }}">
					    	<i class="fas fa-bell"></i>
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="{{ route('product.list') }}">
					    	<i class="fas fa-star"></i>
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#" onClick="window.open('{{ route('radio') }}','pagename','resizable,height=260,width=300,left=500,top=100'); return false;">
					    	<i class="fas fa-music"></i>
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="{{ route('settings') }}">
					    	<i class="fas fa-cog"></i>
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="{{ route('logout') }}"
					    		onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
					    	<i class="fas fa-sign-out-alt"></i>
					    </a>
					    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
					  </li>
					</ul> 
				</div>
				<div class="col-1 col-md-3 p-0 text-center text-md-right d-lg-none">
					<a href="#" id="show-right-nav">
						<i class="fas fa-th"></i>
					</a>
				</div>
			</div>
		</div>	
  </header>
  <div class="col-12 h-100" id="content">
	  <div class="row h-100">
	  	
		  <aside class="col-lg-2 col-md-3 p-0 d-md-block" id="side-menu">
		  		<div class="col-12 m-0 p-2 sticky-top side-menu">

		  			<div class="col-12 side-profile px-4 py-2 d-md-block d-sm-none d-none">
		  				<div class="side-profile-img p-2">
					 		@if(Auth::user()->isAvatarExists())
			                    <img src="{{ Croppa::url('/public/img/users/'.Auth::user()->avatar(), 500, 500) }}" class="img-fluid" alt="profile-img">
			                @else
			                  <img src="/public/img/users/{{ Auth::user()->defaultAvatar }}" class="img-fluid" alt="profile-img">
			                @endif
		  				</div>
		  				<div class="side-profile-rating">
		  					<div class="star-rating-readonly" data-rating="{{ Auth::user()->rating }}"></div>
		  				</div>
		  				<div class="clearfix"></div>
		  			</div>


		  			<div class="col-12 mt-2 px-4 d-sm-block d-md-none">
		  				<ul class="side-nav">
		  				  <li>
					    	<a href="{{ route('mission.list') }}">{{ __('main.missions') }}</a>
						  </li>
						  <li>
						    <a href="{{ route('adavai.list') }}">{{ __('main.fancy') }}</a>
						  </li>
						  <li>
						    <a href="{{ route('app.ask.get') }}">{{ __('main.ask') }}</a>
						  </li>
						  @if(Auth::user()->isAdmin())
							<li>
						    	<a href="{{ route('admin.index') }}">Админ-панель</a>
						  	</li>
						  @endif
		  				</ul>
		  			<hr>
		  			</div>

		  			<div class="col-12 mt-2 px-4">
		  				<ul class="side-nav">
		  					<li>
		  						<a href="/">{{ __('main.profile') }}</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('diary.list') }}">{{ __('main.diary') }}</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('mission.user', ['active']) }}">{{ __('main.my.missions') }}</a>
		  						<ul class="sub-nav">
		  							<li>
				  						<a href="{{ route('mission.user', ['active']) }}">{{ __('main.current.missions') }}</a>
				  					</li>
				  					<li>
				  						<a href="{{ route('mission.user', ['finish']) }}">{{ __('main.done.missions') }}</a>
				  					</li>	
				  					<li>
				  						<a href="{{ route('mission.user', ['refuse']) }}">{{ __('main.refuse.missions') }}</a>
				  					</li>	
		  						</ul>
		  					</li>
		  					<li>
		  						<a href="{{ route('private-chat') }}">{{ __('main.chats') }}</a>
		  						@if(Auth::user()->unreadMessagesCount())
		  							<span class="side-nav-badge">{{ Auth::user()->unreadMessagesCount() }}</span>
		  						@endif 
		  					</li>
		  					<li>
		  						<a href="{{ route('complice.list') }}">{{ __('main.complices') }}</a>
		  					</li>
		  					<li> 
		  						<a href="{{ route('accomplice.list') }}">{{ __('main.accomplices') }}</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('group.list') }}">{{ __('main.interesting.pages') }}</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('photo.reports') }}">{{ __('mission.photo.reports') }}</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('note.user.liked.show') }}">{{ __('main.liked') }}</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('product.my') }}">{{ __('product.my.products') }}</a>
		  					</li>
		  					<li>
		  						<a href="{{ route('get.replenish.balance') }}">{{ __('user.balance.replenish') }}</a>
		  					</li>
		  				</ul>
		  			</div>
		  			<div class="col-12 mt-5 px-4 soc-share">
		  				<p>{{ __('main.share.soc') }}</p>
		  			</div>
		  			<div class="col-12 px-4">
						<div class="ya-share2 mb-2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter"></div>
					</div>
		  		</div>
		  </aside>

		  @yield('content')
	  
	  </div>
  </div>

<div id="site-overlay"></div>

@include('site.modals')

@endsection

@section('css')

<link rel="stylesheet" type="text/css" href="/public/css/emojionearea.min.css">
	<link rel="stylesheet" type="text/css" href="/public/css/star-rating-svg.css" defer>
@endsection
@section('js-app')

<script type="text/javascript" src="/public/js/emojionearea.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $(".emoji-enabled").emojioneArea();
  });

$(function() {

  // basic use comes with defaults values
  $(".star-rating-readonly").starRating({
  	readOnly: true,
    starSize: 15, 
    useGradient: false,
    activeColor: '#F7AA28', 
    ratedColor: '#F7AA28',
    starShape: 'rounded',
    emptyColor: '#e4e4e4'
  });

  $(".star-rating-readonly-gray").starRating({
  	readOnly: true,
    starSize: 15, 
    useGradient: false,
    activeColor: '#99a7aa', 
    ratedColor: 'yellow',
    starShape: 'rounded',
    emptyColor: '#e4e4e4'
  });

});
  
</script>
<script src="/public/js/jquery.star-rating-svg.min.js" defer></script> 
<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="//yastatic.net/share2/share.js"></script>
@yield('js')
@endsection 
