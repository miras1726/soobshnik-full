@extends('layouts.app')

@section('title')
  Сайт не поддерживает Internet Explorer
@endsection

@section('main')

    <div class="container"> 
      
      <div class="row justify-content-md-center my-5"> 
        <div class="col-md-4 block-white px-5 py-4">
          <div class="px-2">
            <img src="/public/img/logo.png" class="img-fluid my-3" alt="logo">
             
            <div class="form-group text-center">
              <p class="text-help">Сайт не поддерживает Internet Explorer</p>
              <small>Просим Вас использовать более современные браузеры</small>
            </div>
          </div> 
        </div>
      </div>

    </div>

@endsection
