<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <script src="{{ asset('public/js/app.js') }}" defer></script>

        <!-- Styles --> 
        <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">

        <meta name="csrf-token" content="{{ csrf_token() }}">

    </head>
    <body>
    <div class="container"> 
        
        <div class="row justify-content-md-center my-5"> 
            <div class="col-md-4 block-white px-5 py-4">
                <div class="px-2">
                    <img src="public/img/logo.png" class="img-fluid my-3" alt="logo">
                    <p class="auth-title text-left">Войти через социальные сети:</p>
                    <img src="public/img/social.png" class="img-fluid">
                    <div class="auth-or-block mt-4">
                        <p class="auth-or">или</p>
                    </div>
                    <form class="text-left">
                        <div class="form-group">
                            <label for="">Электронная почта:</label>
                            <input type="email" class="form-control">
                          </div>
                        <div class="form-group">
                            <label for="">Пароль:</label>
                            <input type="password" class="form-control">
                        </div>
                        <div class="form-group">
                             <label class="float-left">Запомнить меня</label>
                             <label class="checkbox float-right">
                                <input type="checkbox" name="remember">
                                <span class="checkmark"></span>
                            </label>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn-default">Войти</button>
                        </div>
                        <div class="form-group">
                            <a href="#" class="text-green font-medium">Напомнить пароль</a>
                        </div>
                        <div class="form-group text-center">
                            <h6 class="mb-0">Еще не зарегистировались?</h6>
                            <a href="#" class="text-green font-medium">Регистрация за 30 секунд</a>
                        </div>
                    </form>
                </div> 
            </div>
        </div>

    </div>
    </body>
</html>
