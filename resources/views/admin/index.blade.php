 @extends('layouts.admin')

@section('title')
  Админ панель
@endsection

@section('content')
<div class="col-md-8 px-5 py-1 mb-3">
	

			<h4 class="col-12 page-top-title p-0 my-3">Главная</h4>
 

	@include('common.alerts')

	<div class="col-12 block-white text-left p-4 mb-4">
		<table class="table table-hover fs-10">
		  <thead>
		    <tr>
		      <th scope="col">Пользователей</th>
		      <th scope="col">Групп</th>
		      <th scope="col">Миссии</th>
		      <th scope="col">Заметок</th>
		      <th scope="col">Продуктов</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>{{ $users }}</td>
		      <td>{{ $groups }}</td>
		      <td>{{ $missions }}</td>
		      <td>{{ $notes }}</td>
		      <td>{{ $products }}</td>
		    </tr>
		  </tbody>
		</table>
	</div>

</div>
@endsection  