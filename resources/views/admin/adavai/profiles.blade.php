 @extends('layouts.admin')

@section('title')
  Пользователи
@endsection

@section('content')
<div class="col-md-10 px-5 py-1 mb-3">
	
	<div class="col-12 my-3 px-0">
		<div class="row">
			<div class="col-12">
				<h4 class="page-top-title">Организаторы</h4>
				<span class="float-right">Сумма выигрыша на {{ date('Y') }} год - {{ $totalPrize }} руб</span>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

 
	@include('common.alerts')

	<nav class="nav">
	  <a class="nav-link" href="{{ route('admin.adavai.profiles.get') }}">Все</a>
	  <a class="nav-link" href="{{ route('admin.adavai.profiles.get', 'for-confirm') }}">Неподтвержденные</a>
	  <a class="nav-link" href="{{ route('admin.adavai.profiles.get', 'verified') }}">Подтвержденные</a>
	  <a class="nav-link" href="{{ route('admin.adavai.profiles.get', 'licensed') }}">Лицензированные</a>
	</nav>

	<div class="col-12 block-white text-left p-4 mb-4">
		<table class="table table-hover fs-10">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Пользователь</th>
		      <th scope="col">Полный адрес</th>
		      <th scope="col">Паспорт</th>
		      <th scope="col">Всего лайков</th>
		      <th scope="col"></th>
		      <th scope="col"></th>
		    </tr>
		  </thead>
		  <tbody>
		  	@if($profiles)
			  	@foreach($profiles as $item)
				    <tr>
				      <th scope="row">{{ $loop->index + 1 }}</th>
				      <td>
				      	<a href="{{ route('userProfile', $item->user->nickname) }}" target="_blank">
				      		{{ $item->user->nickname }}
				      	</a>
				      	<br>
				      	@if($item->user->awards)
		                    @foreach($item->user->awards as $award)
		                      <small class="badge badge-warning" style="font-weight: 100;"><i class="fas fa-medal"></i> {{ $award->name }}</small>
		                    @endforeach
		                @endif
				      </td>
				      <td>{{ $item->full_address }}</td>
				      <td>
				      	@if($item->user->adavaiProfile->passportExists())
							<a href="{{ $item->user->adavaiProfile->getPassport() }}" data-fancybox="">
								<img src="{{ $item->user->adavaiProfile->getPassport() }}" width="50" height="auto">
							</a>
						@endif
				      </td>
				      <td>
				      	{{ $item->notes_total_likes }}
				      </td>
				      <td>
				      	@if($item->for_confirmation)
				      		<form action="{{ route('admin.confirm.adavai.profile.post', $item->user->id) }}" method="post">
				      			@csrf
						      	<button type="submit" class="btn-default btn-default-sm confirm-required">Подтвердить</button>
				      		</form>
				      	@endif
				      </td>
				      <td>
					    <div class="dropdown note-options">
	                    <button class="btn btn-link px-1 py-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                      <i class="fas fa-ellipsis-h"></i>
	                    </button> 
		                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
		                      <a class="dropdown-item confirm-required" href="{{ route('admin.adavai.award.get', [$item->user->id, 'best_manager']) }}">Организатор года</a>
		                    </div>
	                    </div>
                	</td>
				    </tr>
			    @endforeach
		    @endif
		  </tbody>
		</table>
	</div> 
		<div id="pagination">
			
		  {{ $profiles->links() }}
		</div>





</div>
@endsection 