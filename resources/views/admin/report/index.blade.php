 @extends('layouts.admin')

@section('title')
  Жалобы
@endsection

@section('content')
<div class="col-md-8 px-5 py-1 mb-3">
	
	<div class="col-12 my-3 px-0">
		<div class="row">
			<div class="col-12">
				<h4 class="page-top-title text-center">Жалобы</h4>
			</div>
		</div>
	</div>


	@include('common.alerts')

	<div class="col-12 p-4 mb-4">
		<report-notes :user="{{ Auth::user()->id }}"
					  :admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}"
					  :report="1">
		</report-notes>
	</div>

 



</div>
@endsection 