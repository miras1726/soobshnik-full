@extends('layouts.admin')

@section('title')
  Список слов-паразитов
@endsection

@section('content')

          <div class="col-md-8 px-5 py-1 profile-block">
             
            <h4 class="page-top-title my-3">Список слов-паразитов</h4>
            
            <div class="col-12 block-white px-md-5 px-3 py-4 text-left">
                <div class="col-md-8 col-12">
                    <form action="{{ route('admin.badword.list.post') }}" class="table-form" method="post">
                        @csrf
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td class="table-form-label">Список:</td>
                            <td  class="form-group">
                                <textarea id="bad_words" type="text" class="form-control{{ $errors->has('bad_words') ? ' is-invalid' : '' }}" name="bad_words" placeholder="Пишите через запятую например first, second и т.д">{{ $list }}</textarea>

                                @if ($errors->has('bad_words'))
                                    <span class="invalid-feedback" role="alert">
                                        <span>{{ $errors->first('bad_words') }}</span>
                                    </span>
                                @endif
                            </td> 
                        </tr>
                        <tr>
                            <td class="table-form-label"></td>
                            <td>
                                <button type="submit" class="btn-default btn-default-sm w-auto mt-3">Отправить</button>
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>

          </div>
@endsection
