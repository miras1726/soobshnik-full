 @extends('layouts.admin')

@section('title')
  Группы
@endsection

@section('content')
<div class="col-md-8 px-5 py-1 mb-3">
	
	<div class="col-12 my-3 px-0">
		<div class="row">
			<div class="col-12">
				<h4 class="page-top-title">Группы</h4>
			</div>
		</div>
	</div>

 
	@include('common.alerts')

	<div class="col-12 block-white text-left p-4 mb-4">
		<table class="table table-hover fs-10">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Название</th>
		      <th scope="col">Автор</th>
		      <th scope="col">Добавлено</th>
		      <th scope="col"></th>
		      <th scope="col"></th>
		    </tr>
		  </thead>
		  <tbody>
		  	@if($groups)
			  	@foreach($groups as $item)
				    <tr>
				      <th scope="row">{{ $loop->index + 1 }}</th>
				      <td>{{ $item->title }}</td>
				      <td>
				      	@if($item->author)
				      	<a href="{{ route('admin.user.edit.get', [$item->author->id]) }}" target="_blank">
				      		{{ $item->author->nickname }}
				      	</a>
				      	@endif
				      </td>
				      <td>{{ $item->created_at }}</td>
				      <td>
				      	<a href="{{ route('group.single', [$item->id]) }}" class="color-gray" target="_blank">
				      		<i class="fas fa-eye"></i>
				      	</a>
				      </td>
				      <td>
				      	<a href="{{ route('admin.group.delete.get', [$item->id]) }}" class="color-gray confirm-required">
				      		<i class="fas fa-trash"></i>
				      	</a>
				      </td>
				    </tr>
			    @endforeach
		    @endif
		  </tbody>
		</table>
	</div>





</div>
@endsection 