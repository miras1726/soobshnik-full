@extends('layouts.admin')

@section('title')
  Добавить город
@endsection

@section('content')

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">Добавить</h4>
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-md-8 col-12">
			  		<form action="{{ route('admin.city.add.post') }}" class="table-form" method="post">
			  			@csrf
			  		<table class="table table-sm table-borderless">
						<tr>
							<td class="table-form-label">Название:</td>
							<td class="form-group">
						        <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required>

						        @if ($errors->has('title'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('title') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Страна:</td>
							<td  class="form-group">
								<select name="country_id" id="country_id" class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}">
									@if($countries->count() > 0)
										@foreach($countries as $item)
											<option value="{{ $item->id }}">{{ $item->title_original }}</option>
										@endforeach
									@endif
								</select>

								@if ($errors->has('country_id'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('country_id') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<button type="submit" class="btn-default btn-default-sm w-auto mt-3">Отправить</button>
							</td>
						</tr>
					</table>
			  		</form>
		  		</div>
		  	</div>

		  </div>
@endsection
