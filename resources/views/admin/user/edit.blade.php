@extends('layouts.admin')

@section('title')
  Редактировать пользователя
@endsection

@section('content')

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">Редактировать</h4>
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="row">
		  		<div class="col-md-8 col-12">

			  		<form action="{{ route('admin.user.edit.post', [$user->id]) }}" class="table-form" method="post">
			  			@csrf
			  		<table class="table table-sm table-borderless">
						<tr>
							<td class="table-form-label">Псевдоним:</td>
							<td class="form-group">
						        <input type="text" class="form-control" value="{{ $user->nickname }}" readonly="true">
							</td>
						</tr>
						<tr>
							<td class="table-form-label">ФИО:</td>
							<td class="form-group">
						        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}">

						        @if ($errors->has('name'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('name') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Email:</td>
							<td class="form-group">
						        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}">

						        @if ($errors->has('email'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('email') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Пол:</td>
							<td  class="form-group">
								<select name="gender" id="gender" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}">
									<option value="">Не выбран</option>
									<option value="male" {{ ($user->gender == 'male' ? 'selected' : '') }}>Мужской</option>
									<option value="female" {{ ($user->gender == 'female' ? 'selected' : '') }}>Женский</option>
								</select>

								@if ($errors->has('gender'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('gender') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Дата рождения:</td>
							<td  class="form-group">
								<input type="date" name="born" id="born" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}" value="{{ Date::parse($user->born)->format('Y-m-d') }}"> 
								@if ($errors->has('born'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('born') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Город:</td>
							<td  class="form-group">
						        <select class="form-control" name="city">
									@if($countries->count() > 0)
										@foreach($countries as $country)
										<optgroup label="{{ $country->title_original }}"></optgroup>
											@if($country->cities->count() > 0)
												@foreach($country->cities as $city)
													<option value="{{ $city->id }}" {{ ($user->city == $city->id) ? 'selected' : '' }}>{{ $city->title_original }}</option>
												@endforeach
											@endif
										@endforeach
									@endif
								</select>

						        @if ($errors->has('city'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('city') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Skype:</td>
							<td  class="form-group">
								<input type="text" name="skype" id="skype" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}" value="{{ $user->skype }}"> 
								@if ($errors->has('skype'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('skype') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Информация:</td>
							<td  class="form-group">
						        <textarea id="info" type="text" class="form-control{{ $errors->has('info') ? ' is-invalid' : '' }}" name="info">{{ $user->info }}</textarea>

						        @if ($errors->has('info'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('info') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label">Статус:</td>
							<td  class="form-group">
						        <textarea id="quote" type="text" class="form-control{{ $errors->has('quote') ? ' is-invalid' : '' }}" name="quote">{{ $user->quote }}</textarea>

						        @if ($errors->has('quote'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('quote') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<button type="submit" class="btn-default btn-default-sm w-auto mt-3">Отправить</button>
							</td>
						</tr>
					</table>
			  		</form>
		  		</div>
		  		<div class="col-4">		
	  				<img src="{{ Croppa::url('public/img/users/'.$user->avatar(), 500, 500) }}" class="img-fluid" alt="profile-img">
	  				<br>
	  				<div class="mt-3">
	  					<a href="{{ route('private-chat', [$user->id]) }}" class="btn-default btn-default-sm" target="_blank">Написать пользователю</a>
	  				</div>
	  			</div>
	  			</div>
		  	</div>

		  </div>
@endsection
