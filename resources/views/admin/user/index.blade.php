 @extends('layouts.admin')

@section('title')
  Пользователи
@endsection

@section('content')
<div class="col-md-8 px-5 py-1 mb-3">
	
	<div class="col-12 my-3 px-0">
		<div class="row">
			<div class="col-12">
				<h4 class="page-top-title">Пользователи</h4>
			</div>
		</div>
	</div>

 
	@include('common.alerts')

	<div class="col-12 block-white text-left p-4 mb-4">
		<table class="table table-hover fs-10">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Псевдоним</th>
		      <th scope="col">Email</th>
		      <th scope="col">Добавлено</th>
		      <th scope="col"></th>
		      <th scope="col"></th>
		      <th scope="col"></th>
		    </tr>
		  </thead>
		  <tbody>
		  	@if($users)
			  	@foreach($users as $item)
				    <tr>
				      <th scope="row">{{ $loop->index + 1 }}</th>
				      <td>
				      	{{ $item->nickname }}
				      	<br>
				      	@if($item->awards)
		                    @foreach($item->awards as $award)
		                      <small class="badge badge-warning" style="font-weight: 100;"><i class="fas fa-medal"></i> {{ $award->name }}</small>
		                    @endforeach
		                @endif
				      </td>
				      <td>{{ $item->email }}</td>
				      <td>{{ $item->created_at }}</td>
				      <td><a href="{{ route('admin.user.edit.get', [$item->id]) }}" class="color-gray"><i class="fas fa-edit"></i></a></td>
				      <td>
				      	<a href="{{ route('admin.user.delete.get', [$item->id]) }}" class="color-gray confirm-required">
				      		<i class="fas fa-trash"></i>
				      	</a>
				      </td>
				      <td>
					    <div class="dropdown note-options">
	                    <button class="btn btn-link px-1 py-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                      <i class="fas fa-ellipsis-h"></i>
	                    </button> 
		                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
		                      <a class="dropdown-item confirm-required" href="{{ route('admin.adavai.award.get', [$item->id, 'lucky_manager']) }}">Счастливчик года</a>
		                    </div>
	                    </div>
                	</td>
				    </tr>
			    @endforeach
		    @endif
		  </tbody>
		</table>
	</div>
		<div id="pagination">
			
		  {{ $users->links() }}
		</div>





</div>
@endsection 