@extends('layouts.admin')

@section('title')
  Добавить продукт
@endsection

@section('content')

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">Добавить</h4> 
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-md-8 col-12">
			  		<form action="{{ route('admin.product.add.post') }}" class="table-form" method="post" enctype="multipart/form-data">
			  			@csrf
			  		<table class="table table-sm table-borderless">
						<tr>
							<td class="table-form-label">Название:</td>
							<td class="form-group">
						        <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required>

						        @if ($errors->has('title'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('title') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Цена:</td>
							<td class="form-group">
						        <input id="price" type="number" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" value="{{ old('price') }}" required>

						        @if ($errors->has('price'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('price') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Цена (Звезды):</td>
							<td class="form-group">
						        <input id="price_star" type="number" class="form-control{{ $errors->has('price_star') ? ' is-invalid' : '' }}" name="price_star" value="{{ old('price_star') }}" required>

						        @if ($errors->has('price_star'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('price_star') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Изображение:</td>
							<td class="form-group">
						        <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image">

						        @if ($errors->has('image'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('image') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<button type="submit" class="btn-default btn-default-sm w-auto mt-3">Отправить</button>
							</td>
						</tr>
					</table>
			  		</form>
		  		</div>
		  	</div>

		  </div>
@endsection
