 @extends('layouts.admin')

@section('title')
  Продукты
@endsection

@section('content')
<div class="col-md-8 px-5 py-1 mb-3">
	
	<div class="col-12 my-3 px-0">
		<div class="row">
			<div class="col-6">
				<h4 class="page-top-title">Продукты</h4>
			</div>
			<div class="col-6 text-right">
				<a href="{{ route('admin.product.add.get') }}" class="float-right mt-2">Добавить</a>
			</div>
		</div> 
	</div>


	@include('common.alerts')

	<div class="col-12 block-white text-left p-4 mb-4">
		<table class="table table-hover fs-10">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Название</th>
		      <th scope="col">Цена</th>
		      <th scope="col">Цена (Звезды)</th>
		      <th scope="col">Добавлено</th>
		      <th scope="col"></th>
		      <th scope="col"></th>
		    </tr>
		  </thead>
		  <tbody>
		  	@if($products)
			  	@foreach($products as $item)
				    <tr>
				      <th scope="row">{{ $loop->index + 1 }}</th>
				      <td>{{ $item->title }}</td>
				      <td>{{ $item->price }}</td>
				      <td>{{ $item->price_star }}</td>
				      <td>{{ $item->created_at }}</td>
				      <td><a href="{{ route('admin.product.edit.get', [$item->id]) }}" class="color-gray"><i class="fas fa-edit"></i></a></td>
				      <td>
				      	<a href="{{ route('admin.product.delete.get', [$item->id]) }}" class="color-gray confirm-required">
				      		<i class="fas fa-trash"></i>
				      	</a>
				      </td>
				    </tr>
			    @endforeach
		    @endif
		  </tbody>
		</table>
	</div>





</div>
@endsection 