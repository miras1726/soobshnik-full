 @extends('layouts.admin')

@section('title')
  Заказы
@endsection

@section('content')
<div class="col-md-8 px-5 py-1 mb-3">
	
	<div class="col-12 my-3 px-0">
		<div class="row">
			<div class="col-6">
				<h4 class="page-top-title">Заказы</h4>
			</div>
		</div> 
	</div>


	@include('common.alerts')

	<div class="col-12 block-white text-left p-4 mb-4">
		<table class="table table-hover fs-10">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Продукт</th>
		      <th scope="col">Пользователь</th>
		      <th scope="col">Дата</th>
		      <th scope="col"></th>
		    </tr>
		  </thead>
		  <tbody>
		  	@if($orders->count() > 0)
			  	@foreach($orders as $order)
				  	@if($order->product)
					    <tr>
					      <th scope="row">{{ $loop->index + 1 }}</th>
					      <td>
					      	@if($order->product)
					      		<a href="{{ route('admin.product.edit.get', [$order->product->id]) }}">
					      			{{ $order->product->title }}
					      		</a>
					      	@endif
					      </td>
					      <td>
					      	@if($order->user)
					      		<a href="{{ route('admin.user.edit.get', [$order->user->id]) }}">
					      			{{ $order->user->nickname }}
					      		</a>
					      	@endif

					      </td>
					      <td>{{ $order->created_at }}</td>
					      <td>
					      	<a href="{{ route('admin.order.delete', [$order->id]) }}" class="color-gray confirm-required">
					      		<i class="fas fa-trash"></i>
					      	</a>
					      </td>
					    </tr>
			    	@endif
			    @endforeach
		    @endif
		  </tbody>
		</table>
	</div>





</div>
@endsection 