   @extends('layouts.admin')

@section('title')
  Вопросы
@endsection

@section('content')
<div class="col-md-8 px-5 py-1 mb-3">
	
	<div class="col-12 my-3 px-0">
		<div class="row">
			<div class="col-12">
				<h4 class="page-top-title">Вопросы</h4>
			</div>
		</div> 
	</div>


	@include('common.alerts')

	<div class="col-12 block-white text-left p-4 mb-4">
  		@if($questions->count() > 0)
			<div class="list-group">
	  			@foreach($questions as $question)
				  <div class="list-group-item list-group-item-action">
				    <div class="d-flex w-100 justify-content-between">
				      <h5 class="mb-1">
				      		<i class="fa fa-circle {{ ($question->answer) ? 'color-green' : 'color-red'}} fs-8"></i>
				      	{{ $question->subject }}
				      </h5>
				      <small>{{ $question->date() }}</small>
				    </div>
				    <p class="mb-1">{{ $question->text }}</p>
				    @if($question->answer)
				    	<hr>
				    	<small class="mb-1"><p class="mb-1 color-gray"><i class="fa fa-comment"></i> {{ $question->answer }}</p></small>
				    @endif
				    <small>
				    	<a href="{{ route('admin.answer.to.question.get', [$question->id]) }}" class="color-green">
				    		@if($question->answer)
				    			Изменить
				    		@else
				    			Ответить
				    		@endif	
				    	</a>
				    </small>
				    <br>
				    <small><a href="{{ route('admin.question.delete', [$question->id]) }}" class="confirm-required color-red">Удалить</a></small>
				    <a href="{{ route('admin.user.edit.get', [$question->user->id]) }}" class="float-right" target="_blank"><i class="fa fa-user"></i> {{ $question->user->nickname }}</a>
				  </div>
	  			@endforeach
			</div>
  		@endif

	</div>

		<div id="pagination">
		  {{ $questions->links() }}
		</div>






</div>
@endsection 