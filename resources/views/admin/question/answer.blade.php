   @extends('layouts.admin')

@section('title')
  Ответить на вопрос
@endsection

@section('content')
<div class="col-md-8 px-5 py-1 mb-3">
	
	<div class="col-12 my-3 px-0">
		<div class="row">
			<div class="col-12">
				<h4 class="page-top-title">Ответить на вопрос</h4>
			</div>
		</div> 
	</div>


	@include('common.alerts')

	<div class="col-12 block-white text-left p-4 mb-4">
<form action="{{ route('admin.answer.to.question.post') }}" class="table-form" method="post">
			  			@csrf
			  		<table class="table table-sm table-borderless">
						<tr>
							<td  class="form-group">
						       <h5>{{ $question->subject }}</h5>
						       <p>{{ $question->text }}</p>
						       <a href="{{ route('admin.user.edit.get', [$question->user->id]) }}" target="_blank">
						       	<i class="fa fa-user"></i> {{ $question->user->nickname }}</a><br>
						       <small><span class="color-gray">{{ $question->date() }}</span></small>
							</td>
						</tr>
						<tr>
							<td  class="form-group">
						        <textarea id="text" class="form-control{{ $errors->has('text') ? ' is-invalid' : '' }}" name="text" placeholder="Ваш ответ на вопрос" required>{{ $question->answer }}</textarea>

						        @if ($errors->has('text'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('text') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td>
								<input type="hidden" name="id" value="{{ $question->id }}">
								<button type="submit" class="btn-default btn-default-sm w-auto float-right">Отправить</button>
							</td>
						</tr>
					</table> 
			  		</form>

	</div> 
</div>
@endsection 