@extends('layouts.admin')

@section('title')
  Редактировать миссию
@endsection

@section('content')

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	 
		  	<h4 class="page-top-title my-3">Редактировать</h4>
		  	 
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="row">
		  		<div class="col-md-8 col-12">
			  		<form action="{{ route('admin.mission.edit.post', [$mission->id]) }}" class="table-form" method="post" enctype="multipart/form-data">
			  			@csrf
			  		<table class="table table-sm table-borderless">
						<tr>
							<td class="table-form-label">Название:</td>
							<td class="form-group">
						        <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ $mission->title_original }}" required>

						        @if ($errors->has('title'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('title') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
						<td class="table-form-label">Категория:</td>
							<td  class="form-group">
								<select name="category_id" id="category_id" class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}">
									@if($categories->count() > 0)
										@foreach($categories as $item)
											<option value="{{ $item->id }}" {{ ($item->id == $mission->category_id) ? 'selected' : '' }}>{{ $item->title_original }}</option>
										@endforeach
									@endif
								</select>

								@if ($errors->has('category_id'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('category_id') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Мотивашка:</td>
							<td  class="form-group">
						        <textarea id="motivation" type="text" class="form-control{{ $errors->has('motivation') ? ' is-invalid' : '' }}" name="motivation">{{ $mission->motivation_original }}</textarea>

						        @if ($errors->has('motivation'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('motivation') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label">Изображение:</td>
							<td  class="form-group">
						        <input type="file" name="image" id="image" class="h-auto form-control{{ $errors->has('image') ? ' is-invalid' : '' }}">

						        @if ($errors->has('image'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('image') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<button type="submit" class="btn-default btn-default-sm w-auto mt-3">Отправить</button>
							</td>
						</tr>
					</table>
			  		</form>
		  		</div>
		  		<div class="col-4">		
	  				<img src="{{ Croppa::url('public/img/missions/'.$mission->avatar(), 500, 500) }}" class="img-fluid" alt="profile-img">
	  			</div>
	  			</div>
		  	</div>


			<div class="col-12 my-3 px-0">
				<div class="row">
					<div class="col-6">
						<h4 class="page-top-title">Полезные ссылки</h4>
					</div>
					<div class="col-6 text-right">
						<a href="{{ route('admin.mission.link.add.get', [$mission->id]) }}" class="float-right mt-2">Добавить</a>
					</div>
				</div>
			</div>

			<div class="col-12 block-white text-left p-4 mb-4">
				<table class="table table-hover fs-10">
				  <thead>
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">Название</th>
				      <th scope="col">Ссылка</th>
				      <th scope="col"></th>
				    </tr>
				  </thead>
				  <tbody>
				  	@if($mission->links->count() > 0)
					  	@foreach($mission->links as $item)
						    <tr>
						      <th scope="row">{{ $loop->index + 1 }}</th>
						      <td>{{ $item->title_original }}</td>
						      <td>{{ $item->link }}</td>
						      <td>
						      	<a href="{{ route('admin.mission.link.delete.get', [$item->id]) }}" class="color-gray confirm-required">
						      		<i class="fas fa-trash"></i>
						      	</a>
						      </td>
						    </tr>
					    @endforeach
				    @endif
				  </tbody>
				</table>
			</div>

		  </div>
@endsection
