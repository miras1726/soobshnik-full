@extends('layouts.mail')

@section('content')
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td align="center" class="padding-copy" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33; padding-top: 0px;">{{ __('mail.greeting') }}, {{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td align="left" class="padding-copy textlightStyle" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33;">

                        <p style="margin:0px;">{{ __('mail.you.reveiced.mail') }}
                        <p>{{ __('mail.go.link') }}: {{ url('password/reset/').'/'.$token }}</p> 

                        {{ __('mail.ignor') }}
                        </p>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <tr>
            <td>
            <!-- BULLETPROOF BUTTON -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                    <tbody>
                        <tr>
                            <td align="center" style="padding: 25px 0 0 0;" class="padding-copy">
                                <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                                    <tbody>
                                        <tr>
                                        <td align="center"><a target="_new" class="mobile-button" style="display: inline-block; font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #7C9F58; padding-top: 15px; padding-bottom: 15px; padding-left: 25px; padding-right: 25px; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-bottom: 3px solid #566f3d;" href="{{ url('password/reset/').'/'.$token }}">{{ __('mail.reset.btn') }} →</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
@endsection