@extends('layouts.mail')

@section('content')
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td align="center" class="padding-copy" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33; padding-top: 0px;">
                        	{{ __('mail.report.for.days', ['frequency' => $frequency]) }} {{ $diary }}
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="padding-copy textlightStyle" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33;">

                        <p style="margin:0px;">

							{{ __('mail.personal.ref.link') }} - {{ $refLink }}<br> 
							
<div class="container">
  	<label>{{ __('mail.mission.progress') }}</label>
  	<div class="progress-outer">
    	<div class="progress-inner" style="width:{{ $progress }}%; left:{{ $progress }}%;"></div>
  	</div>
</div>
<div class="container">
  	<label>{{ __('mail.goal.reaching') }}</label>
  	<div class="progress-outer">
    	<div class="progress-inner" style="width:{{ $goalReached }}%; left:{{ $goalReached }}%;"></div>
  	</div>
</div>
<div class="container">
  	<label>{{ __('mail.level') }}</label>
  	<div class="progress-outer">
    	<div class="progress-inner" style="width:{{ $isEasy }}%; left:{{ $isEasy }}%;"></div>
  	</div>
</div>
<div class="container">
  	<label>{{ __('mail.motivation.complice') }}</label>
  	<div class="progress-outer">
    	<div class="progress-inner" style="width:{{ $motivation }}%; left:{{ $motivation }}%;"></div>
  	</div>
</div>  
                        </p>


                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
@endsection