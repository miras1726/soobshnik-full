 @extends('layouts.main')

@section('title')
  {{ __('diary.diary') }}
@endsection

@section('content')

<div class="col-md-8 px-5 py-1 mb-3">
	
	<h4 class="col-12 page-top-title p-0 my-3">{{ __('diary.diary') }} > {{ $diary->mission->title }}</h4>

	@include('common.alerts')
 
	<div class="col-12 block-white text-left p-4 mb-4">
		<div class="row">
			<div class="col-2">
	            <img src="{{ Croppa::url('public/img/missions/'.$diary->mission->avatar(), 500, 500) }}" class="img-fluid rounded-circle" alt="profile-img">
			</div>
			<div class="col-10 mb-md-0 mb-3">
				<p class="h6 mb-3">{{ $diary->goal }}</p>
				<p class="mb-0">
					<span class="color-gray">{{ __('diary.week.goal') }}:</span> 
					 {{ $diary->goal_for_week }}
				</p>
				
				
				
				<p class="mb-0">
					<span class="color-gray">{{ __('diary.daily.goal') }}:</span>
					 {{ $diary->goal_daily }}
				</p>
				
			</div> 
			<div class="col-md-12 text-right">
				<p class="color-gray mb-0 float-left mt-2 ml-3">
					@if($diary->finishingAt())
						{{ __('diary.finishing') }} {{ $diary->finishingAt() }}
					@else
						{{ __('diary.finished') }}
					@endif
				</p>
				<a href="{{ route('diary.delete.get', [$diary->id]) }}" class="d-inline-block pt-1 color-gray confirm-required">{{ __('diary.delete') }}</a>
				<a href="{{ route('diary.write.get', [$diary->id]) }}" class="btn-default btn-default-sm float-right w-auto ml-3">{{ __('diary.refresh') }}</a>
			</div> 


		</div>
	</div>

	<div class="col-12 block-white text-left p-4 mb-4">
		<div class="row">
			<div class="col-6"> 
				<label>{{ __('diary.progress.title') }}</label>
				<div class="progress-outer">
					<div class="progress-inner" style="width:{{ $diary->calculateProgress()  }}%; left:{{ $diary->calculateProgress()  }}%;"></div>
				</div>
			</div>
			<div class="col-6">
				<label>{{ __('diary.goal.reached.title') }}</label>
				<div class="progress-outer">
					<div class="progress-inner" style="width:{{ $diary->goalReachedProgress(true)  }}%; left:{{ $diary->goalReachedProgress(true)  }}%;"></div>
				</div>
			</div>
			<div class="col-6 mt-5">
				<label>{{ __('diary.is.easy.title') }}</label>
				<div class="progress-outer">
					<div class="progress-inner" style="width:{{ $diary->isEasyProgress(true)  }}%; left:{{ $diary->isEasyProgress(true)  }}%;"></div>
				</div>
			</div>
			<div class="col-6 mt-5">
				<label>{{ __('diary.motivation.enough.title') }}</label>
				<div class="progress-outer">
					<div class="progress-inner" style="width:{{ $diary->motivationProgress(true)  }}%; left:{{ $diary->motivationProgress(true)  }}%;"></div>
				</div>
			</div>
		</div>
	</div>

	@if($diary->notes()->count() > 0)
		@foreach($diary->notes() as $note)
			<div class="col-md-12">
			  	<div class="row">
			  		<div class="col-md-12 my-2 block-white text-left">
			  			<div class="row p-4">
				  			@if($note->params->goal_reached)
				  				<i class="fas fa-check-circle fa-3x color-green"></i>
				  			@else
				  				<i class="fas fa-times-circle fa-3x color-red"></i>
				  			@endif
				  			<div class="px-2 note-info">
				  				<a href="#" class="diary-date mb-1 mt-2">{{ $note->posted_at }}</a>
				  			</div>
			  				<div class="dropdown note-options">
			                    <button class="btn btn-link px-1 py-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			                      <i class="fas fa-ellipsis-h"></i>
			                    </button>
			                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			                      @if($note->params->canEdit())
			                      	<a class="dropdown-item" href="{{ route('diary.note.edit.post', [$note->id]) }}">{{ __('diary.edit') }}</a>
			                      @endif
			                      <a class="dropdown-item confirm-required" href="{{ route('note.delete', [$note->id, true]) }}">{{ __('diary.delete') }}</a>
			                    </div>
			                    </div>
				  			<div class="col-md-12 px-0 my-3">
				  				<div class="row px-2">
				  					<div class="col-12 px-2 mb-1">
				  						{{ $note->content }}
				  					</div>
				  				</div>
				  			</div>
				  			<div class="col-md-12 mt-2">
				  				<div class="row note-actions d-flex">
				  					@if($note->params->isReposted())
					  					<a href="{{ route('diary.note.unshare', [$note->id]) }}" class="mr-3 confirm-required">
					  						<i class="fas fa-undo"></i>
					  						<span class="note-action-badge">
					  							{{ __('diary.remove') }}
					  						</span>
					  					</a>
				  					@else
				  						<a href="{{ route('diary.note.share', [$note->id]) }}" class="mr-3 confirm-required">
					  						<i class="fas fa-bullhorn"></i>
					  						<span class="note-action-badge">
					  							{{ __('diary.share') }}
					  						</span>
					  					</a>
				  					@endif
				  				</div>
				  			</div>
			  			</div>
			  		</div>
			  	</div>
		  	</div>
	  	@endforeach
  	@endif


</div>

	<div class="col-2 block-white-lite p-0 d-md-block d-none">
		<div class="sticky-top side-menu p-4" dir="rtl">
			<div class="row" dir="ltr"> 
		  		@include('site.diary._side')
			</div>
		</div>
	</div>

@endsection

