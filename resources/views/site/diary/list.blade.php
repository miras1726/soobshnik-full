 @extends('layouts.main')

@section('title')
  {{ __('diary.diaries') }}
@endsection

@section('content')
 
<div class="col-md-9 px-5 py-1 mb-3">
	

	<div class="col-12 my-3">
		<div class="row">
			<div class="col-6">
				<h4 class="page-top-title">{{ __('diary.my.diaries') }}</h4>
			</div>
			<div class="col-6 text-right">
				<a href="{{ route('diary.add.get') }}" class="float-right mt-2">{{ __('diary.create') }}</a>
			</div>
		</div>
	</div>
	
	<div class="col-12">
		<div class="row">
 

			@if($diaries->count() > 0)
				@foreach($diaries as $diary)
					@if($diary->mission)
						<div class="col-md-3 mb-4">
							<div class="col-12 block-white user-list-item text-center py-3 px-4">
							
								<img src="{{ Croppa::url('/public/img/missions/'.$diary->mission->avatar(), 120, 120) }}" class="rounded-circle">
								<p class="mb-0 mt-2">
									<a href="{{ route('diary.single', [$diary->id]) }}" class="link-black fixed-h-max">
										{{ $diary->mission->title }}
									</a>
								</p>
								<p class="fs-9 mt-2 mb-1 color-green">
									{{ $diary->notes()->count() }} {{ __('diary.notes') }}
								</p>
								<p class="fs-9 mt-2 mb-2 color-gray">
									@if($diary->userMission()->status == 'refuse')
										{{ __('diary.refused') }} 
									@else
										@if($diary->finishingAt()) 
											{{ __('diary.finishing') }} {{ $diary->finishingAt() }}
										@else
											{{ __('diary.finished') }}
										@endif
									@endif
								</p>
								
								<a href="{{ route('diary.single', [$diary->id]) }}" class="btn-default btn-default-sm">{{ __('diary.open') }}</a>
							</div>
						</div>
					@endif
				@endforeach
			@endif


		</div>
	</div>
</div>
@endsection

