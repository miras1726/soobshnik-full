@extends('layouts.main')

@section('title')
  {{ __('diary.create.new.diary') }}
@endsection
  
@section('content')

		  <div class="col-md-8 px-5 py-1 profile-block">
		  		
		  	  

		  	<h4 class="page-top-title my-3">{{ __('diary.new.diary') }}</h4>
		  	
		  	 @include('common.alerts')
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-md-8 col-12">
			  		<form action="{{ route('diary.add.post') }}" class="table-form" method="post" enctype="multipart/form-data">
			  			@csrf
			  		<table class="table table-sm table-borderless">
						<tr> 
							<td class="table-form-label">{{ __('diary.mission') }}:</td>
							<td class="form-group">
						        <select name="mission_id" id="mission_id" class="form-control{{ $errors->has('mission_id') ? ' is-invalid' : '' }}" required>
						        	@if($categories->count() > 0)
										@foreach($categories as $cat)
											<option value="{{ $cat->mission->id }}">{{ $cat->mission->title }}</option>
										@endforeach
									@endif 
								</select>

						        @if ($errors->has('mission_id'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('mission_id') }}</span>
						            </span>
						        @endif 
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('diary.date.ending') }}:</td>
							<td  class="form-group">
						        <input type="date" id="should_finished_at" type="text" class="form-control{{ $errors->has('should_finished_at') ? ' is-invalid' : '' }}" name="should_finished_at" value="{{ Date::parse()->format('Y-m-d') }}" required>

						        @if ($errors->has('should_finished_at'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('should_finished_at') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label">{{ __('diary.goal') }}:</td>
							<td  class="form-group">
						        <textarea id="goal" type="text" class="form-control{{ $errors->has('goal') ? ' is-invalid' : '' }}" name="goal" required></textarea>

						        @if ($errors->has('goal'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('goal') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label">{{ __('diary.week.goal') }}:</td>
							<td  class="form-group">
						        <textarea id="goal_for_week" type="text" class="form-control{{ $errors->has('goal_for_week') ? ' is-invalid' : '' }}" name="goal_for_week" required></textarea>

						        @if ($errors->has('goal_for_week'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('goal_for_week') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label">{{ __('diary.daily.goal') }}:</td>
							<td  class="form-group">
						        <textarea id="goal_daily" type="text" class="form-control{{ $errors->has('goal_daily') ? ' is-invalid' : '' }}" name="goal_daily" required></textarea>

						        @if ($errors->has('goal_daily'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('goal_daily') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td  class="form-group">
								<label>{!! __('diary.how.often.fill') !!}:</label>
						        <select name="write_period" id="write_period" class="form-control{{ $errors->has('write_period') ? ' is-invalid' : '' }}" required>
									<option value="1">{{ __('diary.once.day') }}</option>
									<option value="7">{{ __('diary.once.week') }}</option>
								</select>

						        @if ($errors->has('write_period'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('write_period') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td  class="form-group">
								<label>{!! __('diary.how.often.receive') !!}:</label>
						       	<select name="email_frequency" id="email_frequency" class="form-control{{ $errors->has('email_frequency') ? ' is-invalid' : '' }}" required>
									<option value="7">{{ __('diary.once.week') }}</option>
									<option value="30">{{ __('diary.once.month') }}</option>
								</select>

						        @if ($errors->has('email_frequency'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('email_frequency') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<button type="submit" class="btn-default btn-default-sm w-auto">{{ __('diary.send') }}</button>
							</td>
						</tr>
					</table>
			  		</form>
		  		</div>
		  	</div>

		  </div>
@endsection

@section('js')
{!! JsValidator::formRequest('App\Http\Requests\SettingsRequest') !!}
@endsection