@extends('layouts.main')

@section('title')
	{{ __('diary.edit.note') }}
@endsection

@section('content')

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">{{ __('diary.edit.note') }}</h4>
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-12">
			  		<form action="{{ route('diary.note.edit.post', [$note->id]) }}" class="table-form" method="post">
			  			@csrf
			  			<div class="row">

			  				<div class="col-12">
				  				<div class="row form-group">
							  		
					  				<div class="col-12">
							  			<label for="goal_reached" id="goal_reached">{{ __('diary.goal.reached') }}</label>
									</div>
					  				<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input" type="radio" name="goal_reached" id="goal_reached" value="1" required {{ ($note->params->goal_reached == 1) ? 'checked' : '' }}>
										  <label class="form-check-label" for="goal_reached">
										    Да
										  </label>
										</div>
									</div>
									<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input" type="radio" name="goal_reached" value="0"  {{ ($note->params->goal_reached == 0) ? 'checked' : '' }}>
										  <label class="form-check-label" for="goal_reached">
										    Нет
										  </label>
										</div>
									</div>
									@if ($errors->has('goal_reached'))
							            <span class="invalid-feedback" role="alert">
							                <span>{{ $errors->first('goal_reached') }}</span>
							            </span>
							        @endif
				  				</div>
			  				</div>

			  				<div class="col-12">

				  				<div class="row form-group">
							  		
					  				<div class="col-12">
							  			<label for="is_easy" id="is_easy">{{ __('diary.is.easy') }}</label>
									</div>
					  				<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input{{ $errors->has('is_easy') ? ' is-invalid' : '' }}" type="radio" name="is_easy" id="is_easy" value="1" required {{ ($note->params->is_easy == 1) ? 'checked' : '' }}>
										  <label class="form-check-label" for="is_easy">
										    Да
										  </label>
										</div>
									</div>
									<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input{{ $errors->has('is_easy') ? ' is-invalid' : '' }}" type="radio" name="is_easy" value="0" {{ ($note->params->is_easy == 0) ? 'checked' : '' }}>
										  <label class="form-check-label" for="is_easy">
										    Нет
										  </label>
										</div>
									</div>
									@if ($errors->has('is_easy'))
							            <span class="invalid-feedback" role="alert">
							                <span>{{ $errors->first('is_easy') }}</span>
							            </span>
							        @endif
				  				</div>
			  				</div>

			  				<div class="col-12">

				  				<div class="row form-group">
							  		
					  				<div class="col-12">
							  			<label for="is_motivation_enough" id="is_motivation_enough">{{ __('diary.motivation.enough') }}</label>
									</div>
					  				<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input{{ $errors->has('is_motivation_enough') ? ' is-invalid' : '' }}" type="radio" name="is_motivation_enough" id="is_motivation_enough" value="1" required {{ ($note->params->is_motivation_enough == 1) ? 'checked' : '' }}>
										  <label class="form-check-label" for="is_motivation_enough">
										    Да
										  </label>
										</div>
									</div>
									<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input{{ $errors->has('is_motivation_enough') ? ' is-invalid' : '' }}" type="radio" name="is_motivation_enough" value="0" {{ ($note->params->is_motivation_enough == 0) ? 'checked' : '' }}>
										  <label class="form-check-label" for="is_motivation_enough">
										    Нет
										  </label>
										</div>
									</div>
									@if ($errors->has('is_motivation_enough'))
							            <span class="invalid-feedback" role="alert">
							                <span>{{ $errors->first('is_motivation_enough') }}</span>
							            </span>
							        @endif
				  				</div>
			  				</div>

			  				<div class="col-12">
				  				<div class="form-group">
				  					<label for="content">{{ __('diary.review') }}</label>
							        <textarea type="text" class="emoji-enabled form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" name="content">{{ $note->content }}</textarea>

							        @if ($errors->has('content'))
							            <span class="invalid-feedback" role="alert">
							                <span>{{ $errors->first('content') }}</span>
							            </span>
							        @endif			  					
				  				</div>
			  				</div>

			  				<div class="col-12">
				  				<div class="form-group">
			  						<button type="submit" class="btn-default btn-default-sm w-auto float-right">{{ __('diary.send') }}</button>
			  					</div>
			  				</div>

			  			</div>

			  		</form>
		  		</div>
		  	</div>

		  </div>
@endsection

