<div class="col-12">
	<h6>{{ __('diary.active.diaries') }}</h6>
	<hr>
	@if($active->count() > 0)
		@foreach($active as $activeDiary)
			@if($activeDiary->mission)
				<img src="{{ Croppa::url('/public/img/missions/'.$activeDiary->mission->avatar(), 120, 120) }}" class="rounded-circle">
				<a href="{{ route('diary.single', [$activeDiary->id]) }}" class="h6 d-block mt-2">{{ $activeDiary->mission->title }}</a>
				<p class="fs-9 mt-2 mb-1 color-green">
					{{ $activeDiary->notes()->count() }} {{ __('diary.notes') }}
				</p>
			@endif
		@endforeach
	@endif
</div>

<div class="col-12">
	<h6>{{ __('diary.filled.diaries') }}</h6>
	<hr>
	@if($finished->count() > 0)
		@foreach($finished as $finishedDiary)
			@if($finishedDiary->mission)
			<div class="col-12 text-center mb-4">
				<div class="position-relative">	
					<img src="{{ Croppa::url('/public/img/missions/'.$finishedDiary->mission->avatar(), 120, 120) }}" class="rounded-circle">
					<span class="color-green block-outer-hint h4"><i class="fas fa-check-circle"></i></span>
				</div>
				<a href="{{ route('diary.single', [$finishedDiary->id]) }}" class="h6 d-block mt-2">{{ $finishedDiary->mission->title }}</a>
				<p class="fs-9 mt-2 mb-1 color-gray">
					{{ $finishedDiary->notes()->count() }} {{ __('diary.notes') }}
				</p>
			</div>
			@endif
		@endforeach
	@endif
</div>