@extends('layouts.main')

@section('title')
  @lang('adavai.bank')
@endsection

@section('content') 
	<div class="col-md-8 px-5 py-1 profile-block">
		<h4 class="page-top-title my-3">@lang('user.balance.replenish')</h4>
		<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
			<div class="alert alert-{{ $status == 'success' ? 'success' : 'danger' }}" role="alert">
			  {{ $message }}
			</div>
			<a href="{{ route('adavai.user.bank') }}" class="btn btn-default btn-default-sm w-25">@lang('adavai.bank')</a>
		</div>
	</div>

@endsection
