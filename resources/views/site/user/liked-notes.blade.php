   @extends('layouts.main')

@section('title')
  {{ __('user.liked.notes') }}
@endsection

@section('content')

<div class="col-md-8 px-5 py-1 mb-3">
	
	<div class="col-12 px-0 my-3">
		<h4 class="page-top-title">{{ __('user.liked.notes') }}</h4>
	</div>
	
  	<liked-notes :user="{{ Auth::user()->id }}" :type="'user'"></liked-notes>

</div>

@endsection

