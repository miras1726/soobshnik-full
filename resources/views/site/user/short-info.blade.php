{{ __('user.on.site.days', ['days' => $days]) }}<br>
<div class="short-info-rating" data-rating="{{ $rating }}"></div>
{{ __('user.completed.missions') }}: {{ $missionsCount }} 
<script type="text/javascript">
  $(".short-info-rating").starRating({
  	readOnly: true,
    starSize: 20,
    useGradient: false,
    activeColor: '#F7AA28',
    ratedColor: '#F7AA28',
    starShape: 'rounded'
  });
</script> 