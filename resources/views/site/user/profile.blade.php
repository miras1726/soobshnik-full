@extends('layouts.main')

@section('title')
  {{ __('user.profile.user') }}
@endsection 

@section('content')
      <div class="col-lg-8 col-md-7 px-lg-5 px-md-3 px-3 py-1 profile-block">
        <h4 class="page-top-title my-3">{{ __('user.profile') }}</h4>
        <div class="col-12 block-white p-4 text-left">
          <div class="row">
            <div class="col-md-4"> 
              <div class="profile-img text-center">
                
                @if($user->isAvatarExists())  
                <a href="/public/img/users/{{ $user->avatar() }}" data-fancybox="">
                  <img src="{{ Croppa::url('public/img/users/'.$user->avatar(), 500, 500) }}" class="img-fluid" alt="profile-img">
                </a>
                @else
                  <img src="/public/img/users/{{ $user->defaultAvatar }}" class="img-fluid" alt="profile-img">
                @endif
              </div>  
               
              <div class="profile-actions my-2">
                <ul class="nav justify-content-start">
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('private-chat', [$user->id]) }}"> 
                    <i class="fas fa-envelope"></i>
                  </a> 
                </li>
                @if(!is_null($user->skype))
                <li class="nav-item"> 
                  <a class="nav-link" href="skype:{{ $user->skype }}?call">
                    <i class="fas fa-phone"></i>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="skype:{{ $user->skype }}?call">
                    <i class="fas fa-video"></i>
                  </a>
                </li>
                @endif
                @if($user->isComplice() || $user->isAccomplice())
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('private-chat', [$user->id]) }}">
                    <i class="fas fa-paperclip"></i>
                  </a>
                </li>
                @endif
              </ul>
              </div>
              <div class="profile-missions my-3">
                @if($user->isComplice() || $user->isAccomplice())
                  <span>{{ __('user.missions') }}:
                    @if($missions->count() > 0)
                      @foreach($missions as $mission)
                        <a href="{{ route('mission.single', [$mission->mission->id]) }}">{{ $mission->mission->title }}</a>
                        @if(!$loop->last)
                          ,
                        @endif
                      @endforeach
                    @endif
                  </span>
                @endif
              </div>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-lg-11 col-md-11">
                  <span class="profile-name d-block">{{ $user->nickname }}</span>
                  @if($user->awards)
                    @foreach($user->awards as $award)
                      <small class="badge badge-warning" style="font-weight: 100;"><i class="fas fa-medal"></i> {{ $award->name }}</small>
                    @endforeach
                  @endif
                  <div class="star-rating-readonly" data-rating="{{ $user->rating }}"></div>
                  <p class="profile-status">
                    @if($user->isComplice() || $user->isAccomplice())
                      {{ $user->quote }} 
                    @endif
                  </p>
                  <div class="col-12 p-0 profile-data">
                    <table class="table table-sm table-borderless w-auto">
                      <tr>
                        <td>{{ __('user.name') }}:</td>
                        <td>{{ $user->name }}</td>
                      </tr>
                      <tr>
                        <td>{{ __('user.gender') }}:</td>
                        <td>
                          @if($user->gender == 'male')
                            {{ __('user.male') }}
                          @elseif($user->gender == 'female')
                            {{ __('user.female') }}
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>{!! __('user.born') !!}:</td>
                        <td>{{ ($user->born) ? Date::parse($user->born)->format('j F Y') : '' }}</td>
                      </tr>
                      <tr>
                        <td>{!! __('user.registration.date') !!}:</td> 
                        <td>{{ Date::parse($user->created_at)->format('j F Y') }}</td>
                      </tr>
                      <tr>
                        <td>{{ __('user.city') }}:</td>
                        <td>{{ ($user->city) ? $user->userCity->title : '' }}</td>
                      </tr>
                      <tr> 
                        <td>{{ __('user.info') }}:</td>
                        <td>
                          @if($user->isComplice() || $user->isAccomplice())
                            {{ $user->info }}
                          @endif
                        </td>
                      </tr>

                    </table>
                  </div> 
                </div>
                <div class="col-lg-1 d-lg-block d-md-none text-right dropdown">
                  
                  
                    <!--<button class="btn btn-link px-1 py-0 d-md-block d-none color-green" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                      @if(!$markedUser)
                        <a class="dropdown-item confirm-required" href="{{ route('user.mark', [$user->id]) }}">{{ __('user.mark') }}</a>
                      @else
                      <a class="dropdown-item confirm-required" href="{{ route('user.unmark', [$user->id]) }}">{{ __('user.unmark') }}</a>
                      @endif
                    </div>-->
                  


                </div>
              </div>
            </div>
          </div>
        </div>


      <notes-component 
                      :owner="{{ $user->id }}" 
                      :user="{{ Auth::user()->id }}" 
                      :type="'user'"
                      :admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}">
      </notes-component>

      </div>
@endsection