@extends('layouts.main')

@section('title')
  {{ __('user.my.profile') }}
@endsection

@section('content')
      <div class="col-lg-8 col-md-7 px-lg-5 px-md-3 px-3 py-1 profile-block">
        <h4 class="page-top-title my-3">{{ __('user.my.profile') }}</h4>
        <div class="col-12 block-white p-4 text-left">
          <div class="row">
            <div class="col-md-4">
              <div class="profile-img img-caption-wrap text-center">
                @if(Auth::user()->isAvatarExists())
                  <a href="/public/img/users/{{ Auth::user()->avatar() }}" data-fancybox="">
                    <img src="{{ Croppa::url('public/img/users/'.Auth::user()->avatar(), 500, 500) }}" class="img-fluid" alt="profile-img">
                  </a>
                  <div class="img-caption">
                    <a href="{{ route('getAvatarDelete') }}" class="confirm-required">{{ __('user.delete') }}</a>
                  </div>
                @else
                  <img src="public/img/users/{{ Auth::user()->defaultAvatar }}" class="img-fluid" alt="profile-img">
                  <div class="img-caption d-block">
                    <a href="#" data-toggle="modal" data-target="#avatarUploadModal">{{ __('user.upload.image') }}</a>
                  </div> 
                @endif  
                
              </div> 
              <div class="profile-missions my-3">
                <span>{{ __('user.missions') }}:
                    @if($missions->count() > 0)
                      @foreach($missions as $mission)
                        <a href="{{ route('mission.single', [$mission->mission->id]) }}">{{ $mission->mission->title }}</a>
                        @if(!$loop->last)
                          , 
                        @endif
                      @endforeach
                    @endif
                </span>
              </div>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-lg-11 col-md-11">
                  <span class="profile-name d-block">{{ Auth::user()->nickname }} </span>
                  @if(Auth::user()->awards)
                    @foreach(Auth::user()->awards as $award)
                      <small class="badge badge-warning" style="font-weight: 100;"><i class="fas fa-medal"></i> {{ $award->name }}</small>
                    @endforeach
                  @endif
                  <div class="star-rating-readonly" data-rating="{{ Auth::user()->rating }}"></div>
                  <p class="profile-status">
                    {{ Auth::user()->quote }} 
                  </p>
                  <div class="col-12 p-0 profile-data">
                    <table class="table table-sm table-borderless w-auto">
                      <tr>
                        <td>{{ __('user.name') }}:</td>
                        <td>{{ Auth::user()->name }}</td>
                      </tr>
                      <tr>
                        <td>{{ __('user.gender') }}:</td>
                        <td>
                          @if(Auth::user()->gender == 'male')
                            {{ __('user.male') }}
                          @elseif(Auth::user()->gender == 'female')
                            {{ __('user.female') }}
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>{!! __('user.born') !!}:</td>
                        <td>{{ (Auth::user()->born) ? Date::parse(Auth::user()->born)->format('j F Y') : '' }}</td>
                      </tr>
                      <tr>
                        <td>{!! __('user.registration.date') !!}:</td>
                        <td>{{ Date::parse(Auth::user()->created_at)->format('j F Y') }}</td>
                      </tr>
                      <tr>
                        <td>{{ __('user.city') }}:</td>
                        <td>{{ (Auth::user()->city) ? Auth::user()->userCity->title : '' }}</td>
                      </tr>
                      <tr>
                        <td>{{ __('user.info') }}:</td>
                        <td>{{ Auth::user()->info }}</td>
                      </tr>
                    </table> 
                  </div>
                </div>
                <div class="col-lg-1 d-lg-block d-md-none text-right">
                  <a href="{{ route('settings') }}" class="profile-settings d-md-block d-none">
                    <i class="fas fa-cog"></i>
                  </a>
                </div>
              </div>
            </div>

          </div>

        </div>

        <notes-component 
                        :owner="{{ Auth::user()->id }}" 
                        :user="{{ Auth::user()->id }}" 
                        :type="'user'"
                        :admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}">
        </notes-component>

      </div>



<!-- Modal -->
<div class="modal fade" id="avatarUploadModal" tabindex="-1" role="dialog" aria-labelledby="avatarUploadLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="avatarUploadLabel">{{ __('user.upload.image') }}</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('postAvatarUpload') }}" class="ajax-form" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input type="file" name="avatar" accept=".jpg, .jpeg, .JPEG, .png">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default">{{ __('user.upload') }}</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection