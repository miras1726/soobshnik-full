@extends('layouts.main')

@section('title')
  {{ __('user.retrieve.profile') }}
@endsection

@section('content') 

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">{{ __('user.retrieve.profile') }}</h4>
		  	
		  	<div class="col-12 block-white px-4 py-4 text-left">
		  		<div class="col-12">
		  			{{ __('user.welcome.back.text') }}
		  			<div class="col-12 mt-3 px-0 text-right">
		  				<form action="{{ route('profile.retrieve.post') }}" method="post">
		  					@csrf
		  					<button type="submit" class="btn-default btn-default-sm w-auto d-inline-block">{{ __('user.retrieve') }}</button>
		  				</form>
		  			</div>
		  		</div>
		  	</div>

		  </div>
@endsection