@extends('layouts.main')

@section('title')
  {{ __('user.search.results') }}
@endsection

@section('content')

<div class="col-md-8 px-5 py-1 mb-3">
	
	<h4 class="col-12 page-top-title my-3">{{ __('user.search.results') }}</h4>
	
	<div class="col-12">
		@include('common.alerts')
	</div>
	
	<div class="col-12">
		<div class="row">
			@if($complices->count() > 0)
				@foreach($complices as $complice)
					<div class="col-md-4 mb-4">
						<div class="col-12 block-white user-list-item text-center p-3">
						
						<img src="{{ Croppa::url('public/img/users/'.$complice->user->avatar(), 120, 120) }}" class="rounded-circle">
						<p class="h5 mb-0 mt-1">
							<a href="{{ route('userProfile', [$complice->user->nickname]) }}" class="profile-name-link" data-toggle="tooltip" title="{{ $complice->user->short_info }}">{{ $complice->user->nickname }}</a>
						</p>
						<p class="profile-rating mt-1 mb-2">
							<div class="star-rating-readonly" data-rating="{{ $complice->user->rating }}"></div>
						</p>
						@if(!$complice->user->isComplice($missionId, false)) 
							<a href="{{ route('complice.add', [$complice->user->id, $missionId]) }}">{{ __('user.hook') }}</a>
						@elseif($complice->user->getCompliceStatus($missionId, false) == 0) 
							<a href="{{ route('complice.delete', [$complice->user->id, $missionId]) }}" class="color-red confirm-required">{{ __('user.cancel.request') }}</a>
						@else 
							<a href="{{ route('complice.delete', [$complice->user->id, $missionId]) }}" class="color-gray confirm-required">{{ __('user.remove.complice') }}</a>
						@endif
						<span class="fs-9 d-block mt-2">{{ $complice->mission->title }}</span>
						</div>
					</div>
				@endforeach
			@endif
 

		</div>
	</div>
</div>

	<div class="col-2 block-white-lite p-0 d-md-block d-none">
		<div class="sticky-top side-menu p-4" dir="rtl">
			<div dir="ltr"> 
			  	<h6>{{ __('user.search') }}</h6>
			  	<hr>

			  	<div class="row">
			  		@widget('search')
			  	</div>
		  	</div>
	  	</div>
	</div>

@endsection

