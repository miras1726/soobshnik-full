@extends('layouts.main')

@section('title')
  @lang('adavai.bank')
@endsection

@section('content') 
	<div class="col-md-8 px-5 py-1 profile-block">
		<h4 class="page-top-title my-3">@lang('adavai.bank')</h4>

		@include('common.errors') 
		@include('common.alerts')
		
		<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
			<div class="col-12">
				<h6 class="font-medium d-inline-block">@lang('adavai.license')</h6>
				<span class="float-right">@lang('adavai.on.bank'): {{ $user->money }}</span>
				<div class="clearfix"></div>
				<hr>
				<div class="alert alert-success text-center" role="alert"> 
					@lang('adavai.license.not.necessary')
				</div>
				<p class="color-green font-medium">@lang('adavai.license.features'):</p>
				<ul type="circle">
					<li>@lang('adavai.license.feature.1')</li>
					<li>@lang('adavai.license.feature.2')</li>
				</ul>
				<p class="color-green font-light">@lang('adavai.you.can.buy.for.month')</p>
				<a href="{{ route('get.replenish.balance') }}" class="btn-green-lite w-25">{{ __('user.balance.replenish') }}</a>
				<hr>
				
				@if(!$user->adavaiProfile->active_license)
					<form action="{{ route('adavai.buy.license', $user->id) }}" method="post">
						@csrf
						<button type="submit" class="btn-default btn-default-sm w-25 confirm-required">@lang('adavai.buy.license')</button>
					</form>
				@else
					<small class="color-gray"><i class="fas fa-exclamation-triangle"></i> @lang('adavai.license.expires.in') {{ $user->adavaiProfile->license->addMonth()->format('d.m.Y') }}</small>
				@endif
			</div>
		</div>
		
		@if($user->awards->count() > 0)
			@if(!$user->awards[0]->feedback)	
				<div class="col-12 block-white px-md-5 px-3 py-4 text-left mt-4">
					<h6 class="text-center">@lang('adavai.you.are.best')</h6>
					<form action="{{ route('adavai.award.leave.feedback') }}" method="post">
						@csrf
						<div class="form-group">
							<label>@lang('adavai.you.can.leave.feedback')</label>
							<textarea class="form-control" name="feedback" placeholder="@lang('adavai.your.feedback')"></textarea>
						</div>
						<div class="form-group">
							<button class="btn-default btn-default-sm w-25 float-right">@lang('adavai.send')</button>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			@endif
		@endif
		
	</div>

	<div class="col-2 block-white-lite p-0 d-md-block d-none">
		<div class="sticky-top side-menu p-4" dir="rtl">
			<div dir="ltr"> 
		  	<h6 class="text-center color-green">@lang('adavai.organizer.of.year')</h6>
		  	<hr>
		  	<div class="row">
			  	<div class="col-12">
			  		<p>@lang('adavai.lucky.date'): <span class="color-green">31.12.{{ date('Y') }}</span></p>
			  		<p>@lang('adavai.lucky.sum'): <span class="color-green">{{ $sum }}</span></p>
			  		<p>@lang('adavai.lucky.chance'): <span class="color-green">1:{{ $profiles }}</span></p>
			  	</div>
		  	</div>
		  	<hr>
		  	@if($best)
		  	<h6 class="text-center color-green">@lang('adavai.organizer.of.year')</h6>
		  	<div class="side-profile-img p-2">
		 		@if($best->user && $best->user->isAvatarExists())
		            <img src="{{ Croppa::url('/public/img/users/'.$best->user->avatar(), 500, 500) }}" class="img-fluid rounded-circle" alt="profile-img">
	            @else
	              <img src="/public/img/users/{{ $best->user->defaultAvatar }}" class="img-fluid rounded-circle" alt="profile-img">
	            @endif
	            <a href="{{ $best->user ? route('userProfile', $best->user->nickname) : '#' }}" class="btn-default btn-default-sm mt-3">@lang('adavai.lucky.profile')</a>
	            <p class="my-2 text-center color-gray">@lang('adavai.lucky.feedback'):</p>
	            <p>
		            {{ $best->feedback }}
	            </p>
			</div>
			@endif
		</div>
		</div>
	</div>
@endsection
