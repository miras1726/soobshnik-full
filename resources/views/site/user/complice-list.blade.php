@extends('layouts.main')

@section('title')
  {{ __('user.complices') }}
@endsection

@section('content')

<div class="col-md-8 px-5 py-1 mb-3">
	
	<h4 class="col-12 page-top-title my-3">{{ __('user.complices') }}</h4>
	
	<div class="col-12">
		@include('common.alerts')
	</div>  
	 
	<div class="col-12">
		@foreach($userMissions as $mission)
		@if($mission->authUserMissionComplicesCount($mission->mission->id) > 0)
		<p class="page-top-title">{{ $mission->mission->title }}</p>
		<div class="row">

			@foreach($complices as $complice)
				@if($mission->mission->id == $complice->mission_id)
				<div class="col-md-4 mb-4">
					<div class="col-12 block-white user-list-item text-center p-3">
					
					<img src="{{ Croppa::url('public/img/users/'.$complice->user->avatar(), 120, 120) }}" class="rounded-circle">
					<p class="h5 mb-0 mt-1">
						<a href="{{ route('userProfile', [$complice->user->nickname]) }}" class="profile-name-link" data-toggle="tooltip" title="{{ $complice->user->short_info }}">{{ $complice->user->nickname }}</a>
					</p>
					<p class="profile-rating mt-1 mb-1">
						<div class="star-rating-readonly" data-rating="{{ $complice->user->rating }}"></div>
					</p>

				<div class="mb-1">
				      <a class="h6 color-green mx-2" href="{{ route('private-chat', [$complice->user->id]) }}"> 
				        <i class="fas fa-envelope"></i>
				      </a>
				  
				    <a class="h6 mx-2 {{ !is_null($complice->user->skype) ? 'color-green' : 'color-green-mute link-disabled' }}" 
				       href="skype:{{ $complice->user->skype }}?call">
				        <i class="fas fa-phone"></i>
				      </a>
				      <a class="h6 mx-2 {{ !is_null($complice->user->skype) ? 'color-green' : 'color-green-mute link-disabled' }}"
				       	 href="skype:{{ $complice->user->skype }}?call">
				        <i class="fas fa-video"></i>
				      </a>
				    
				      <a class="h6 color-green mx-2" href="{{ route('private-chat', [$complice->user->id]) }}">
				        <i class="fas fa-paperclip"></i>
				      </a>
				</div>

					@if($complice->isComplice($complice->user->id, $complice->mission_id, true))
						<a href="{{ route('complice.refuse.get', [$complice->user->id, $complice->mission_id]) }}" class="color-gray confirm-required">{{ __('user.remove.complice') }}</a>
					@else
						@if((Auth::user()->id == $complice->user_id) && ($complice->accepted == 0))
							<a href="{{ route('complice.delete', [$complice->user->id, $complice->mission_id]) }}" class="color-red confirm-required">{{ __('user.cancel.request') }}</a>
						@else
							<a href="{{ route('complice.accept', [$complice->user->id, $complice->mission_id]) }}" class="color-green confirm-required">{{ __('user.accept.request') }}</a> 
							/
							<a href="{{ route('complice.delete', [$complice->user->id, $complice->mission_id]) }}" class="color-red confirm-required">{{ __('user.decline.request') }}</a>
						@endif
					@endif
					</div>
				</div>
				@endif
			@endforeach
		</div>
		@endif
		@endforeach
	</div>
</div>

	<div class="col-2 block-white-lite p-0 d-md-block d-none">
		<div class="sticky-top side-menu p-4" dir="rtl">
			<div dir="ltr"> 
	  	<h6>{{ __('user.search') }}</h6>
	  	<hr>

	  	<div class="row">
	  		@widget('search')
	  	</div>

	</div>
	</div>
</div>

@endsection

