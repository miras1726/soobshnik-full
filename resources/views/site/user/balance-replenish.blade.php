@extends('layouts.main')

@section('title')
  @lang('user.balance.replenish')
@endsection

@section('content') 

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">@lang('user.balance.replenish')</h4>

		  	@include('common.errors')
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-md-8 col-12">
		  			<form action="{{ route('post.replenish.balance') }}" method="post">
		  				@csrf
				  		<table class="table table-sm table-borderless">
							<tr>
								<td class="table-form-label">@lang('user.balance.current'):</td>
								<td class="form-group">
							        {{ $user->money }} @lang('user.roubles')
								</td>
							</tr>
							<tr>
								<td class="table-form-label">@lang('user.sum'):</td>
								<td class="form-group">
							        <input type="number" class="form-control" name="sum" min="10" value="10">
								</td>
							</tr>
							<tr>
							<tr>
								<td class="table-form-label"></td>
								<td>
									<button type="submit" class="btn-default w-auto">@lang('user.replenish')</button>
								</td>
							</tr>
						</table> 
		  			</form>
		  		</div>
		  	</div>

		  </div>
@endsection