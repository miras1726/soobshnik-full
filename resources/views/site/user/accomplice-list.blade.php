 @extends('layouts.main')

@section('title') 
  {{ __('user.accomplices') }}
@endsection

@section('content') 

<div class="col-md-8 px-5 py-1 mb-3">
	
	<h4 class="col-12 page-top-title my-3">{{ __('user.accomplices') }}</h4>
	
	<div class="col-12">
		@include('common.alerts')
	</div>
	
	<div class="col-12">
		<div class="row"> 
			@if($accomplices->count() > 0)
				@foreach($accomplices as $accomplice)
					<div class="col-md-4 mb-4">
						<div class="col-12 block-white user-list-item text-center p-3">
						
						<img src="{{ Croppa::url('public/img/users/'.$accomplice->user->avatar(), 120, 120) }}" class="rounded-circle">
						<p class="h5 mb-0 mt-1">
							<a href="{{ route('userProfile', $accomplice->user->nickname) }}" class="profile-name-link" data-toggle="tooltip" data-html="true" title="{{ $accomplice->user->short_info }}">{{ $accomplice->user->nickname }}</a>
						</p> 
						<p class="profile-rating mt-1 mb-2">
							<div class="star-rating-readonly" data-rating="{{ $accomplice->user->rating }}"></div>
						</p>
						
						<div class="mb-2">
						      <a class="h6 color-green mx-2" href="{{ route('private-chat', [$accomplice->user->id]) }}"> 
						        <i class="fas fa-envelope"></i>
						      </a>
						  
						    <a class="h6 mx-2 {{ !is_null($accomplice->user->skype) ? 'color-green' : 'color-green-mute link-disabled' }}" 
						       href="skype:{{ $accomplice->user->skype }}?call">
						        <i class="fas fa-phone"></i>
						      </a>
						      <a class="h6 mx-2 {{ !is_null($accomplice->user->skype) ? 'color-green' : 'color-green-mute link-disabled' }}"
						       	 href="skype:{{ $accomplice->user->skype }}?call">
						        <i class="fas fa-video"></i>
						      </a>
						    
						      <a class="h6 color-green mx-2" href="{{ route('private-chat', [$accomplice->user->id]) }}">
						        <i class="fas fa-paperclip"></i>
						      </a>
						</div>

						<a href="{{ route('accomplice.delete', [$accomplice->user->id]) }}" class="color-gray confirm-required">{{ __('user.delete') }}</a>
						
						</div>
					</div>
				@endforeach
			@endif
		</div>
	</div>
</div>

@endsection

