@extends('layouts.main')

@section('title')
  {{ __('user.profile.settings') }}
@endsection

@section('content') 

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">{{ __('user.profile.settings') }}</h4>

		  	@include('common.errors')
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-md-8 col-12">
			  		<form action="{{ route('settingsUpdate') }}" class="table-form" method="post">
			  			@csrf
			  		<table class="table table-sm table-borderless">
						<tr>
							<td class="table-form-label">{{ __('user.nickname') }}:</td>
							<td class="form-group">
						        <input type="text" class="form-control" value="{{ Auth::user()->nickname }}" readonly="true">
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('user.name') }}:</td>
							<td class="form-group">
						        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ Auth::user()->name }}">

						        @if ($errors->has('name'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('name') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('user.gender') }}:</td>
							<td  class="form-group">
								<select name="gender" id="gender" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}">
									<option value="">{{ __('user.none.selected') }}</option>
									<option value="male" {{ (Auth::user()->gender == 'male' ? 'selected' : '') }}>{{ __('user.male') }}</option>
									<option value="female" {{ (Auth::user()->gender == 'female' ? 'selected' : '') }}>{{ __('user.female') }}</option>
								</select>

								@if ($errors->has('gender'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('gender') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{!! __('user.born') !!}:</td>
							<td  class="form-group">
								<input type="date" name="born" id="born" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}" value="{{ Date::parse(Auth::user()->born)->format('Y-m-d') }}"> 
								@if ($errors->has('born'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('born') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('user.city') }}:</td>
							<td  class="form-group">
						        <select class="form-control" name="city">
									@if($countries->count() > 0)
										@foreach($countries as $country)
										<optgroup label="{{ $country->title }}"></optgroup>
											@if($country->cities->count() > 0)
												@foreach($country->cities as $city)
													<option value="{{ $city->id }}" {{ (Auth::user()->city == $city->id) ? 'selected' : '' }}>{{ $city->title }}</option>
												@endforeach
											@endif
										@endforeach
									@endif
								</select>

						        @if ($errors->has('city'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('city') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">Skype:</td>
							<td  class="form-group">
								<input type="text" name="skype" id="skype" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}" value="{{ Auth::user()->skype }}"> 
								@if ($errors->has('skype'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('skype') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('user.locale') }}:</td>
							<td  class="form-group">
								<select name="locale" id="locale" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}">
									<option value="ru" {{ (Auth::user()->locale == 'ru' ? 'selected' : '') }}>{{ __('user.ru') }}</option>
									<option value="en" {{ (Auth::user()->locale == 'en' ? 'selected' : '') }}>{{ __('user.en') }}</option>
								</select>

								@if ($errors->has('locale'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('locale') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('user.timezone') }}:</td>
							<td  class="form-group">
								<select name="timezone" id="timezone" class="form-control">
								    @foreach (timezone_identifiers_list() as $timezone)
								        <option value="{{ $timezone }}"{{ $timezone == old('timezone', Cookie::get('userTimeZoneFromSettings')) ? ' selected' : '' }}>{{ $timezone }}</option>
								    @endforeach
								</select>
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('user.info') }}:</td>
							<td  class="form-group">
						        <textarea id="info" type="text" class="form-control{{ $errors->has('info') ? ' is-invalid' : '' }}" name="info">{{ Auth::user()->info }}</textarea>

						        @if ($errors->has('info'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('info') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label">{{ __('user.personal.statement') }}:</td>
							<td  class="form-group">
						        <textarea id="quote" type="text" class="form-control{{ $errors->has('quote') ? ' is-invalid' : '' }}" name="quote">{{ Auth::user()->quote }}</textarea>

						        @if ($errors->has('quote'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('quote') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('user.new.password') }}:</td>
							<td  class="form-group">
								<input type="password" name="password" id="password" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}"> 
								@if ($errors->has('password'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('password') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('user.new.password.accept') }}:</td>
							<td  class="form-group">
								<input type="password" name="password_confirmation" id="password_confirmation" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"> 
								@if ($errors->has('password_confirmation'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('password_confirmation') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<button type="submit" class="btn-default w-auto">{{ __('user.save') }}</button>

							</td>
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<hr>
								<a href="{{ route('profile.delete.get') }}" class="color-gray">{{ __('user.profile.delete') }}</a>
							</td>
						</tr>
					</table> 
			  		</form>
		  		</div>
		  	</div>

		  </div>
@endsection

@section('js')
{!! JsValidator::formRequest('App\Http\Requests\SettingsRequest') !!}
@endsection

