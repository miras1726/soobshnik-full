   @extends('layouts.main')

@section('title')
  {{ __('chat.chat') }}

@endsection

@section('content')
		  <div class="col-lg-8 col-md-7 px-lg-5 px-md-3 px-3 py-1 profile-block">
		  	<h4 class="page-top-title my-3">
				<a href="{{ route('private-chat') }}" class="color-black">
		  			{{ __('chat.chats') }}
		  		</a>
		  	</h4> 

		  		<private-chat :user_id="{{ Auth::user()->id }}" :selected="{{ $selected }}"></private-chat>

		  </div>
	<div class="col-2 block-white-lite p-0 d-md-block d-none">
		<div class="sticky-top side-menu p-4" dir="rtl">
			<div class="row" dir="ltr"> 
		  		@include('site.chat._side')
		  	</div>
		</div>
	</div>

@endsection

		