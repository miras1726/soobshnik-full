 @php 
	$public = Auth::user()->publicConversations();
	$private = Auth::user()->privateConversations();
@endphp
	  		<div class="col-12">
	  			<h6>{{ __('chat.mission.chats') }}</h6>
	  			<hr>
	  		</div>
	  		@if($public->count() > 0)
				@foreach($public as $chat) 
					@if($chat->mission)
					<div class="col-12">
			  		<div class="row">
				  		<div class="col-4 position-relative">
				  			<img src="{{ Croppa::url('public/img/missions/'.$chat->missionAvatar, 40, 40) }}" class="img-fluid rounded-circle">
				  			@if($chat->unreadCount)
				  				<span class="chat-unread-badge" style="bottom: 5px">{{ $chat->unreadCount }}</span>
				  			@endif
				  		</div>
				  		<div class="col-8 pl-0">
				  			<a href="{{ route('chat', [$chat->mission['id']]) }}" class="fs-9">{{ $chat->mission['title'] }}</a>
				  			<p class="fs-8">{{ $chat->users->count() }} {{ __('chat.participants') }}</p>
				  		</div>
			  		</div>
					</div>
					@endif
				@endforeach
			@endif


	  		<div class="col-12 mt-3">
	  			<h6>{{ __('chat.tet.a.tet') }}</h6>
	  			<hr>
	  		</div>
	  		@if($private->count() > 0) 
		  		@foreach($private as $chat) 
		  			@if($chat->companion)
			  		<div class="col-12 mb-2">
				  		<div class="row">
					  		<div class="col-4 position-relative">
					  			<img src="{{ Croppa::url('public/img/users/'.$chat->companion->avatar(), 40, 40) }}" class="img-fluid rounded-circle">
					  			@if($chat->unreadCount)
					  				<span class="chat-unread-badge">{{ $chat->unreadCount }}</span>
					  			@endif
					  		</div>
					  		<div class="col-8 pl-0">
					  			<a href="{{ route('private-chat', [$chat->companion->id]) }}" class="fs-9">{{ $chat->companion->nickname }}</a>
					  			<div class="star-rating-readonly" data-rating="{{ $chat->companion->rating }}"></div>
					  		</div>
				  		</div>
			  		</div>
	  				@endif
		  		@endforeach
	  		@endif