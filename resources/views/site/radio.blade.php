@extends('layouts.app')

@section('main')
<!--НАЧАЛО БЛОКА РАДИО ОНЛАЙН-->

<div class="col-12" id="radio_block">
	
	<div class="col-12 my-3 text-center">
		<img src="/public/img/logo.png" class="img-fluid">
	</div>
	
	<div id="ro_h2"></div> 
	
	<div id="ro_player"></div>

	<ul class="list-group mb-3">
	  <li class="list-group-item">
	  	Chillout/Lounge
	  </li>
	  <li class="list-group-item">
	  	<a href="#" onclick="playRadio(154);setOpacity(154);return false;" id="rad_154">
			<span class="radio_option" id="rad_154_img"><i class="fas fa-play-circle"></i> 
				1.FM - Chillout Lounge Radio
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(612);setOpacity(612);return false;" id="rad_612">
			<span class="radio_option" id="rad_612_img"><i class="fas fa-play-circle"></i> 
				Атмосфера
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(256);setOpacity(256);return false;" id="rad_256">
			<span class="radio_option" id="rad_256_img"><i class="fas fa-play-circle"></i> 
				Сумерки
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(3499);setOpacity(3499);return false;" id="rad_3499">
			<span class="radio_option" id="rad_3499_img"><i class="fas fa-play-circle"></i> 
				Radio Record: ChillOut
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(1094);setOpacity(1094);return false;" id="rad_1094">
			<span class="radio_option" id="rad_1094_img"><i class="fas fa-play-circle"></i> 
				ENERGY Lounge
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(336);setOpacity(336);return false;" id="rad_336">
			<span class="radio_option" id="rad_336_img"><i class="fas fa-play-circle"></i> 
				Lounge FM
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(2571);setOpacity(2571);return false;" id="rad_2571">
			<span class="radio_option" id="rad_2571_img"><i class="fas fa-play-circle"></i> 
				Lounge FM Terrace
			</span>
		</a>
	  </li>
	  <li class="list-group-item mt-4">
	  	Relax
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(28);setOpacity(28);return false;" id="rad_28">
			<span class="radio_option" id="rad_28_img"><i class="fas fa-play-circle"></i> 
				San FM Relax
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(126);setOpacity(126);return false;" id="rad_126">
			<span class="radio_option" id="rad_126_img"><i class="fas fa-play-circle"></i> 
				Aloha Joe Radio
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(2943);setOpacity(2943);return false;" id="rad_2943">
			<span class="radio_option" id="rad_2943_img"><i class="fas fa-play-circle"></i> 
				Радио странствий
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(35);setOpacity(35);return false;" id="rad_35">
			<span class="radio_option" id="rad_35_img"><i class="fas fa-play-circle"></i> 
				Романтика
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(315);setOpacity(315);return false;" id="rad_315">
			<span class="radio_option" id="rad_315_img"><i class="fas fa-play-circle"></i> 
				Relax FM (Релакс FM)
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(3486);setOpacity(3486);return false;" id="rad_3486">
			<span class="radio_option" id="rad_3486_img"><i class="fas fa-play-circle"></i> 
				Nicefm
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(46);setOpacity(46);return false;" id="rad_46">
			<span class="radio_option" id="rad_46_img"><i class="fas fa-play-circle"></i> 
				Спокойное
			</span>
		</a>
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(882);setOpacity(882);return false;" id="rad_882">
			<span class="radio_option" id="rad_882_img"><i class="fas fa-play-circle"></i> 
				101.ru - Звуки Природы
			</span>
		</a>
	  </li>
      <li class="list-group-item mt-4">
	  	New age
	  </li>
	  <li class="list-group-item">
		<a href="#" onclick="playRadio(2417);setOpacity(2417);return false;" id="rad_2417">
			<span class="radio_option" id="rad_2417_img"><i class="fas fa-play-circle"></i> 
				New Age Radio
			</span>
		</a>
	  </li>
	</ul>
</div>



<!--КОНЕЦ БЛОКА РАДИО ОНЛАЙН-->
<!--
<div class="col-12">
	<a href="#" onclick="playRadio(23);setOpacity(23);return false;" id="rad_23">
		<p style="height:20px; width:50px; background-color: green;"></p>
	</a>
	<a href="#" onclick="playRadio(48);setOpacity(48);return false;" id="rad_23">
		<p style="height:20px; width:50px; background-color: green;"></p>
	</a>
	<button class="btn btn-default" onclick="playRadio(''); setOpacity(); return false;">turn off</button>
</div>
-->
@endsection