@extends('layouts.main')

@section('title')
  {{ __('mission.photo.reports') }}
@endsection

@section('content')

		  <div class="col-md-8 px-md-5 px-3 py-1 profile-block">
		  	<h4 class="page-top-title my-3">{{ __('mission.photo.reports') }}</h4>
		  	<p>{{ __('mission.photo.reports.text') }}</p>

			<photo-report 
						:user_joined="false" 
						:owner="0" 
						:user="{{ Auth::user()->id }}" 
						:type="'photo-report'"
						:report_type="'mission'"
						:all_reports="true"
						:admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}">
			</photo-report>


		  </div>

	<div class="col-2 block-white-lite p-0 d-md-block d-none">
		<div class="sticky-top side-menu p-4" dir="rtl">
			<div class="row" dir="ltr"> 
				  		<div class="col-12">
							<h6>{{ __('mission.photo.reports') }}</h6>
							<hr>
							@if($photos->count() > 0)
								@foreach($photos as $mission)
									<div class="col-12 text-center mb-4">
										<img src="{{ Croppa::url('/public/img/missions/'.$mission->avatar(), 120, 120) }}" class="rounded-circle">
										<a href="{{ route('photo-report.mission', [$mission->id]) }}" class="h6 d-block mt-2">{{ $mission->title }}</a>
										<p class="fs-9 mt-2 mb-1 color-green">
											{{ $mission->photoReports()->count() }} {{ __('diary.notes') }}
										</p>
									</div>
								@endforeach
							@endif
						</div>
					</div>

				</div>
			</div>

@endsection
