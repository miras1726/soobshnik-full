  @extends('layouts.main')

@section('title')
  {{ __('notification.notifications') }}
@endsection

@section('content')

<div class="col-md-8 px-5 py-1 mb-3">
	
	<div class="col-12 my-3">
		<div class="row">
			<div class="col-6 p-0">
				<h4 class="page-top-title">{{ __('notification.notifications') }}</h4>
			</div>
			<div class="col-6 p-0 text-right">
				@if($notifications->count() > 0)
				<a href="{{ route('notification.clean', [request()->type, request()->targetId]) }}" 
				class="float-right mt-2 color-red confirm-required">{{ __('notification.clear.all') }}</a>
				@endif
			</div>
		</div>
	</div>
	
  	<div class="col-12 block-white p-4 text-left">
  		@if($notifications->count() > 0)
			<div class="list-group">
	  			@foreach($notifications as $not)
				  <div class="list-group-item list-group-item-action">
				    <div class="d-flex w-100 justify-content-between">
				      <a href="{{ $not->link }}" class="link-black"><h5 class="mb-1">{{ $not->title }}</h5></a>
				      <small>{{ $not->date() }}</small>
				    </div>
				    <p class="mb-1">{!! $not->text !!}</p>
				    <small><a href="{{ route('notification.delete', [$not->id]) }}" class="confirm-required color-red">{{ __('notification.remove') }}</a></small>
				  </div>
	  			@endforeach
			</div> 
  		@else
  			<h6 class="page-top-title text-center color-gray">{{ __('notification.empty') }}</h6>
  		@endif
  		<div class="mt-4" id="pagination">
  			{{ $notifications->links() }}
  		</div>
  	</div>
</div>

@endsection

