 @extends('layouts.main')

@section('title')
	{{ __('mission.mission.report') }}
@endsection

@section('content')
 
		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">{{ __('mission.mission.report') }} > {{ $mission->mission->title }}</h4>
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="row">
			  		<div class="col-9">
				  			@csrf
				  			<div class="row">

				  				<div class="col-12">
					  				<div class="row form-group">
						  				<div class="col-12">
						  					<label>{{ __('mission.is.completed') }}</label> <b>{{ ($mission->completed) ? __('mission.yes') : __('mission.no') }}</b>
										</div>
					  				</div>
				  				</div>

				  				<div class="col-12">
					  				<div class="row form-group">
						  				<div class="col-12">
						  					<label>{{ __('mission.is.completed.in.time') }}</label> <b>{{ ($mission->completed) ? __('mission.yes') : __('mission.no') }}</b>
										</div> 
					  				</div>
				  				</div>

				  				<div class="col-12">
					  				<div class="row form-group">
						  				<div class="col-12">
						  					<label>{{ __('diary.progress.title') }}</label>
											<div class="progress-outer">
												<div class="progress-inner" style="width:{{ $diary->calculateProgress()  }}%; left:{{ $diary->calculateProgress()  }}%;"></div>
											</div>
										</div> 
					  				</div> 
				  				</div> 

				  				<div class="col-12">
					  				<div class="row form-group">
						  				<div class="col-12">
											<label>{{ __('diary.goal.reached.title') }}</label>
											<div class="progress-outer">
												<div class="progress-inner" style="width:{{ $diary->goalReachedProgress(true)  }}%; left:{{ $diary->goalReachedProgress(true)  }}%;"></div>
											</div>
										</div> 
					  				</div> 
				  				</div>

				  				<div class="col-12">
					  				<div class="row form-group">
						  				<div class="col-12">
											<label>{{ __('diary.is.easy.title') }}</label>
											<div class="progress-outer">
												<div class="progress-inner" style="width:{{ $diary->isEasyProgress(true)  }}%; left:{{ $diary->isEasyProgress(true)  }}%;"></div>
											</div>
										</div> 
					  				</div> 
				  				</div> 

				  				<div class="col-12">
					  				<div class="row form-group">
						  				<div class="col-12">
											<label>{{ __('diary.motivation.enough.title') }}</label>
											<div class="progress-outer">
												<div class="progress-inner" style="width:{{ $diary->motivationProgress(true)  }}%; left:{{ $diary->motivationProgress(true)  }}%;"></div>
											</div>
										</div> 
					  				</div> 
				  				</div>  

				  				<div class="col-12"> 
					  				<div class="row form-group">
						  				<div class="col-12">
											<label>{{ __('diary.review') }}:</label>
											<br>
											<b>{{ $mission->review }}</b>
										</div> 
					  				</div> 
				  				</div> 

				  				<div class="col-12"> 
					  				<div class="row form-group">
						  				<div class="col-12">
											<label>{{ __('mission.started.at') }}:</label>
											<b>{{ $mission->created_at }}</b>
										</div> 
					  				</div> 
				  				</div>  

				  				<div class="col-12">  
					  				<div class="row form-group">
						  				<div class="col-12">
											<label>{{ __('mission.finished.at') }}:</label>
											<b>{{ $mission->finished_at }}</b>
										</div> 
					  				</div> 
				  				</div>

				  				<div class="col-12">  
					  				<div class="row form-group">
						  				<div class="col-12">
											<a href="{{ route('diary.single', [$mission->diary()->id]) }}" class="color-green">{{ __('diary.diary') }}</a>
										</div> 
					  				</div> 
				  				</div> 

				  			</div>
			  		</div>
		  			<div class="col-md-3">
						<p class="mb-3">{{ __('mission.complices') }}</p>
						<div class="col-12">
							<div class="row">

								@if($mission->authUserComplices()->count() > 0)
									@foreach($mission->authUserComplices() as $complice)
										<div class="col-12 mb-1">
											<div class="row">
												<div class="col-4 p-1">
													<img src="{{ Croppa::url('public/img/users/'.$complice->user->avatar(), 60, 60) }}" class="img-fluid rounded-circle">
												</div>
												<div class="col-8 px-2">
													<p class="h6 my-1">
														<a href="#" class="profile-name-link">{{ $complice->user->nickname }}</a>
													</p>
													<p class="profile-rating mb-0">
														<div class="star-rating-readonly" data-rating="{{ $complice->getCompliceRating() }}"></div>
													</p>
												</div>
											</div>
										</div>
									@endforeach
								@endif

							</div>
						</div>
					</div>
				</div>
		  	</div>

		  </div>
@endsection

