@extends('layouts.main')

@section('title')
	{{ __('mission.mission.report') }}
@endsection

@section('content')
 
		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">{{ __('mission.mission.report') }}</h4>
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-12">
			  		<form action="{{ route('mission.report.post') }}" class="table-form" method="post">
			  			@csrf
			  			<div class="row">

			  				<div class="col-12">
				  				<div class="row form-group">
							  		
					  				<div class="col-12">
							  			<label for="review" id="review">{{ __('mission.is.completed') }}</label>
									</div>
					  				<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input" type="radio" name="completed" id="completed" value="1" required>
										  <label class="form-check-label" for="completed">
										    {{ __('mission.yes') }}
										  </label>
										</div>
									</div>
									<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input" type="radio" name="completed" id="completed" value="0" required>
										  <label class="form-check-label" for="completed">
										    {{ __('mission.no') }}
										  </label>
										</div>
									</div>
									@if ($errors->has('completed'))
							            <span class="invalid-feedback" role="alert">
							                <span>{{ $errors->first('completed') }}</span>
							            </span>
							        @endif
				  				</div>
			  				</div>

			  				<div class="col-12">

				  				<div class="row form-group">
							  		
					  				<div class="col-12">
							  			<label for="completed_time" id="completed_time">{{ __('mission.is.completed.in.time') }}</label>
									</div>
					  				<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input{{ $errors->has('completed_time') ? ' is-invalid' : '' }}" type="radio" name="completed_time" id="completed_time" value="1" required>
										  <label class="form-check-label" for="completed_time">
										    {{ __('mission.yes') }}
										  </label>
										</div>
									</div>
									<div class="col-md-1 col-3">
						  				<div class="form-check">
										  <input class="form-check-input{{ $errors->has('completed_time') ? ' is-invalid' : '' }}" type="radio" name="completed_time" id="completed_time" value="0" required>
										  <label class="form-check-label" for="completed_time">
										    {{ __('mission.no') }}
										  </label>
										</div>
									</div>
									@if ($errors->has('completed_time'))
							            <span class="invalid-feedback" role="alert">
							                <span>{{ $errors->first('completed_time') }}</span>
							            </span>
							        @endif
				  				</div>
			  				</div>

			  				<div class="col-12">
				  				<div class="form-group">
				  					<label for="review" id="review">{{ __('mission.review') }}</label>
							        <textarea id="review" type="text" class="form-control{{ $errors->has('review') ? ' is-invalid' : '' }}" name="review"></textarea>

							        @if ($errors->has('review'))
							            <span class="invalid-feedback" role="alert">
							                <span>{{ $errors->first('review') }}</span>
							            </span>
							        @endif			  					
				  				</div>
			  				</div>
			  				 
			  				
			  					@if($complices->count() > 0)
			  						@foreach($complices as $complice)
			  							<div class="col-4">
			  							<div class="col-12">
											<div class="row">
												<div class="col-3 p-1">
													<figure class="p-0 note-user-img mb-2">
													<img src="{{ Croppa::url('public/img/users/'.$complice->user->avatar(), 40, 40) }}" class="img-fluid rounded-circle">
													</figure>
												</div>
												<div class="col-8 px-2">
													<p class="h6 mt-2">
														<a href="#" class="profile-name-link">{{ $complice->user->nickname }}</a>
													</p>
												</div>
												<div class="col-12 px-2">
													<div class="form-group">
									  					<label for="review" class="mb-0">{{ __('mission.punctuality') }}</label>
												        <div class="star-rating" id="{{ $complice->id }}">
												        </div>		  					
												        <input type="hidden" id="rating_par_1_{{ $complice->id }}" value="0">
									  				</div>
									  				<div class="form-group">
									  					<label for="review" class="mb-0">{{ __('mission.support') }}</label>
												        <div class="star-rating" id="{{ $complice->id }}">
												        </div>			  					
												        <input type="hidden" id="rating_par_2_{{ $complice->id }}" value="0">
									  				</div>
									  				<div class="form-group">
									  					<label for="review" class="mb-0">{{ __('mission.motivation') }}</label>
												        <div class="star-rating" id="{{ $complice->id }}">
												        </div>			  					
												        <input type="hidden" id="rating_par_3_{{ $complice->id }}" value="0">
									  				</div>
									  				<div class="form-group">
									  					<label for="review" class="mb-0">{{ __('mission.communication') }}</label>
												        <div class="star-rating" id="{{ $complice->id }}">
												        </div>			  					
												        <input type="hidden" id="rating_par_4_{{ $complice->id }}" value="0">
									  				</div>
												    <input type="hidden" name="complice_{{ $complice->id }}" id="complice_{{ $complice->id }}" value="0">
												</div>
											</div>
										</div>
										</div>
			  						@endforeach
			  					@endif
			  			
			  				<div class="col-12">
				  				<div class="form-group">
				  					<input type="hidden" name="id" value="{{ $mission->id }}">
				  					<input type="hidden" name="status" value="{{ $action }}">
			  						<button type="submit" class="btn-default btn-default-sm w-auto float-right">{{ __('mission.send') }}</button>
			  					</div>
			  				</div>

			  			</div>

			  		</form>
		  		</div>
		  	</div>

		  </div>
@endsection

@section('css')
	<link rel="stylesheet" type="text/css" href="/public/css/star-rating-svg.css">
@endsection

@section('js')
<script>
$(function() {

  // basic use comes with defaults values
  $(".star-rating").starRating({
    initialRating: 0.0,
    starSize: 18,
    useGradient: false,
    activeColor: '#F7AA28',
    disableAfterRate: false,
    ratedColor: '#F7AA28',
    starShape: 'rounded',
    callback: function(currentRating, $el){

		$el.next().val(currentRating);
        var summ = Number($('#rating_par_1_'+$el.attr('id')).val()) +
        		   Number($('#rating_par_2_'+$el.attr('id')).val()) +
        		   Number($('#rating_par_3_'+$el.attr('id')).val()) +
        		   Number($('#rating_par_4_'+$el.attr('id')).val());

        $('#complice_'+$el.attr('id')).val(summ / 5);

    }
  });

});
</script>
	<script src="/public/js/jquery.star-rating-svg.min.js"></script>
@endsection
