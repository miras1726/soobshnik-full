@if(!Cookie::has('motivation_' . Auth::user()->id . '_' . $id))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
	  {!! $motivation !!}
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>
@endif