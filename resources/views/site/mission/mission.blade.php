@extends('layouts.main')

@section('title')
  {{ __('mission.missions') }}
@endsection
 
@section('content')

<div class="col-md-8 px-5 py-1 mb-3">
	
	<h4 class="col-12 page-top-title p-0 my-3"><a href="{{ route('mission.list') }}" class="color-black">{{ __('mission.missions') }}</a> > {{ $mission->title }}
		<span class="color-green float-right fs-8 mt-2" title="{{ $mission->description }}">
			<i class="fas fa-info-circle"></i>
		</span>
	</h4> 

	@include('common.alerts') 
 	
	{!! $mission->motivation() !!}

	<div class="col-12 block-white text-left p-4 mb-4">
		<div class="row">
			<div class="col-2">
	            <img src="{{ Croppa::url('public/img/missions/'.$mission->avatar(), 500, 500) }}" class="img-fluid rounded-circle" alt="profile-img">
			</div>
			<div class="col-10 mb-md-0 mb-3">
				<p class="h6 mb-3">{{ $mission->description }}</p>
				<p class="color-gray mb-2">{{ __('mission.completed.time') }} {{ $doneMissions }}</p>
				<p class="color-gray mb-0">{{ __('mission.on.mission') }} {{ $activeMissions }}</p>
				
			</div> 
			<div class="col-md-12 text-right">
			<a href="{{ route('photo-report.mission', [$mission->id]) }}" class="color-green d-inline-block mt-2 ml-3 float-left">{{ __('mission.photo.reports') }}</a>
			@if($userJoined)
				<a href="{{ route('mission.report.get', [$mission->id, 'refuse']) }}" class="mt-1 d-inline-block confirm-required">{{ __('mission.refuse') }}</a>
				<a href="{{ route('mission.report.get', [$mission->id, 'finish']) }}" class="btn-default btn-default-sm float-right w-auto ml-3 confirm-required">{{ __('mission.finish') }}</a>
			@else
				<a href="{{ route('mission.join', [$mission->id]) }}" class="btn-default btn-default-sm float-right w-auto ml-3">{{ __('mission.join') }}</a>
			@endif
			</div> 


		</div>
	</div>

	<mission-post 
				:user_joined="{{ $userJoined }}" 
				:owner="{{ $mission->id }}" 
				:user="{{ Auth::user()->id }}" 
				:type="'mission'"
				:admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}">
	</mission-post>



</div>

	<div class="col-2 block-white-lite p-0 d-md-block d-none">
		<div class="sticky-top side-menu p-4" dir="rtl">
		<div dir="ltr"> 
	  	<h6>{{ __('mission.useful.links') }}</h6>
	  	<hr>

  		<ul class="list-unstyled text-left">
  			@if($mission->links->count() > 0)
	  			@foreach($mission->links as $link)
		  			<li class="mb-2">
		  				<a href="{{ $link->link }}" target="_blank">{{ $link->title }}</a>
		  			</li>
	  			@endforeach
  			@endif
  		</ul>

  		@if($mission->groups->count() > 0)
  		<h6 class="mt-4">{{ __('group.interesting.pages') }}</h6>
	  	<hr>
	  	<div class="row">
	  		
	  		@foreach($groups as $group)
		  		<div class="col-12">
			  		<div class="row">
				  		<div class="col-4">
				  			<img src="{{ Croppa::url('/public/img/groups/'.$group->avatar(), 50, 50) }}" class="img-fluid rounded-circle">
				  		</div>
				  		<div class="col-8 pl-0">
				  			<a href="{{ route('group.single', [$group->id]) }}" class="fs-9">{{ $group->title }}</a>
				  			<p class="fs-8">{{ $group->subscribersCount() .' '. __('group.subscribers') }}</p>
				  		</div>
			  		</div>
		  		</div>
	  		@endforeach

	  		<div class="col-12 mt-3">
	  			<a href="{{ route('group.add.get') }}" class="btn-default btn-default-sm text-center">
	  				{{ __('group.new.group') }}
	  			</a>
	  		</div>
	  	</div>
	  	@endif

	</div>
	</div>
	</div>

@endsection

