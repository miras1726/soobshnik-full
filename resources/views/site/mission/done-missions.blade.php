@extends('layouts.main')

@section('title')
  {{ __('mission.missions') }}
@endsection

@section('content')

<div class="col-md-8 px-5 py-1 mb-3">
	
	<h4 class="col-12 page-top-title my-3 p-0">{{ $title }} {{ __('mission.missions') }}</h4>

	@include('common.alerts') 

	@if($userMissions->count() > 0) 
		@foreach($userMissions as $uMission)
			<div class="col-12 block-white text-left p-4 mt-5">
				<div class="row">
					<div class="col-md-3">
						<p class="h6 mb-3"><a href="{{ route('mission.single', [$uMission->mission->id]) }}" class="color-black">{{ $uMission->mission->title }}</a></p>
						<img src="{{ Croppa::url('public/img/missions/'.$uMission->mission->avatar(), 110, 110) }}" class="img-fluid rounded-circle">
						<p class="note-date mt-3 mb-1">{{ __('mission.started') }} {{ Date::parse($uMission->created_at)->format('j F Y') }}</p>  
						@if($uMission->status != 'active')
							<p class="note-date mb-0"> 
								{{ __('mission.finished') }} 
								{{ Date::parse($uMission->finished_at)->format('j F Y') }}
							</p>
								<!--<a href="{{ route('mission.resume', [$uMission->mission->id]) }}" class="confirm-required color-green">{{ __('mission.resume') }}</a>
								<br>-->
								@if($uMission->status == 'refuse') 
									<a href="{{ route('mission.single', [$uMission->mission->id]) }}" class="confirm-required color-green">{{ __('mission.start.again') }}</a>
									<br>
									<a href="{{ route('mission.resume', [$uMission->id]) }}" class="confirm-required color-green">{{ __('mission.resume') }}</a>
									<br>
								@endif
								<a href="{{ route('mission.archive.get', [$uMission->id]) }}" class="color-gray">{{ __('mission.archive') }}</a> 
						@else
							<a href="{{ route('mission.report.get', [$uMission->mission->id, 'refuse']) }}" class="mt-1 d-inline-block color-gray confirm-required">{{ __('mission.refuse') }}</a>
							<br>
							<a href="{{ route('mission.report.get', [$uMission->mission->id, 'finish']) }}" class="confirm-required color-green">{{ __('mission.finish') }}</a> 
						@endif  
					</div>
					<div class="col-md-6">
						<p class="mb-3 mt-md-0 mt-4">{{ __('mission.review') }}</p>
						<p class="color-gray">{{ $uMission->review }}</p>
					</div>
					<div class="col-md-3">
						<p class="mb-3">{{ __('mission.complices') }}</p>
						<div class="col-12">
							<div class="row">

								@if($uMission->authUserComplices()->count() > 0)
									@foreach($uMission->authUserComplices() as $complice)
										<div class="col-12 mb-1">
											<div class="row">
												<div class="col-4 p-1">
													<img src="{{ Croppa::url('public/img/users/'.$complice->user->avatar(), 60, 60) }}" class="img-fluid rounded-circle">
												</div>
												<div class="col-8 px-2">
													<p class="h6 my-1">
														<a href="#" class="profile-name-link">{{ $complice->user->nickname }}</a>
													</p>
													<p class="profile-rating mb-0">
														<div class="star-rating-readonly" data-rating="{{ $complice->getCompliceRating() }}"></div>
													</p>
												</div>
											</div>
										</div>
									@endforeach
								@endif

							</div>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	@endif

</div>



@endsection

