@extends('layouts.main')

@section('title')
  {{ __('mission.missions') }}
@endsection

@section('content')

<div class="col-md-8 px-5 py-1">
	
	<h4 class="page-top-title my-3">{{ __('mission.missions') }}</h4>
	 
	<div class="col-12 d-md-block d-none p-4">
		<div class="row"> 
			<div class="col-md-3">
				<img src="/public/img/health.png" class="img-fluid" width="120" height="auto" alt="health">
			</div>
			<div class="col-md-3">
				<img src="/public/img/study.png" class="img-fluid" width="120" height="auto" alt="study">
			</div>
			<div class="col-md-3">
				<img src="/public/img/panda.png" class="img-fluid" width="120" height="auto" alt="panda">
			</div>
			<div class="col-md-3">
				<img src="/public/img/more.png" class="img-fluid" width="120" height="auto" alt="more">
			</div>
		</div>
	</div>
	
	<div class="col-12 block-white p-4 text-left">
		<div class="row">
			@if($categories->count() > 0)
				@foreach($categories as $category)
					<div class="col-md-3 mt-md-0 mt-4">
						<p class="mission-list-header">{{ $category->title }}</p>
						@if($category->missions->count() > 0)
							@foreach($category->missions as $mission)
								<p class="mb-1"><a href="{{ route('mission.single', [$mission->id]) }}">{{ $mission->title }}</a></p>
							@endforeach
						@endif
					</div>
				@endforeach
			@endif

		</div>
	</div>
</div>
@endsection

