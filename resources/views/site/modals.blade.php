<div class="modal" tabindex="-1" role="dialog" id="reported-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ __('modal.report.sent') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <i class="fas fa-check-circle fa-7x color-green"></i>
        <p class="mt-3">{{ __('modal.report.sent.text') }}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn-sm" data-dismiss="modal">{{ __('modal.close') }}</button>
      </div>
    </div>
  </div>
</div>
 
<div class="modal" tabindex="-1" role="dialog" id="badword-warning-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ __('modal.badword.modal.title') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <i class="fas fa-ban fa-7x color-red"></i>
        <p class="mt-3">{{ __('modal.badword.modal.body') }}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn-sm" data-dismiss="modal">{{ __('modal.close') }}</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="shared-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ __('modal.shared.title') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <i class="fas fa-check-circle fa-7x color-green"></i>
        <p class="mt-3">{{ __('modal.shared.text') }}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn-sm" data-dismiss="modal">{{ __('modal.close') }}</button>
      </div>
    </div>
  </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="note-share-modal">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-sm">
      <div class="modal-body text-center">
        <note-share-component></note-share-component>  
      </div>
    </div>
  </div>
</div>