@extends('layouts.main')

@section('title')
  @lang('adavai.fancies') > @lang('adavai.organizer.photo.reports')
@endsection

@section('content')
			<!--@todo translate-->
		  <div class="col-md-8 px-md-5 px-3 py-1 profile-block">
		  	<h4 class="page-top-title my-3">@lang('adavai.fancies') > @lang('adavai.organizer.photo.reports')</h4>

			<photo-report  
						:owner="{{ Auth::user()->id }}" 
						:user="{{ Auth::user()->id }}" 
						:type="'user'"
						:author_id="{{ $user->id }}"
						:admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}">
			</photo-report>
			
		  </div>

@endsection

