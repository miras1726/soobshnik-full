@extends('layouts.main')

@section('title')
  Адавай
@endsection

@section('content')
<!--@todo translate-->
<div class="col-md-8 px-5 py-1 mb-3">
	
		
		<div class="col-12">
			<div class="row">
				<h4 class="col-8 page-top-title p-0 my-3 float-left">{{ __('adavai.fancy') }}</h4>
				
				<div class="col-4 my-3 p-0 d-md-none d-block">
					<a href="{{ route('adavai.list') }}" class="btn-default btn-default-sm mb-1">{{ __('adavai.participant') }}</a>
				</div>
			</div>
		</div>	

	<div class="clearfix"></div>

	@include('common.alerts')
	@include('common.errors')


        <div class="col-12 block-white p-4 text-left">
          <div class="row">
            <div class="col-md-4">
              <div class="profile-img img-caption-wrap text-center">
              	<span class="d-block font-light h6 text-left mb-3">{{ __('adavai.organizer.profile') }}</span>
                @if($user->isAvatarExists())
                  <a href="/public/img/users/{{ $user->avatar() }}" data-fancybox="">
                    <img src="{{ Croppa::url('/public/img/users/'.$user->avatar(), 500, 500) }}" class="img-fluid" alt="profile-img">
                  </a>
                @else
                  <img src="/public/img/users/{{ $user->defaultAvatar }}" class="img-fluid" alt="profile-img">
                @endif  
                
              </div> 
			  <div class="profile-missions my-2">
                <a href="{{ route('adavai.user.reports', $user->id) }}" class="color-green d-block font-medium">{{ __('mission.photo.reports') }}</a>
                @if(!$own)
                	<a href="{{ route('private-chat', [$user->id]) }}" class="color-green d-block font-medium">{{ __('adavai.organizer.contact') }}</a>
              	@endif 
              </div>
            </div>

 
            <div class="col-md-8">
              <div class="row">
                <div class="col-lg-11 col-md-11">
                  <span class="profile-name d-block mt-4">{{ $user->nickname }} </span>
                  @if($user->awards)
                    @foreach($user->awards as $award)
                      <small class="badge badge-warning" style="font-weight: 100;"><i class="fas fa-medal"></i> {{ $award->name }}</small>
                    @endforeach
                  @endif
                  <div class="col-12 p-0 profile-data">
                    <table class="table table-sm table-borderless w-auto pt-2 d-block">
                      <tr>
                        <td>{{ __('user.name') }}:</td>
                        <td>{{ $user->name }}</td>
                      </tr>
                      <tr>
                        <td>{{ __('adavai.organizer.rating') }}:</td>
                        <td>
                        	<div class="star-rating-readonly-gray" 
                        	data-rating="{{ $user->adavaiProfile ? $user->adavaiProfile->rating : 0 }}">
                        	</div>
                        </td>
                      </tr>
                      <tr>
                        <td>{{ __('user.gender') }}:</td>
                        <td>
                          @if($user->gender == 'male')
                            {{ __('user.male') }}
                          @elseif($user->gender == 'female')
                            {{ __('user.female') }}
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>{!! __('user.born') !!}:</td>
                        <td>{{ ($user->born) ? Date::parse($user->born)->format('j F Y') : '' }}</td>
                      </tr>
                      <tr>
                        <td>{!! __('user.registration.date') !!}:</td>
                        <td>{{ Date::parse($user->created_at)->format('j F Y') }}</td>
                      </tr>
                      <tr>
                        <td>{{ __('user.city') }}:</td>
                        <td>{{ ($user->city) ? $user->userCity->title : '' }}</td>
                      </tr>
                      <tr>
                        <td>{{ __('user.info') }}:</td>
                        <td>{{ $user->info }}</td>
                      </tr>
                      <tr>
                        <td>{{ __('group.interesting.pages') }}:</td>
                        <td>
                        	@if($user->createdGroups->count() > 0)
	                        	@foreach($user->createdGroups as $group)
	                        		<a href="{{ route('group.single', $group->id) }}" target="_blank">{{ $group->title }}</a>@if(!$loop->last),@endif
	                        	@endforeach
                        	@endif
                    	</td>
                      </tr>
                    </table> 
                  </div>
                </div>
                <div class="col-lg-1 d-lg-block d-md-none text-right">
		            @if($own)
		              <a href="{{ route('adavai.profile.settings') }}" class="profile-settings">
		                <i class="fas fa-cog"></i>
		              </a>
		              <a href="{{ route('adavai.user.bank') }}" class="profile-settings mt-2 ml-md-0 ml-3">
		            	<i class="fas fa-wallet"></i>
		            </a>
		            @endif
                </div>
              </div>
            </div>

          </div>

        </div>

	<adavai-notes :owner="0" 
	 			  :user="{{ Auth::user()->id }}" 
	 			  :user_city_id="{{ Auth::user()->city ?? 0 }}" 
	 			  :type="'adavai'" 
		 		  :adavai-profile-id="{{ $user->id }}" 
		 		  :admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}"
		 		  :adavai_profile="{{ 1 }}"
		 		  >
	</adavai-notes>



</div>

@if($own)
<div class="col-md-2 block-white-lite p-0 d-md-block d-none">
	<div class="sticky-top side-menu p-4" dir="rtl">
	<div dir="ltr">
		
		<div class="col-12 mb-2 adavai-switch">
			<a href="{{ route('adavai.profile') }}" class="float-left color-black active">{{ __('adavai.organizer') }}</a>
			<a href="{{ route('adavai.list') }}" class="float-right color-black">{{ __('adavai.participant') }}</a>
			<div class="clearfix"></div>
		</div>

	  	<h6 class="font-light mt-3">{{ __('adavai.add.fancy') }}</h6>
	  	<hr>

	  	<div class="row">
			<form action="{{ route('adavai.store') }}" class="w-100" method="post">
				@csrf
	  		<div class="col-12">	
				<div class="form-group"> 
					<label>{{ __('adavai.subject') }}:</label>
					<select class="form-control" name="category_id" required>
						@if($categories->count() > 0)
							@foreach($categories as $cat)
								<option value="{{ $cat->id }}">{{ $cat->title }}</option>
							@endforeach
						@endif
					</select>
				</div>
	  		</div> 

	  		<div class="col-12">
				<div class="form-group"> 
					<label>{{ __('adavai.city') }}:</label>
					<select class="form-control" name="city_id" required>
						@if($countries->count() > 0)
							@foreach($countries as $country)
							<optgroup label="{{ $country->title }}"></optgroup>
								@if($country->cities->count() > 0)
									@foreach($country->cities as $city)
										<option value="{{ $city->id }}" {{ (Auth::user()->city == $city->id) ? 'selected' : '' }}>{{ $city->title }}</option>
									@endforeach
								@endif
							@endforeach
						@endif
					</select>
				</div>
	  		</div>

	  		<div class="col-12">
				<div class="form-group"> 
					<label>{{ __('adavai.date') }}:</label>
					<input type="datetime-local" name="date" class="form-control" value="{{ old('date') }}" required>
				</div>
	  		</div>

	  		<div class="col-12">
				<div class="form-group"> 
					<label>{{ __('adavai.participants.amount') }}:</label>
					<input type="number" name="limit" class="form-control" min="1" value="{{ old('limit') }}" required>
				</div>
	  		</div>

	  		<div class="col-12">
	  			<div class="form-group">
                     <label class="float-left">{{ __('adavai.age.restrictions') }}</label>
                     <label class="checkbox float-right">
                        <input type="checkbox" name="check_age" value="1">
                        <span class="checkmark"></span>
                    </label>
                    <div class="clearfix"></div>
                </div>
	  		</div>

	  		<div class="col-12">
				<div class="form-group"> 
					<label>{{ __('adavai.description') }}:</label>
					<textarea class="form-control" name="content" required>{{ old('content') }}</textarea>
				</div>
	  		</div>


	  		<div class="col-12 mt-3">
	  			<button type="submit" class="btn-default btn-default-sm text-center">{{ __('adavai.add.fancy') }}</button>
	  		</div>
	  		</form>
		</div>
	  	



	  

	</div>
	</div>
</div>
@endif

@endsection
