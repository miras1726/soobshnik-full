@extends('layouts.main')

@section('title')
  @lang('adavai.fancies') > @lang('mission.photo.reports')
@endsection

@section('content')
		  <div class="col-md-8 px-md-5 px-3 py-1 profile-block">
		  	<h4 class="page-top-title my-3">@lang('adavai.fancies') > @lang('mission.photo.reports')</h4>

			<photo-report 
						:owner="{{ $note->id }}" 
						:user="{{ Auth::user()->id }}" 
						:type="'adavai-report'"
						:user_joined="{{ Auth::user()->id == $note->author_id ? 1 : 0 }}"
						:admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}">
			</photo-report>
			
		  </div>



@endsection 

