 @extends('layouts.main')

@section('title')
  Адавай
@endsection

@section('content')

<div class="col-md-8 px-5 py-1 mb-3">
	
		<div class="col-12">
			<div class="row">
				
			<h4 class="col-8 page-top-title p-0 my-3 float-left">{{ __('adavai.fancy') }}</h4>
				<div class="col-4 my-3 p-0 d-md-none d-block">
					<a href="{{ route('adavai.profile') }}" class="btn-default btn-default-sm mb-1">
					{{ __('adavai.organizer') }}</a>
				</div>
			</div>
		</div>

	<div class="clearfix"></div>

	@include('common.alerts')

	<div class="col-12 block-white text-left p-4 mb-4">
		{!! __('adavai.text') !!}
	</div>


	<adavai-notes :owner="0" 
	 			  :user="{{ Auth::user()->id }}" 
	 			  :user_city_id="{{ Auth::user()->city ?? 0 }}" 
	 			  :type="'adavai'" 
		 		  :admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}"
		 		  :adavai_profile="{{ 0 }}">
	</adavai-notes>



</div>

<div class="col-md-2 block-white-lite p-0 d-md-block d-none">
	<div class="sticky-top side-menu p-4" dir="rtl">
	<div dir="ltr">
		
		<div class="col-12 mb-2 adavai-switch">
			<a href="{{ route('adavai.profile') }}" class="float-left color-black">{{ __('adavai.organizer') }}</a>
			<a href="{{ route('adavai.list') }}" class="float-right color-black active">{{ __('adavai.participant') }}</a>
			<div class="clearfix"></div>
		</div>

	  	<h6 class="font-light ">{{ __('adavai.current.adavais') }}</h6>
	  	<hr>

	  	@if($activeAdavai->count() > 0)
		  	@foreach($activeAdavai as $note)
			  	<div class="mt-3">
			  		<p class="mb-1"><a href="{{ route('adavai.list', ['note' => $note->id]) }}" class="color-black">{{ $note->content_short }}</a></p>
			  		<span class="color-green mr-3">
		        		<i class="far fa-star color-gray"></i> 
		        		<span class="note-action-badge">
		      				{{ $note->likes->count() }}
		    			</span>
		    		</span> 
		    		<span class="color-green mr-3">
		    			<i class="far fa-comment-alt"></i> 
		    			<span class="note-action-badge">
		    				{{ $note->comments->count() }}
		    			</span>
		    		</span>
		    		<span class="color-green ">
		    			<i class="fas fa-user"></i>
		    			<span class="note-action-badge">
		      				{{ $note->adavaiki->count() }}
		    			</span> 
		    		</span> 
			  	</div>
			  	<div>
					<small class="color-gray">{{ $note->adavai->city->country->title }}, {{ $note->adavai->city->title }}</small><br>
					<small class="color-gray">{{ $note->adavai->date_formatted }}</small>
				</div>
				<div>
					<a href="{{ route('adavai.reports', $note->id) }}" class="btn-default btn-default-sm mb-1 mt-1">{{ __('mission.photo.reports') }}</a>
					<a href="{{ route('adavai.leave', $note->id) }}" class="btn-default btn-default-sm mb-1">{{ __('adavai.refuse') }}</a>
					
				</div>
			@endforeach
		@endif

		<h6 class="font-light mt-3">{{ __('adavai.finished.adavais') }}</h6>
	  	<hr>

	  	@if($inactiveAdavai->count() > 0)
		  	@foreach($inactiveAdavai as $note)
			  	<div class="mt-3">
			  		<p class="mb-1"><a href="{{ route('adavai.list', ['note' => $note->id]) }}" class="color-black">{{ $note->content_short }}</a></p>
			  		<span class="color-green mr-3">
		        		<i class="far fa-star color-gray"></i> 
		        		<span class="note-action-badge">
		      				{{ $note->likes->count() }}
		    			</span>
		    		</span> 
		    		<span class="color-green mr-3">
		    			<i class="far fa-comment-alt"></i> 
		    			<span class="note-action-badge">
		    				{{ $note->comments->count() }}
		    			</span>
		    		</span>
		    		<span class="color-green ">
		    			<i class="fas fa-user"></i>
		    			<span class="note-action-badge">
		      				{{ $note->adavaiki->count() }}
		    			</span> 
		    		</span>
			  	</div>
			  	<div>
					<small class="color-gray">{{ $note->adavai->city->country->title }}, {{ $note->adavai->city->title }}</small><br>
					<small class="color-gray">{{ $note->adavai->date_formatted }}</small>
				</div>
				<div>
					<a href="{{ route('adavai.reports', $note->id) }}" target="_blank" class="btn-default btn-default-sm mb-1 mt-1">{{ __('mission.photo.reports') }}</a> 
					<a href="{{ route('adavai.leave', $note->id) }}" class="btn-default btn-default-sm mb-1">{{ __('adavai.delete') }}</a>
				</div>
			@endforeach
		@endif
	</div>
	</div>
</div>

@endsection
