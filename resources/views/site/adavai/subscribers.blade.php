 @extends('layouts.main')

@section('title') 
  @lang('adavai.participants.list')
@endsection
@section('content') 
<!--@todo translate-->

<div class="col-md-8 px-5 py-1 mb-3">
	
	<h4 class="col-12 page-top-title my-3">@lang('adavai.participants.list')</h4>
	
	<div class="col-12">
		@include('common.alerts')
	</div>
	
	<div class="col-12">
		<div class="row"> 
			@if($users->count() > 0)
				@foreach($users as $user)
					<div class="col-md-4 mb-4">
						<div class="col-12 block-white user-list-item text-center p-3">
						
						<img src="{{ Croppa::url('public/img/users/'.$user->avatar(), 120, 120) }}" class="rounded-circle">
						<p class="h5 mb-0 mt-1">
							<a href="{{ route('userProfile', $user->nickname) }}" class="profile-name-link" data-toggle="tooltip" data-html="true" title="{{ $user->short_info }}">{{ $user->nickname }}</a>
						</p> 
						<p class="profile-rating mt-1 mb-2">
							<div class="star-rating-readonly" data-rating="{{ $user->rating }}"></div>
						</p>
						
						</div>
					</div>
				@endforeach
			@endif
		</div>
	</div>
</div>

@endsection

