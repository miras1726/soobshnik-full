@extends('layouts.main')

@section('title')
  {{ __('user.profile.settings') }}
@endsection

@section('content') 

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">{{ __('user.profile.settings') }}</h4>

		  	@include('common.errors')
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-md-8 col-12">
			  		<form action="{{ route('adavai.profile.settings.update') }}" class="table-form" method="post" enctype="multipart/form-data">
			  			@csrf
			  			@method('PUT')
			  		<table class="table table-sm table-borderless">
						<tr>
							<td class="table-form-label">{{ __('adavai.profile.full.address') }}:</td>
							<td class="form-group">
								<label class="d-block d-md-none">{{ __('adavai.profile.full.address') }}</label>
						        <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ $user->adavaiProfile->full_address }}">

						        @if ($errors->has('address'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('address') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						@if($user->adavaiProfile->passportExists())
						<tr>
							<td class="table-form-label"></td>
							<td class="form-group">
								<a href="{{ $user->adavaiProfile->getPassport() }}" data-fancybox="">
									<img src="{{ $user->adavaiProfile->getPassport() }}" width="100" height="auto">
								</a>
							</td>
						</tr>
						@endif
						<tr>
							<td class="table-form-label">{{ __('adavai.profile.passport') }}:</td>
							<td class="form-group">
								<label class="d-block d-md-none mt-3">{{ __('adavai.profile.passport') }}</label>
						        <input id="passport" type="file" class="form-control{{ $errors->has('passport') ? ' is-invalid' : '' }}" name="passport">

						        @if ($errors->has('passport'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('passport') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('adavai.profile.enable.notifications') }}</td>
							<td class="form-group">
								<label class="d-block d-md-none mt-3">{{ __('adavai.profile.enable.notifications') }}</label>
								<div class="custom-control custom-switch d-inline-block">
								  <input type="checkbox" 
								  		 class="custom-control-input" 
								  		 id="enable_notifications"
								  		 name="enable_notifications"
								  		 value="1"
								  		 {{ $user->adavaiProfile->enable_notifications ? 'checked' : '' }} 
								  		 >
								  <label class="custom-control-label" for="enable_notifications"></label>
								</div>
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('adavai.profile.enable.auto.prolong') }}</td>
							<td class="form-group">
								<label class="d-block d-md-none mt-3">{{ __('adavai.profile.enable.auto.prolong') }}</label>
								<div class="custom-control custom-switch d-inline-block">
								  <input type="checkbox" 
								  		 class="custom-control-input" 
								  		 id="auto_prolong"
								  		 name="auto_prolong"
								  		 value="1"
								  		 {{ $user->adavaiProfile->auto_prolong ? 'checked' : '' }} 
								  		 >
								  <label class="custom-control-label" for="auto_prolong"></label>
								</div>
							</td>
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<button type="submit" class="btn-default w-auto">{{ __('user.save') }}</button>
							</td>
						</tr>
					</table> 
			  		</form>
		  		</div>
		  	</div>

		  </div>
@endsection
