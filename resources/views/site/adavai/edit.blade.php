@extends('layouts.main')

@section('title')
	{{ __('adavai.edit') }}
@endsection
<!--@todo translate-->
@section('content') 

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">{{ __('adavai.edit') }}</h4>

		  	@include('common.alerts')
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-md-8 col-12">
			  		<form action="{{ route('adavai.update', $note->id) }}" class="table-form" method="post">
			  			@csrf
			  			@method('PUT')
			  		<table class="table table-sm table-borderless">
						<tr>
							<td class="table-form-label">{{ __('adavai.subject') }}:</td>
							<td class="form-group">
						        <select class="form-control" name="category_id" required>
									@if($categories->count() > 0)
										@foreach($categories as $cat)
											<option value="{{ $cat->id }}" 
												{{ $note->adavai->category_id == $cat->id ? 'selected' : '' }}
												>{{ $cat->title }}</option>
										@endforeach
									@endif
								</select>
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('adavai.city') }}:</td>
							<td  class="form-group">
						        <select class="form-control" name="city_id">
									@if($countries->count() > 0)
										@foreach($countries as $country)
										<optgroup label="{{ $country->title }}"></optgroup>
											@if($country->cities->count() > 0)
												@foreach($country->cities as $city)
													<option value="{{ $city->id }}" {{ ($note->adavai->city_id == $city->id) ? 'selected' : '' }}>{{ $city->title }}</option>
												@endforeach
											@endif
										@endforeach
									@endif
								</select>
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('adavai.date') }}:</td>
							<td  class="form-group">
								<input type="datetime-local" name="date" class="form-control" value="{{ $note->adavai->date_for_input }}" required>
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('adavai.participants.amount') }}:</td>
							<td  class="form-group">
								<input type="number" name="limit" class="form-control" min="1" value="{{ $note->adavai->limit }}" required>
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('adavai.age.restrictions') }}:</td>
							<td  class="form-group">
								<label class="checkbox float-left mt-1">
			                        <input type="checkbox" name="check_age" value="1" {{ $note->adavai->check_age ? 'checked' : '' }}>
			                        <span class="checkmark"></span>
			                    </label>
			                    <div class="clearfix"></div>
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('adavai.description') }}:</td>
							<td  class="form-group">
						        <textarea type="text" class="form-control" name="content">{{$note->content}}</textarea>
							</td>
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<button type="submit" class="btn-default w-auto">{{ __('adavai.add.fancy') }}</button>
							</td>
						</tr>
					</table> 
			  		</form>
		  		</div>
		  	</div>

		  </div>
@endsection

