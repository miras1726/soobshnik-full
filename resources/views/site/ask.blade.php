 @extends('layouts.main')

@section('title')
  {{ __('ask.ask') }}
@endsection

@section('content') 

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">{{ __('ask.ask') }}</h4>
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		@if (session('message'))
                  <div class="alert alert-success" role="alert">
                      {{ session('message') }}
                  </div>
                @endif
		  		<div class="col-md-8 col-12">
		  	        <form action="{{ route('app.ask.post') }}" class="table-form" method="post">
			  			@csrf
			  		<table class="table table-sm table-borderless">
						<tr>
							<td class="table-form-label">{{ __('ask.subject') }}:</td>
							<td  class="form-group">
						        <select class="form-control" name="subject" id="subject" required>
									<option value="Техническая поддержка">{{ __('ask.tech.support') }}</option>
									<option value="Верификация (e-mail и мобильный телефон)">{{ __('ask.verification') }}</option>
									<option value="Пожелания по улучшению работы сайта">{{ __('ask.wishes') }}</option>
									<option value="Другая причина">{{ __('ask.other') }} </option>
								</select>

						        @if ($errors->has('subject'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('subject') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('ask.text') }}:</td>
							<td  class="form-group">
						        <textarea id="body" class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" required></textarea>

						        @if ($errors->has('body'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('body') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<button type="submit" class="btn-default btn-default-sm w-auto">{{ __('ask.send') }}</button>
							</td>
						</tr>
					</table> 
			  		</form>
		  		</div>
 
		  	</div>

		  	<h4 class="page-top-title my-3">{{ __('ask.my.questions') }}</h4>

		  		<div class="col-12 block-white text-left p-4 mb-4">
		  			@if($questions->count() > 0)
					<div class="list-group">
			  			@foreach($questions as $question)
						  <div class="list-group-item list-group-item-action">
						    <div class="d-flex w-100 justify-content-between">
						      <h5 class="mb-1">
						      		<i class="fa fa-circle {{ ($question->answer) ? 'color-green' : 'color-gray'}} fs-8"></i>
						      	{{ $question->subject }}
						      </h5>
						      <small>{{ $question->date() }}</small>
						    </div>
						    <p class="mb-1">{{ $question->text }}</p>
						    @if($question->answer)
						    	<hr>
						    	<p class="mb-1 color-green fs-9">{{ $question->answer }}</p>
						    @endif
						  </div>
			  			@endforeach
					</div>
					@endif
		  		</div>

		  		<div id="pagination">
				  {{ $questions->links() }}
				</div>


		  </div>
@endsection
