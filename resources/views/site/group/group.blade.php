@extends('layouts.main')

@section('title')
  {{ $group->title }}
@endsection

@section('content')

<div class="col-md-8 px-5 py-1 mb-3">
	
	<h4 class="col-12 page-top-title p-0 my-3">{{ __('group.interesting.pages') }}</h4>
	
	<div class="col-12 block-white text-left p-4 mb-4">
		<div class="row">
			<div class="col-2">
	            <img src="{{ Croppa::url('/public/img/groups/'.$group->avatar(), 500, 500) }}" class="img-fluid rounded-circle" alt="profile-img">
			</div>
			<div class="col-10 mb-md-0 mb-3">
				<p class="h6 mb-3">{{ $group->title }}</p>

				<p class="color-gray mb-2">{{ $group->description }}</p>
			</div>
			<div class="col-md-12">
				<p class="fs-9 color-gray float-left mb-0 mt-2 ml-3">{{ $group->subscribersCount() }} {{ __('group.subscribers') }}</p>
				@if($userJoined)
					<a href="{{ route('group.leave', [$group->id]) }}" class="btn-default btn-default-sm float-right w-auto">{{ __('group.unsubscribe') }}</a>
				@else
					<a href="{{ route('group.join', [$group->id]) }}" class="btn-default btn-default-sm float-right w-auto">{{ __('group.subscribe') }}</a>
				@endif
		
			</div> 


		</div>
	</div>

	<group-post :owner="{{ $group->id }}" 
				:user="{{ Auth::user()->id }}" 
				:type="'group'" 
				:group="{{ $group }}"
				:is_admin="{{ $group->isAdmin() }}"
				:admin="{{ (Auth::user()->isAdmin() == true) ? 1 : 0 }}"
				>				
	</group-post>

 

</div>


@endsection

