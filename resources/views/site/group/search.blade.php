@extends('layouts.main')

@section('title')
  {{ __('group.interesting.pages') }}
@endsection

@section('content')

<div class="col-md-9 px-5 py-1 mb-3">
	

	<div class="col-12 my-3">
		<div class="row">
			<div class="col-6">
				<h4 class="page-top-title">{{ __('group.interesting.pages') }} </h4>
			</div>
			<div class="col-6 text-right">
				<ul class="nav justify-content-end heading-links">
              		<li class="nav-item">
						<a class="nav-link mt-2" data-toggle="collapse" href="#collapseNoteSearch" role="button" aria-expanded="false" aria-controls="collapseNoteSearch">
                  			{{ __('note.search') }}
                		</a>
                	</li> 

              		<li class="nav-item">
						<a href="{{ route('group.add.get') }}" class="nav-link float-right mt-2">{{ __('group.create') }}</a>
                	</li> 
				</ul>
			</div>
		</div>
	</div> 
	 
	<div class="col-12">
			<div class="row justify-content-end">
				<div class="col-4 collapse" id="collapseNoteSearch">
					<form action="{{ route('group.search') }}" method="get">
						@csrf
						<input type="text" name="search" class="form-control search-input-rounded mt-0 mb-3" placeholder="{{ __('note.search') }}">
					</form>
				</div>
			</div>
		<div class="row">
			@if($groups->count() > 0)
				@foreach($groups as $group)
					<div class="col-md-3 mb-4">
						<div class="col-12 block-white user-list-item text-center py-3 px-4">
						
							<img src="{{ Croppa::url('/public/img/groups/'.$group->avatar(), 120, 120) }}" class="rounded-circle">
							<p class="mb-0 mt-2">
								<a href="{{ route('group.single', [$group->id]) }}" class="link-black fixed-h">
									{{ $group->title }}
								</a>
							</p>
							<p class="fs-9 mt-2 mb-1 color-gray">
								{{ $group->subscribersCount() }} {{ __('group.subscribers') }}
							</p>
							@if($group->isAdmin())
								<a href="{{ route('group.edit.get', [$group->id]) }}" class="fs-9">{{ __('group.manage') }}</a>
							@endif
						</div>
					</div>
				@endforeach
			@else
			<div class="col">
				<h2 class="font-light"><i class="far fa-frown-open"></i> {{ __('group.nothing.found') }}</h2>
			</div>
			@endif

		</div>
	</div>
</div>
@endsection

