@extends('layouts.main')

@section('title')
  {{ __('group.edit') }}
@endsection

@section('content')

		  <div class="col-md-8 px-5 py-1 profile-block">
		  	
		  	<h4 class="page-top-title my-3">{{ __('group.edit') }}</h4>
		  	
		  	<div class="col-12 block-white px-md-5 px-3 py-4 text-left">
		  		<div class="col-md-8 col-12">
			  		<form action="{{ route('group.edit.post') }}" class="table-form" method="post" enctype="multipart/form-data">
			  			@csrf
			  		<table class="table table-sm table-borderless">
						<tr>
							<td class="table-form-label">{{ __('group.title') }}:</td>
							<td class="form-group">
						        <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ $group->title }}" required>

						        @if ($errors->has('title'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('title') }}</span>
						            </span>
						        @endif
							</td>
						</tr>
						<tr>
							<td class="table-form-label">{{ __('group.description') }}:</td>
							<td  class="form-group">
						        <textarea id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description">{{ $group->description }}</textarea>

						        @if ($errors->has('description'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('description') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label">{{ __('group.image') }}:</td>
							<td  class="form-group">
								
								<img src="{{ Croppa::url('/public/img/groups/'.$group->avatar(), 120, 120) }}" class="img-fluid mb-2">

						        <input type="file" name="image" id="image" class="h-auto form-control{{ $errors->has('image') ? ' is-invalid' : '' }}">

						        @if ($errors->has('image'))
						            <span class="invalid-feedback" role="alert">
						                <span>{{ $errors->first('image') }}</span>
						            </span>
						        @endif
							</td> 
						</tr>
						<tr>
							<td class="table-form-label"></td>
							<td>
								<input type="hidden" name="id" value="{{ $group->id }}">
								<button type="submit" class="btn-default btn-default-sm w-auto">{{ __('group.send') }}</button>
								<a href="{{ route('group.delete', [$group->id]) }}" class="color-gray d-block mt-3 confirm-required">{{ __('group.delete') }}</a>
							</td>
						</tr>
					</table>
			  		</form>
		  		</div>
		  	</div>

		  </div>
@endsection

@section('js')
{!! JsValidator::formRequest('App\Http\Requests\SettingsRequest') !!}
@endsection