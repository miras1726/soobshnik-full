 @extends('layouts.main')

@section('title')
  {{ __('product.my.products') }}
@endsection

@section('content')

<div class="col-md-9 px-5 py-1 mb-3">
	

	<div class="col-12 my-3">
		<div class="row">
			<div class="col-6">
				<h4 class="page-top-title">{{ __('product.my.products') }}</h4>
			</div>
			<div class="col-6 text-right">
				<span class="d-block mt-3">
					{{ Auth::user()->balance }} <i class="text-warning fas fa-star"></i>
				</span>
			</div>
		</div> 
	</div>
	
	<div class="col-12">
		@include('common.alerts')
	</div>
	
	<div class="col-12">
		<div class="row">
 
			@if($orders->count() > 0)
				@foreach($orders as $order)
				@if($order->product) 
					<div class="col-md-3 mb-4">
						<div class="col-12 block-white user-list-item text-center py-3 px-4">
						
							<img src="{{ Croppa::url('/public/img/products/'.$order->product->avatar(), 120, 120) }}" class="rounded-circle">
							<p class="mb-2 mt-2">
									{{ $order->product->title }}
								</p>
								<span>{{ $order->product->price }} <i class="text-warning fas fa-star"></i></span>
								<br>
								<span>{{ $order->order_date }}</span>
						</div>
					</div>
				@endif 
				@endforeach
			@endif 
		</div>
	</div>
</div>
@endsection

