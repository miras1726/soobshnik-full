 @extends('layouts.main')

@section('title')
  {{ __('product.shop') }}
@endsection

@section('content')

<div class="col-md-9 px-5 py-1 mb-3">
	

	<div class="col-12 my-3">
		<div class="row">
			<div class="col-6">
				<h4 class="page-top-title">{{ __('product.shop') }}</h4>
			</div>
			<div class="col-6 text-right">
				<span class="d-block mt-3">
					<span>
						{{ Auth::user()->money }} @lang('user.roubles')
					</span>
					
					<span class="ml-2">
						{{ Auth::user()->balance }} <i class="text-warning fas fa-star"></i>
					</span>
				</span>
			</div>
		</div> 
	</div>
	
	<div class="col-12">
		@include('common.alerts')
	</div>
	
	<div class="col-12">
		<div class="row">
 
			@if($products->count() > 0)
				@foreach($products as $product)
					<div class="col-md-3 mb-4">
						<div class="col-12 block-white user-list-item text-center py-3 px-4">
						
							<img src="{{ Croppa::url('/public/img/products/'.$product->avatar(), 120, 120) }}" class="rounded-circle">
							<p class="mb-2 mt-2">
									{{ $product->title }}
								</p>
								<span>
									{{ $product->price }} @lang('user.roubles')
								</span>
								|
								<span>
									{{ $product->price_star }} <i class="text-warning fas fa-star"></i>
								</span>

								<a href="{{ route('product.buy', [$product->id]) }}" class="btn-default btn-default-sm d-block mt-2 product-buy-btn"  data-toggle="modal" data-target="#productBuyModal">
									{{ __('product.buy') }}
								</a>
						</div>
					</div>
				@endforeach
			@endif 

			<div class="col-12 mt-3" id="pagination">
				{{ $products->links() }}
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="productBuyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <form action="{{ route('product.buy', 1) }}" method="get" id="product_buy_form">
        	<div class="form-group">
        		<button name="balance" value="1" type="submit" class="btn-default btn-default-sm">@lang('product.spend.stars')</button>
        	</div>
        	<div class="form-group m-0">
        		<button name="money" value="1" type="submit" class="btn-green-lite w-100">@lang('product.spend.money')</button>
        	</div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	$('.product-buy-btn').click(function(){
		var link = $(this).attr('href');
		$('#product_buy_form').attr('action', link);
	});
</script>
@endsection