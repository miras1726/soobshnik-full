@extends('layouts.app')

@section('title')
  {{ __('auth.register') }}
@endsection

@section('main')

    <div class="container"> 
      <div class="row justify-content-md-center my-5">  
        <div class="col-md-4 block-white px-5 py-4">
          <div class="px-2">
            <img src="/public/img/logo.png" class="img-fluid my-3" alt="logo">
            <p class="auth-title text-left">{{ __('auth.register') }}:</p>
            @include('common.errors')
            <form class="text-left" action="{{ route('register') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="nickname">{{ __('auth.nickname') }}:</label>
                <input id="nickname" type="text" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}" name="nickname" value="{{ old('nickname') }}" required autofocus>

                @if ($errors->has('nickname'))
                    <span class="invalid-feedback" role="alert"> 
                        <span>{{ $errors->first('nickname') }}</span>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="">{{ __('auth.email.post') }}:</label>
                 <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <span>{{ $errors->first('email') }}</span>
                        </span>
                    @endif
            </div>
            <div class="form-group">
                <label for="">{{ __('auth.password') }}:</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <span>{{ $errors->first('password') }}</span>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="">{{ __('auth.password.one.more') }}:</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
            <div class="form-group">
              <label for="captcha">{{ __('auth.captcha') }}:</label>
              <input class="form-control" id="captcha" name="captcha" required>
              @if ($errors->has('captcha'))
                  <span class="invalid-feedback" role="alert">
                      <span>{{ $errors->first('captcha') }}</span>
                  </span>
              @endif
            </div>
            <div style="text-align: center;">
              <?php echo captcha_img('flat')?>
            </div>
            <div class="form-group">
                <span class="text-help">
                  {{ __('auth.accept.terms') }} <a href="#">{{ __('auth.user.terms') }}.</a>
                </span>

            </div>
            <div class="form-group">
                <input type="hidden" name="ref" value="{{ $ref }}">
                <button type="submit" class="btn-default">{{ __('auth.to.register') }}</button>
            </div>
            <div class="form-group text-center">
              <h6 class="mb-0">{{ __('auth.already.have.account') }}</h6>
              <a href="{{ route('login') }}" class="text-green font-medium">{{ __('auth.signin') }}</a>
            </div>
            </form>
          </div> 
        </div>
      </div>

    </div>

@endsection
