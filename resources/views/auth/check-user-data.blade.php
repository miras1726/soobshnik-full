 @extends('layouts.app')

@section('title')
 {{ __('auth.fill.fields') }}
@endsection
 
@section('main')


 <div class="container"> 
        
        <div class="row justify-content-md-center my-5">  
            <div class="col-md-4 block-white px-5 py-4">
                <div class="px-2">
                    <img src="/public/img/logo.png" class="img-fluid my-3" alt="logo">
                    <p class="auth-title text-left">{{ __('auth.fill.fields') }}:</p>

                    @include('common.static-alerts')

                    <form class="text-left" action="{{ route('check.user.data.post') }}" method="post">
                         @csrf
                        @if(!Auth::user()->email)
	                        <div class="form-group">
	                            <label for="email">{{ __('auth.email.post') }}:</label>
	                            
	                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" value="{{ old('email') }}" required autofocus>
	                            
	                            @if ($errors->has('email'))
	                                <span class="invalid-feedback" role="alert">
	                                    <span>{{ $errors->first('email') }}</span>
	                                </span>
	                            @endif
	                        </div>
                        @endif
                        @if(!Auth::user()->nickname)
				            <div class="form-group">
				                <label for="nickname">{{ __('auth.nickname') }}:</label>
				                <input id="nickname" type="text" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}" name="nickname" value="{{ old('nickname') }}" required>

				                @if ($errors->has('nickname'))
				                    <span class="invalid-feedback" role="alert">
				                        <span>{{ $errors->first('nickname') }}</span>
				                    </span>
				                @endif
				            </div>
			            @endif
                        <div class="form-group">
                            <button type="submit" class="btn-default">{{ __('auth.send') }}</button>
                        </div>
                    </form>
                </div> 
            </div>
        </div>

    </div>

@endsection