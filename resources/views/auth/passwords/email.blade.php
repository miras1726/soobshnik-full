@extends('layouts.app')

@section('title')
  {{ __('auth.password.reset') }}
@endsection

@section('main')
    <div class="container"> 
      
      <div class="row justify-content-md-center my-5"> 
        <div class="col-md-4 block-white px-5 py-4">
          <div class="px-2">
            <img src="/public/img/logo.png" class="img-fluid my-3" alt="logo">
            <p class="auth-title text-left">{{ __('auth.password.reset') }}:</p>
              @if (session('message'))
                  <div class="alert alert-success" role="alert">
                      {{ session('message') }}
                  </div> 
              @endif
            <form class="text-left" action="{{ route('password.email') }}" method="post">
              @csrf
            <div class="form-group">
                <label for="">{{ __('auth.email') }}:</label>
                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" value="{{ old('email') }}" required autofocus>
                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <span>{{ $errors->first('email') }}</span>
                      </span>
                  @endif 
            </div>
            <div class="form-group">
                <button type="submit" class="btn-default">{{ __('auth.send') }}</button>
            </div>
            <div class="form-group text-center">
              <h6 class="mb-0">{{ __('auth.remember.password') }}</h6>
              <a href="{{ route('login') }}" class="text-green font-medium">{{ __('auth.signin') }}</a>
            </div>
            </form>
          </div> 
        </div>
      </div>

    </div>

@endsection
