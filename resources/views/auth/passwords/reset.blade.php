@extends('layouts.app')

@section('title')
  {{ __('auth.reset.password') }}
@endsection

@section('main')
 
    <div class="container"> 
      <div class="row justify-content-md-center my-5"> 
        <div class="col-md-4 block-white px-5 py-4">
          <div class="px-2">
            <img src="/public/img/logo.png" class="img-fluid my-3" alt="logo">
            <p class="auth-title text-left">{{ __('auth.reset.password') }}:</p>
            <form class="text-left" action="{{ route('password.update') }}" method="post">
              @csrf
            <div class="form-group">
                <label for="email">{{ __('auth.email.post') }}:</label>
                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <span>{{ $errors->first('email') }}</span>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="">{{ __('auth.password') }}:</label>
                <input type="password" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="password" name="password" required>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <span>{{ $errors->first('password') }}</span>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password_confirmation">{{ __('auth.password.one.more') }}:</label>
                <input type="password" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="password-confirm" name="password_confirmation" required>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <span>{{ $errors->first('password') }}</span>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <input type="hidden" name="token" value="{{ $token }}">
                <button type="submit" class="btn-default">{{ __('auth.send') }}</button>
            </div>
            <div class="form-group text-center">
              <a href="{{ route('login') }}" class="text-green font-medium">{{ __('auth.signin') }}</a>
            </div>
            </form>
          </div> 
        </div>
      </div>

    </div>

@endsection
