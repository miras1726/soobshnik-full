@extends('layouts.app')

@section('title')
  {{ __('auth.verify.email') }}
@endsection

@section('main')

    <div class="container"> 
      
      <div class="row justify-content-md-center my-5"> 
        <div class="col-md-4 block-white px-5 py-4">
          <div class="px-2">
            <img src="/public/img/logo.png" class="img-fluid my-3" alt="logo">
            @if (session('resented'))
                <div class="alert alert-success" role="alert">
                    {{ session('resented') }}
                </div>
                <br>
            @endif 
            <div class="form-group text-center">
              <p class="text-help">{{ __('auth.verify.text') }}</p>
              <p>
                <a href="{{ route('verification.resend') }}" class="text-green font-medium">{{ __('auth.send.one.more') }}</a>
              </p>
              <a class="text-green font-medium" href="{{ route('logout') }}"
                 onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                  {{ __('auth.logout') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </div>
          </div> 
        </div>
      </div>

    </div>

@endsection
