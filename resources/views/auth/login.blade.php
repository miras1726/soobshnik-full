@extends('layouts.app')

@section('title')
    {{ __('auth.signin') }}
@endsection

@section('main')


 <div class="container"> 
        
        <div class="row justify-content-md-center my-5">  
            <div class="col-md-4 block-white px-5 py-4">
                <div class="px-2">
                    <img src="/public/img/logo.png" class="img-fluid my-3" alt="logo">
                    <p class="auth-title text-center">{{ __('auth.login.with.social') }}:</p>
                    
                    <div>
                        <div class="text-center margin-bottom-20" id="uLogin"
     data-ulogin="display=panel;theme=flat;
                             providers=facebook,vkontakte,odnoklassniki,twitter,google,mailru;
                             redirect_uri={{ urlencode('https://' . $_SERVER['HTTP_HOST']) }}/auth/ulogin;">
                        </div>    
                    </div>
  

                    <div class="auth-or-block mt-4">
                        <p class="auth-or">{{ __('auth.or') }}</p>
                    </div>

                        @include('common.static-alerts')

                    <form class="text-left" action="{{ route('login') }}" method="post">
                         @csrf
                        <div class="form-group">
                            <label for="email">{{ __('auth.email.post') }}:</label>
                            
                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" value="{{ old('email') }}" required autofocus>
                            
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <span>{{ $errors->first('email') }}</span>
                                </span>
                            @endif
                          </div>
                        <div class="form-group">
                            <label for="password">{{ __('auth.password') }}:</label>
                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="password" name="password" type="password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <span>{{ $errors->first('password') }}</span>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                             <label class="float-left">{{ __('auth.remember') }}</label>
                             <label class="checkbox float-right">
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span class="checkmark"></span>
                            </label>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn-default">{{ __('auth.signin') }}</button>
                        </div>
                        <div class="form-group">
                            <a href="{{ route('password.request') }}" class="text-green font-medium">{{ __('auth.forgot.password') }}</a>
                        </div>
                        <div class="form-group text-center">
                            <h6 class="mb-0">{{ __('auth.not.registered') }}</h6>
                            <a href="{{ route('register') }}" class="text-green font-medium">{{ __('auth.register.in.sec') }}</a>
                        </div>
                    </form>
                </div> 
            </div>
        </div>

    </div>

@endsection
@section('js-app')
    <script src="//ulogin.ru/js/ulogin.js"></script>
@endsection