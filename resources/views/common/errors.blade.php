@if (count($errors) > 0)
  <!-- Список ошибок формы -->
  <div class="alert alert-dismissible  alert-danger">
    <strong>Упс! Что-то пошло не так!</strong>

    <br><br>

    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
  </div>
@endif