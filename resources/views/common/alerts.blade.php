@if(Session::has('message'))
<div class="alert alert-dismissible {{ Session::get('alert') }}">
    {!! Session::get("message") !!}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
@endif