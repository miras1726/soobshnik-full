@if(Session::has('message'))
<div class="alert {{ Session::get('alert') }} p-2">
    {!! Session::get("message") !!}
</div>
@endif