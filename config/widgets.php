<?php

/*
 * Ключ: краткое название виджета для обращения к нему из шаблона
 * Значение: пространство имен виджета (путь)
 */

return [

	'search' => 'App\Widgets\SearchWidget',
	'admin.show.count' => 'App\Widgets\AdminShowCountWidget',

];
  